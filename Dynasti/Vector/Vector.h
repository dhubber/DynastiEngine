#ifndef DYNASTI_VECTOR_H
#define DYNASTI_VECTOR_H

#include <yaml-cpp/yaml.h>


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Simple 2d Vector class for Transform2d and other 2d mathematical computations.
    /// \author  D. A. Hubber
    /// \date    30/07/2023
    //=================================================================================================================
    class Vec2
    {
    public:

        constexpr Vec2()
            : x(0.0f)
            , y(0.0f)
        {
        };

        constexpr Vec2(float x, float y)
            : x(x)
            , y(y)
        {
        };

        Vec2(const Vec2&) = default;
        Vec2& operator=(const Vec2&) = default;

        friend Vec2 operator+(const Vec2& left, const Vec2& right)
        {
            return Vec2{left.x + right.x, left.y + right.y};
        }

        float x;
        float y;
    };


    //=================================================================================================================
    /// \brief   Simple 4d Vector class for general vector calculations, and also holding RGBA colour data.
    /// \author  D. A. Hubber
    /// \date    30/07/2023
    //=================================================================================================================
    class Vec4
    {
    public:

        constexpr Vec4()
            : x(0.0f)
            , y(0.0f)
            , z(0.0f)
            , w(0.0f)
        {
        };

        constexpr Vec4(float x, float y, float z, float w)
            : x(x)
            , y(y)
            , z(z)
            , w(w)
        {
        };

        float x;
        float y;
        float z;
        float w;
    };


    // Declarations for YAML streaming operators (used for serializing vectors like built-in types)
    YAML::Emitter &operator<<(YAML::Emitter &out, const Vec2 &v);
    YAML::Emitter &operator<<(YAML::Emitter &out, const Vec4 &v);

}
//---------------------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------------------
namespace YAML
{
    template<>
    struct convert<Dynasti::Vec2>
    {
        static bool decode(const Node& node, Dynasti::Vec2 &rhs)
        {
            if (!node.IsSequence() || node.size() != 2) {
                return false;
            }
            rhs.x = node[0].as<float>();
            rhs.y = node[1].as<float>();
            return true;
        }
    };

    template<>
    struct convert<Dynasti::Vec4>
    {
        static bool decode(const Node& node, Dynasti::Vec4 &rhs)
        {
            if (!node.IsSequence() || node.size() != 4) {
                return false;
            }
            rhs.x = node[0].as<float>();
            rhs.y = node[1].as<float>();
            rhs.z = node[2].as<float>();
            rhs.w = node[3].as<float>();
            return true;
        }
    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
