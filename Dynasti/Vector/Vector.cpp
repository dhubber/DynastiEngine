#include <yaml-cpp/yaml.h>
#include "Dynasti/Vector/Vector.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    YAML::Emitter& operator<<(YAML::Emitter &yamlEmitter, const Vec2 &v)
    {
        yamlEmitter << YAML::Flow;
        yamlEmitter << YAML::BeginSeq << v.x << v.y << YAML::EndSeq;
        return yamlEmitter;
    }

    YAML::Emitter& operator<<(YAML::Emitter &yamlEmitter, const Vec4 &v)
    {
        yamlEmitter << YAML::Flow;
        yamlEmitter << YAML::BeginSeq << v.x << v.y << v.z << v.w << YAML::EndSeq;
        return yamlEmitter;
    }

}
//---------------------------------------------------------------------------------------------------------------------