#ifndef DYNASTI_APPLICATION_H
#define DYNASTI_APPLICATION_H

#include <filesystem>
#include <functional>
#include <memory>
#include <string>
#include "Dynasti/Event/Event.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    class AssetManager;
    class ClassRegistry;
    class Collision2dSystem;
    class EventListener;
    class EventManager;
    class FrameTimer;
    class InputManager;
    class InputMap;
    class RendererSystem;
    class SceneSystem;
    class UuidGenerator;
    class WindowSystem;
    
    //=================================================================================================================
    /// \brief   Virtual parent base class for defining main application object.
    /// \author  D. A. Hubber
    /// \date    04/08/2022
    //=================================================================================================================
    class Application
    {
    public:

        static std::filesystem::path appDirectory_;               ///< Directory containing application executable

        /// Application constructor; also constructs all main system objects
        /// \param[in] name - Human readable application name
        /// \param[in] setupGame - Optional function callback for setting up the game (e.g. creating main scene)
        explicit Application(const std::string& name="DynastiApp");

        /// Entry point for running the application
        void Run();

        /// Callback function when a "QuitApp" command/event is broadcast
        void OnQuitEvent(Event event) { quit_ = true; }

    protected:

        const std::string name_;                                 ///< Human-readable application name
        bool quit_ = false;                                      ///< Flag if program will quit in next frame
        std::shared_ptr<AssetManager> assetManager_;             ///< Global asset manager
        std::shared_ptr<ClassRegistry> classRegistry_;           ///< Global class registry
        std::shared_ptr<Collision2dSystem> collision2DSystem_;   ///< Global physics 2d system
        std::shared_ptr<EventListener> eventListener_;           ///< Event listener for Application object
        std::shared_ptr<EventManager> eventManager_;             ///< Global event manager
        std::shared_ptr<FrameTimer> frameTimer_;                 ///< Global frame timer
        std::shared_ptr<InputManager> inputManager_;             ///< Global input manager
        std::shared_ptr<InputMap> defaultInputMap_;              ///< Default input map used by input manager
        std::shared_ptr<RendererSystem> rendererSystem_;         ///< Renderer
        std::shared_ptr<SceneSystem> sceneSystem_;               ///< Manages all scenes in game loop
        std::shared_ptr<UuidGenerator> uuidGenerator_;           ///< Main UUID generators
        std::shared_ptr<WindowSystem> windowSystem_;             ///< Window manager

        /// Set-up the application, allocate memory, create objects, etc..
        virtual void Setup();

        /// Main function to run application (e.g. executing tasks in the TaskManager as part of the main loop)
        virtual void MainLoop();

        /// Shuts down all application systems, deallocate memory, destroys objects, etc..
        virtual void Shutdown();

        /// Opens and parses settings file for all application systems
        virtual bool LoadSettings();

        /// Writes all settings to file
        virtual bool SaveSettings();

        /// Set-up logic for the entire game application
        virtual bool SetupGame() { return false; }

    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
