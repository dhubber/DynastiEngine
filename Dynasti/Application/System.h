#ifndef DYNASTI_SYSTEM_H
#define DYNASTI_SYSTEM_H

#include <string>
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Event/EventListener.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    class AssetManager;
    class ClassRegistry;
    class EventManager;
    class FrameTimer;
    
    //=================================================================================================================
    /// \brief   Virtual base class for all engine systems that need to communicate via the event system.
    /// \author  D. A. Hubber
    /// \date    12/06/2020
    //=================================================================================================================
    class System
    {
    public:
        
        System(const std::string& name,
               std::shared_ptr<AssetManager> assetManager,
               std::shared_ptr<ClassRegistry> classRegistry,
               std::shared_ptr<EventManager> eventManager,
               std::shared_ptr<FrameTimer> frameTimer)
            : name_(name)
            , assetManager_(assetManager)
            , classRegistry_(classRegistry)
            , eventManager_(eventManager)
            , frameTimer_(frameTimer)
        {
            DYNASTI_LOG_VERBOSE("Constructing system: " + name_);
            DYNASTI_ASSERT(eventManager, "Invalid pointer to EventManager for system:" + name_);
            eventListener_ = std::make_shared<EventListener>(eventManager_, name_);
        };
        
        virtual ~System()
        {
            DYNASTI_LOG_VERBOSE("Destroying system: " + std::string(name_));
        }

        /// Registers all related classes e.g. for reflection or object-pooling
        virtual void RegisterClasses() = 0;
        
        /// Sets-up the system after the application start-up
        virtual void Setup() = 0;
        
        /// Shuts down the system just before application closing
        virtual void Shutdown() = 0;
        
        /// Update function for systems that are updated every frame of the applications main loop
        virtual void Update() = 0;

    protected:
        
        std::string name_;                                 ///< Human-readable system name
        std::shared_ptr<AssetManager> assetManager_;       ///< Global asset manager
        std::shared_ptr<ClassRegistry> classRegistry_;     ///< Global class registry
        std::shared_ptr<EventManager> eventManager_;       ///< Global event manager
        std::shared_ptr<EventListener> eventListener_;     ///< Event listener for this system
        std::shared_ptr<FrameTimer> frameTimer_;           ///< Global frame timer
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
