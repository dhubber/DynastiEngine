#include "Dynasti/Application/Application.h"

#include <format>
#include "Dynasti/Asset/AssetManager.h"
#include "Dynasti/Collision/Collision2dSystem.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Core/FrameTimer.h"
#include "Dynasti/Core/Uuid.h"
#include "Dynasti/Event/EventManager.h"
#include "Dynasti/Input/InputManager.h"
#include "Dynasti/Reflection/ClassRegistry.h"
#include "Dynasti/Renderer/RendererSystem.h"
#include "Dynasti/Scene/Scene.h"
#include "Dynasti/Scene/SceneSystem.h"
#include "Dynasti/Window/WindowSystem.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    std::filesystem::path Application::appDirectory_ = std::filesystem::current_path();

    Application::Application(const std::string& name)
        : name_(name)
    {
        appDirectory_ = std::filesystem::current_path();

        DYNASTI_LOG_INIT(name_, LogMode::Verbose, LogMode::Verbose)
        DYNASTI_TIMER_INIT(name_)

        DYNASTI_LOG("Constructing Application: " + name);
        classRegistry_ = std::make_shared<ClassRegistry>();

        uuidGenerator_ = std::make_shared<UuidGenerator>();
        assetManager_ = std::make_shared<AssetManager>(uuidGenerator_);
        eventManager_ = std::make_shared<EventManager>();
        eventListener_ = std::make_shared<EventListener>(eventManager_, "Application");
        frameTimer_ = std::make_shared<FrameTimer>();
        inputManager_ = std::make_shared<InputManager>(eventManager_);
        windowSystem_ = std::make_shared<WindowSystem>("WindowSystem", assetManager_, classRegistry_, eventManager_,
                                                       frameTimer_, inputManager_);
        sceneSystem_ = std::make_shared<SceneSystem>("SceneSystem", assetManager_, classRegistry_, eventManager_,
                                                     frameTimer_, uuidGenerator_);
        collision2DSystem_ = std::make_shared<Collision2dSystem>("Collision2dSystem", assetManager_, classRegistry_,
                                                                 eventManager_, frameTimer_);
        rendererSystem_ = std::make_shared<RendererSystem>("RendererSystem", assetManager_, classRegistry_,
                                                           eventManager_, frameTimer_);
    }

    void Application::Run()
    {
        LoadSettings();
        Setup();
        MainLoop();
        SaveSettings();
        Shutdown();
    }

    void Application::Setup()
    {
        windowSystem_->RegisterClasses();
        sceneSystem_->RegisterClasses();
        collision2DSystem_->RegisterClasses();
        rendererSystem_->RegisterClasses();

        auto inputMap = inputManager_->FindOrCreateInputMap("DefaultInputMap");
        eventListener_->SubscribeToEvent("QuitApp", std::bind(&Application::OnQuitEvent, this, std::placeholders::_1));
        eventListener_->SubscribeToEvent("QuitAppActionKeyDown", std::bind(&Application::OnQuitEvent, this, std::placeholders::_1));

        windowSystem_->Setup();
        sceneSystem_->Setup();
        collision2DSystem_->Setup();
        rendererSystem_->Setup();

        SetupGame();
        /*if (SetupGame()) {
            // Serialize main scene for debugging
            // TODO: Remove later once more functionality is in place
            YAML::Emitter yamlEmitter;
            Scene::mainScene->Serialize(yamlEmitter);
            const auto tempFileName = Scene::mainScene->GetName() + ".yaml";
            std::ofstream fout(tempFileName);
            fout << yamlEmitter.c_str();
            fout.close();
        }*/

        eventManager_->LogAllRegisteredEvents();
    }

    void Application::MainLoop()
    {
        while (!quit_)
        {
            windowSystem_->Update();
            sceneSystem_->Update();
            collision2DSystem_->Update();
            rendererSystem_->Update();
        }
    }

    void Application::Shutdown()
    {
        rendererSystem_->Shutdown();
        collision2DSystem_->Shutdown();
        sceneSystem_->Shutdown();
        windowSystem_->Shutdown();
    }

    bool Application::LoadSettings()
    {
        auto settingsFileName = std::format("{}/{}.settings.yaml", appDirectory_.string(), name_);

        // First check if file exists
        auto settingsFileExists = std::filesystem::exists(settingsFileName);
        if (!settingsFileExists) {
            DYNASTI_WARNING("Could not find settings file: " + settingsFileName);
            return false;
        }

        DYNASTI_LOG("Loading settings file: " + settingsFileName);
        YAML::Node data = YAML::LoadFile(settingsFileName);

        auto allInputData = data["Input"];
        inputManager_->Deserialize(allInputData);
    }

    bool Application::SaveSettings()
    {
        YAML::Emitter emitter;
        emitter << YAML::BeginMap;

        emitter << YAML::Key << "Input" << YAML::Value;
        inputManager_->Serialize(emitter);

        emitter << YAML::EndMap;

        auto settingsFileName = std::format("{}/{}.settings.yaml", appDirectory_.string(), name_);
        DYNASTI_LOG("Saving settings to: " + settingsFileName);
        std::ofstream fout(settingsFileName);
        fout << emitter.c_str();
        return true;
    }

}
//---------------------------------------------------------------------------------------------------------------------
