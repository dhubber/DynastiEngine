#include "doctest/doctest.h"
#include "Dynasti/Reflection/ClassRegistry.h"
#include "Dynasti/Transform/Transform2d.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti::Tests
{
    TEST_CASE("Transform2d")
    {
        ClassRegistry classRegistry;
        auto testClassReflection = classRegistry.RegisterClass<Transform2d>();
        auto transform2d = Transform2d();

        SUBCASE("Serialization")
        {
            YAML::Emitter yamlEmitter;
            classRegistry.Serialize(yamlEmitter, &transform2d);
            std::string dataString = yamlEmitter.c_str();
            YAML::Node yamlNode = YAML::Load(dataString);
            std::cout << dataString << std::endl;

            Transform2d transform2dDeserialize;
            auto propertyData = yamlNode["Transform2d"];
            classRegistry.Deserialize(propertyData, &transform2dDeserialize);
        }

    }

}
//---------------------------------------------------------------------------------------------------------------------