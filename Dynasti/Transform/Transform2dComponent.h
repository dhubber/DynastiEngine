#ifndef DYNASTI_TRANSFORM2D_COMPONENT_H
#define DYNASTI_TRANSFORM2D_COMPONENT_H


#include "Dynasti/Core/Constants.h"
#include "Dynasti/Object/Object.h"
#include "Dynasti/Transform/Transform2d.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Component class containing all data for rendering a mesh to the viewport.
    /// \author  D. A. Hubber
    /// \date    13/02/2020
    //=================================================================================================================
    class Transform2dComponent : public PoolableObject
    {
    public:

        // Required by reflection system
        std::type_index GetType() override { return std::type_index(typeid(Transform2dComponent)); }
        static std::string Name() { return "Transform2dComponent"; }
        static std::vector<std::string> BaseClasses() { return {}; }
        static std::vector<PropertyPtr> Properties()
        {
            return std::vector<PropertyPtr>{
                std::make_shared<ReflectedProperty<Transform2dComponent, Transform2d>>("localTransform", &Transform2dComponent::localTransform2d_)
            };
        }

        // Required by Poolable
        void OnCreate() override {};
        void OnDestroy() override
        {
            localTransform2d_.Reset();
            //globalTransform2d_.Reset();
        };

        inline bool IsDirty() const { return dirty_; }
        inline const glm::mat4& GetModelMatrix() const { return localTransform2d_.GetModelMatrix(); }
        inline const Vec2& GetPosition() const { return localTransform2d_.GetPos(); }
        inline float GetRotation() const { return localTransform2d_.GetAngle(); }

        /// \param[in] pos - New position
        inline void SetLocalPosition(const Vec2& pos)
        {
            localTransform2d_.SetPosition(pos);
            dirty_ = true;
        }

        /// \param[in] qrot - New quaternion
        inline void SetLocalRotation(const float angle)
        {
            localTransform2d_.SetRotation(angle);
            dirty_ = true;
        }

        /// \param[in] scale - New 3d scale
        inline void SetLocalScale(const Vec2& scale)
        {
            localTransform2d_.SetScale(scale);
            dirty_ = true;
        }

        inline void UpdateModelMatrix()
        {
            localTransform2d_.UpdateModelMatrix();
            dirty_ = false;
        }

    protected:

        bool dirty_ = false;
        Transform2d localTransform2d_;
        //Transform2d globalTransform2d_;

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif