#include "Dynasti/Transform/Transform2d.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    std::vector<PropertyPtr> Transform2d::Properties()
    {
        return std::vector<PropertyPtr>{
                std::make_shared<BasicProperty<Transform2d, Vec2>>("pos", &Transform2d::pos_),
                std::make_shared<BasicProperty<Transform2d, Vec2>>("scale", &Transform2d::scale_),
                std::make_shared<BasicProperty<Transform2d, float>>("angle", &Transform2d::angle_)
        };
    }


    glm::quat Transform2d::AxisAngleToQuaternion(const float ex, const float ey, const float ez, const float angle)
    {
        const float cosHalfAngle = cos(0.5f * angle);
        const float sinHalfAngle = sin(0.5f * angle);
        glm::quat q;
        q.x = sinHalfAngle * ex;
        q.y = sinHalfAngle * ey;
        q.z = sinHalfAngle * ez;
        q.w = cosHalfAngle;
        return q;
    }


    void Transform2d::Reset()
    {
        pos_ = defaultPos;
        scale_ = defaultScale;
        angle_ = defaultAngle;
        UpdateModelMatrix();
    }


    void Transform2d::UpdateModelMatrix()
    {
        modelMatrix_ = glm::mat4(1.0);
        glm::mat4 scaleMatrix = glm::scale(glm::mat4(1.0), glm::vec3(scale_.x, scale_.y, 1.0f));
        glm::mat4 rotationMatrix = glm::mat4_cast(AxisAngleToQuaternion(0.0f, 0.0f, 1.0f, angle_));
        glm::mat4 translationMatrix = glm::translate(glm::mat4(1.0), glm::vec3(pos_.x, pos_.y, 0.0f));
        modelMatrix_ = translationMatrix*rotationMatrix*scaleMatrix;
    }

}
//---------------------------------------------------------------------------------------------------------------------