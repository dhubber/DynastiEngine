#ifndef DYNASTI_TRANSFORM_H
#define DYNASTI_TRANSFORM_H


#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include "Dynasti/Vector/Vector.h"
#include "Dynasti/Reflection/Property.h"
#include "Dynasti/Reflection/Reflectable.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Simple 2d transform data structure
    /// \author  D. A. Hubber
    /// \date    24/11/2022
    //=================================================================================================================
    class Transform2d : public Reflectable
    {
    public:

        // Constants for all default valuess
        static constexpr Vec2 defaultPos = Vec2(0.0f, 0.0f);
        static constexpr Vec2 defaultScale = Vec2(1.0f, 1.0f);
        static constexpr float defaultAngle = 0.0f;

        // Required by reflection system
        std::type_index GetType() override { return std::type_index(typeid(Transform2d)); }
        static std::string Name() { return "Transform2d"; }
        static std::vector<std::string> BaseClasses() { return {}; }
        static std::vector<PropertyPtr> Properties();


        Transform2d()
        {
            UpdateModelMatrix();
        }

        Transform2d(const Vec2& pos, const Vec2& scale, const float angle)
            : pos_(pos)
            , scale_(scale)
            , angle_(angle)
        {
            UpdateModelMatrix();
        }

        void Reset();

        /// Updates the values in the model matrix based on new values of the position, scale and quaternions.
        void UpdateModelMatrix();

        /// Converts an axis-angle representation to a quaternion, which is then returned
        /// \param[in] ex - x value of direction vector
        /// \param[in] ey - y value of direction vector
        /// \param[in] ez - z value of direction vector
        /// \param[in] angle -
        static inline glm::quat AxisAngleToQuaternion(const float ex, const float ey, const float ez, const float angle);

        // Getters and setters
        inline const glm::mat4& GetModelMatrix() const { return modelMatrix_; }
        inline const float GetAngle() const { return angle_; }
        inline const Vec2& GetPos() const { return pos_; }
        inline const Vec2& GetScale() const { return scale_; }
        inline void SetPosition(const Vec2& pos) { pos_ = pos; }
        inline void SetRotation(const float angle) { angle_ = angle; }
        inline void SetScale(const Vec2& scale) { scale_ = scale; }


    protected:

        Vec2 pos_{defaultPos};
        Vec2 scale_{defaultScale};
        float angle_{defaultAngle};
        glm::mat4 modelMatrix_;

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif