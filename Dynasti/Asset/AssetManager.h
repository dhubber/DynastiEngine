#ifndef DYNASTI_ASSET_MANAGER_H
#define DYNASTI_ASSET_MANAGER_H

#include "Dynasti/Asset/AssetRegistry.h"
#include "Dynasti/Renderer/Mesh.h"
#include "Dynasti/Scene/Scene.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Manages all engine-wide assets (and asset registries)
    /// \author  D. A. Hubber
    /// \date    09/08/2023
    //=================================================================================================================
    class AssetManager
    {
    public:

        AssetManager(std::shared_ptr<UuidGenerator> uuidGenerator)
            : meshes_(uuidGenerator)
            , scenes_(uuidGenerator)
        {
        }

        inline AssetRegistry<Mesh>& GetMeshRegistry() { return meshes_; }
        inline AssetRegistry<Scene>& GetSceneRegistry() { return scenes_; }

    protected:

        AssetRegistry<Mesh> meshes_;
        AssetRegistry<Scene> scenes_;

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
