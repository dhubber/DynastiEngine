#ifndef DYNASTI_ASSET_REGISTRY_H
#define DYNASTI_ASSET_REGISTRY_H

#include <memory>
#include <string>
#include <unordered_map>
#include "Dynasti/Asset/Asset.h"
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Core/Uuid.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Registry for all assets of the templated class type.
    /// \author  D. A. Hubber
    /// \date    14/08/2020
    //=================================================================================================================
    template <typename AssetType>
    class AssetRegistry
    {
    public:

        AssetRegistry(UuidGeneratorPtr uuidGenerator)
            : uuidGenerator_(uuidGenerator)
        {

        }

        virtual ~AssetRegistry()
        {
            UnregisterAllAssets();
        }

        ///< Registers an asset in the registry.  If the name is not unique, then an alternative name is generated.
        /// \param[in] assetName - Name of asset for easy lookup
        /// \param[in] assetPtr - Pointer to the asset object
        void RegisterNewAsset(std::shared_ptr<AssetType> asset)
        {
            DYNASTI_ASSERT(asset != nullptr, "Invalid pointer to asset");

            // Generate new UUID for asset
            auto uuid = uuidGenerator_->GenerateUuid();
            const std::string& name = asset->GetName();
            DYNASTI_ASSERT(Find(uuid) == nullptr, "Duplicate asset UUID");
            DYNASTI_ASSERT(Find(name) == nullptr, "Duplicate asset name");
            asset->uuid = uuid;

            assetUuidMap_[uuid] = asset;
            assetNameMap_[name] = asset;
        }

        void UnregisterAsset(std::shared_ptr<AssetType> asset)
        {
            DYNASTI_ASSERT(asset != nullptr, "Invalid pointer to asset");
            assetNameMap_.erase(asset->GetName());
            assetUuidMap_.erase(asset->uuid);
        }

        /// Searches for an asset using its unique global id and returns a pointer to the asset (if it exists)
        /// \param[in] Uuid - Global id of asset
        /// \return - Pointer to the asset; otherwise returns nullptr if it does not exist
        std::shared_ptr<AssetType> Find(const Uuid Uuid)
        {
            auto it = assetUuidMap_.find(Uuid);
            if (it != assetUuidMap_.end())
            {
                DYNASTI_ASSERT(it->second, "Invalid shared pointer found for asset global id : " + std::to_string(Uuid));
                return it->second;
            }
            return nullptr;
        }

        /// Searches for an asset using its unique name and returns a pointer to the asset (if it exists)
        /// \param[in] assetName - Human-readable name of the asset
        /// \return - Pointer to the asset; otherwise returns nullptr if it does not exist
        std::shared_ptr<AssetType> Find(const std::string assetName)
        {
            auto it = assetNameMap_.find(assetName);
            if (it != assetNameMap_.end())
            {
                DYNASTI_ASSERT(it->second, "Invalid shared pointer found for asset name : " + assetName);
                return it->second;
            }
            return nullptr;
        }

        /// Unregisters all assets stored in this registry
        void UnregisterAllAssets()
        {
            assetNameMap_.clear();
            assetUuidMap_.clear();
        }

    protected:

        UuidGeneratorPtr uuidGenerator_;                                           ///< Global UUID generator object
        std::unordered_map<std::string, std::shared_ptr<AssetType>> assetNameMap_; ///< Map of name to asset
        std::unordered_map<Uuid, std::shared_ptr<AssetType>> assetUuidMap_;        ///< Map of id to asset

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
