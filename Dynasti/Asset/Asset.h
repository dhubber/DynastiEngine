#ifndef DYNASTI_ASSET_H
#define DYNASTI_ASSET_H

#include <filesystem>
#include <string>
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Object/Object.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Base class for all engine-wide assets.
    /// \author  D. A. Hubber
    /// \date    09/08/2023
    //=================================================================================================================
    class Asset : public Object
    {
    public:

        Asset(const std::string name)
            : name_(name)
        {
        }

        Asset(const std::string name, std::filesystem::path filePath)
            : name_(name)
            , filePath_(filePath)
        {
        }

        const std::string& GetName() const { return name_; }

    protected:

        std::string name_;                                 ///< Unique human-readable name
        std::filesystem::path filePath_;                   ///< Absolute path of filename (when asset is imported)

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif

