#include "doctest/doctest.h"
#include "Dynasti/Asset/AssetManager.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    class MockAsset : Asset
    {
    public:
        MockAsset() = default;
        std::type_index GetType() override { return std::type_index(typeid(MockAsset)); }
    };

    TEST_CASE("Asset")
    {
        auto uuidGenerator = std::make_shared<UuidGenerator>();
        auto assetRegistry = AssetRegistry<MockAsset>(uuidGenerator);
    }

}
//---------------------------------------------------------------------------------------------------------------------