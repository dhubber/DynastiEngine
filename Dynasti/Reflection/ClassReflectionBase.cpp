#include "Dynasti/Reflection/ClassReflection.h"

#include "Dynasti/Reflection/ClassRegistry.h"
#include "Dynasti/Reflection/Reflectable.h"
#include "ClassReflectionBase.h"



//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    ClassReflectionBase::ClassReflectionBase(const std::string& className,
                                             std::vector<std::shared_ptr<PropertyBase>> properties)
        : className_(className)
    {
        for (auto& property : properties)
        {
            RegisterProperty(property);
        }
    }


    bool ClassReflectionBase::RegisterProperty(std::shared_ptr<PropertyBase> property)
    {
        // Ensure property name is unique
        const std::string& propertyName = property->GetPropertyName();
        if (GetProperty(propertyName))
        {
            DYNASTI_WARNING("BasicProperty name already exits.  Cannot register duplicate names");
            return false;
        }

        propertyNameMap_.insert(std::make_pair(propertyName, property));
        propertyTypeMap_.insert(std::make_pair(property->GetType(), property));
        return true;
    }


    void ClassReflectionBase::RegisterBaseClass(ClassReflectionPtr baseClassReflectionPtr)
    {
        const std::string& baseClassName = baseClassReflectionPtr->GetClassName();
        std::type_index baseClassTypeIndex = baseClassReflectionPtr->GetType();
        baseClassNameMap_.insert(std::make_pair(baseClassName, baseClassReflectionPtr));
        baseClassTypeMap_.insert(std::make_pair(baseClassTypeIndex, baseClassReflectionPtr));
    }


    std::shared_ptr<PropertyBase> ClassReflectionBase::GetProperty(const std::string &propertyName) const
    {
        // First check if property exists in class
        auto it = propertyNameMap_.find(propertyName);
        if (it != propertyNameMap_.end())
        {
            return it->second;
        }

        // If not, then check base classes
        for (auto baseClassReflection : baseClassNameMap_)
        {
            auto property = baseClassReflection.second->GetProperty(propertyName);
            if (property != nullptr)
            {
                return property;
            }
        }

        return nullptr;
    }


    void ClassReflectionBase::CopyProperties(Reflectable *source, Reflectable *target)
    {
        for (auto it : propertyTypeMap_)
        {
            auto property = it.second;
            property->SetValue(target, property->GetValue(source));
        }

        for (auto baseClassReflection : baseClassNameMap_)
        {
            baseClassReflection.second->CopyProperties(source, target);
        }
    }


    /*void ClassReflectionBase::Serialize(YAML::Emitter &yamlEmitter, Reflectable* object, std::optional<std::string> name)
    {
        yamlEmitter << YAML::BeginMap;
        yamlEmitter << YAML::Key << name.value_or(GetClassName()) << YAML::Value;
        yamlEmitter << YAML::BeginMap;
        SerializeProperties(yamlEmitter, object);
        yamlEmitter << YAML::EndMap;
        yamlEmitter << YAML::EndMap;
    }


    void ClassReflectionBase::SerializeProperties(YAML::Emitter &yamlEmitter, Reflectable* object)
    {
        for (auto it : propertyNameMap_) {
            auto property = it.second;
            if (property->IsReflectedClass())
            {
                auto classRegistry = classRegistryWeakPtr_.lock();
                if (classRegistry)
                {
                    auto propertyClassReflection = classRegistry->FindClass(property->GetType());
                    DYNASTI_ASSERT(propertyClassReflection != nullptr,
                                   "Could not find class reflection for: " + property->GetPropertyName());
                    Reflectable *propertyValuePointer = static_cast<Reflectable *>(property->GetPointerToValue(object));
                    yamlEmitter << YAML::Key << property->GetPropertyName() << YAML::Value;
                    yamlEmitter << YAML::BeginMap;
                    propertyClassReflection->SerializeProperties(yamlEmitter, propertyValuePointer);
                    yamlEmitter << YAML::EndMap;
                }
            }
            else
            {
                property->Serialize(yamlEmitter, object);
            }
        }

        // Iterate through all base-classes and serialize their properties as well
        for (auto it : baseClassNameMap_)
        {
            auto baseClassReflection = it.second;
            baseClassReflection->SerializeProperties(yamlEmitter, object);
        }
    }


    void ClassReflectionBase::Deserialize(YAML::Node &yamlNode, Reflectable* object)
    {
        DeserializeProperties(yamlNode, object);
    }


    void ClassReflectionBase::DeserializeProperties(YAML::Node &yamlNode, Reflectable* object)
    {
        for (auto it = std::begin(yamlNode); it != std::end(yamlNode); ++it) {
            auto key = it->first.as<std::string>();
            auto value = it->second;
            auto property = GetProperty(key);
            if (property != nullptr) {
                if (property->IsReflectedClass())
                {
                    auto classRegistry = classRegistryWeakPtr_.lock();
                    if (classRegistry)
                    {
                        auto propertyClassReflection = classRegistry->FindClass(property->GetType());
                        DYNASTI_ASSERT(propertyClassReflection != nullptr,
                                       "Could not find class reflection for: " + property->GetPropertyName());
                        Reflectable *propertyValuePointer = static_cast<Reflectable *>(property->GetPointerToValue(
                                object));
                        propertyClassReflection->Deserialize(value, propertyValuePointer);
                    }
                }
                else
                {
                    property->Deserialize(value, object);
                }
            }
        }
    }*/

}
//---------------------------------------------------------------------------------------------------------------------