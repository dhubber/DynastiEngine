#ifndef DYNASTI_CLASS_REGISTRY_H
#define DYNASTI_CLASS_REGISTRY_H


#include <any>
#include <memory>
#include <optional>
#include <string>
#include <typeindex>
#include <unordered_map>

#include "Dynasti/Core/Debug.h"
#include "Dynasti/Reflection/ClassRegistryBase.h"
#include "Dynasti/Reflection/ClassReflection.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Registry for all reflected classes.
    /// \author  D. A. Hubber
    /// \date    03/07/2023
    //=================================================================================================================
    class ClassRegistry : public ClassRegistryBase, std::enable_shared_from_this<ClassRegistry>
    {
    public:

        using ClassRegistryBase::FindClass;

        ClassRegistry() = default;

        template <typename ClassName>
        std::shared_ptr<ClassReflection<ClassName>> RegisterClass()
        {
            std::type_index classTypeIndex = std::type_index(typeid(ClassName));
            std::string className = ClassName::Name();
            auto classReflection = std::make_shared<ClassReflection<ClassName>>();
            classNameMap_.insert(std::make_pair(className, classReflection));
            classTypeMap_.insert(std::make_pair(classTypeIndex, classReflection));

            std::vector<std::string> baseClassNames = ClassName::BaseClasses();
            for (const auto& baseClassName : baseClassNames)
            {
                auto baseClassReflection = FindClass(baseClassName);
                DYNASTI_ASSERT(baseClassReflection != nullptr, "Could not find reflection for: " + baseClassName);
                classReflection->RegisterBaseClass(baseClassReflection);
            }

            return classReflection;
        }

        template <typename ClassName>
        std::shared_ptr<ClassReflection<ClassName>> FindClass()
        {
            std::type_index classTypeIndex = std::type_index(typeid(ClassName));
            return std::dynamic_pointer_cast<ClassReflection<ClassName>>(FindClass(classTypeIndex));
        }

    };
}
//---------------------------------------------------------------------------------------------------------------------
#endif
