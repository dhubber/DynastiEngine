#ifndef DYNASTI_CLASS_REFLECTION_H
#define DYNASTI_CLASS_REFLECTION_H


#include <any>
#include <memory>
#include <optional>
#include <string>
#include <typeindex>
#include <unordered_map>
#include <vector>

#include "Dynasti/Core/Debug.h"
#include "Dynasti/Object/Object.h"
#include "Dynasti/Object/ObjectPool.h"
#include "Dynasti/Reflection/ClassReflectionBase.h"
#include "Dynasti/Reflection/PropertyBase.h"
#include "Dynasti/Reflection/Reflectable.h"
#include "Dynasti/Scene/ScriptComponent.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    class ClassRegistryBase;

    //=================================================================================================================
    /// \brief   Templated concrete implementation for all class reflection
    /// \author  D. A. Hubber
    /// \date    03/07/2023
    //=================================================================================================================
    template <typename ClassName>
    class ClassReflection : public ClassReflectionBase
    {
    public:

        ClassReflection()
            : ClassReflectionBase(ClassName::Name(), ClassName::Properties())
        {
            RegisterDefaultPropertyValues();
        }

        std::type_index GetType() override { return std::type_index(typeid(ClassName)); }

        bool IsScriptableClass() override { return std::is_base_of_v<ScriptComponentBase, ClassName>; }

        bool IsPoolableClass() override { return std::is_base_of_v<PoolableObject, ClassName>; }

        ObjectPoolPtr CreateObjectPool(ClassRegistryPtr classRegistry, std::uint32_t maxNumObjects) override
        {
            if constexpr (std::is_base_of_v<PoolableObject, ClassName>)
            {
                return std::make_shared<ObjectPool<ClassName>>(classRegistry, maxNumObjects);
            }
            else
            {
                return nullptr;
            }
        }

        /*void CopyProperties(Reflectable* sourceBase, Reflectable* targetBase) override
        {
            auto sourceObject = dynamic_cast<ClassName*>(sourceBase);
            auto targetObject = dynamic_cast<ClassName*>(targetBase);
            *targetObject = *sourceObject;
        }*/

        template <typename PropertyType>
        PropertyType GetPropertyValue(Reflectable* object, const std::string &propertyName) const
        {
            auto propertyBase = GetProperty(propertyName);
            DYNASTI_ASSERT(propertyBase, "Cannot find property with name: " + propertyName);
            return std::any_cast<PropertyType>(propertyBase->GetValue(object));
        }

        template <typename PropertyType>
        void SetPropertyValue(Reflectable* object, const std::string& propertyName, PropertyType newValue)
        {
            auto propertyBase = GetProperty(propertyName);
            DYNASTI_ASSERT(propertyBase, "Cannot find property with name: " + propertyName);
            propertyBase->SetValue(object, std::make_any<PropertyType>(newValue));
        }


    protected:

        void RegisterDefaultPropertyValues()
        {
            ClassName obj;
            for (auto it : propertyNameMap_)
            {
                auto defaultValue = it.second->GetValue(dynamic_cast<Reflectable*>(&obj));
                it.second->SetDefaultValue(defaultValue);
            }
        }

    };
}
//---------------------------------------------------------------------------------------------------------------------
#endif
