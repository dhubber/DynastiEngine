#ifndef DYNASTI_PROPERTY_BASE_H
#define DYNASTI_PROPERTY_BASE_H

#include <any>
#include <memory>
#include <string>
#include <typeindex>
#include <yaml-cpp/yaml.h>
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Reflection/Reflectable.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Base class for all reflected class properties.
    /// \author  D. A. Hubber
    /// \date    03/07/2023
    //=================================================================================================================
    class PropertyBase
    {
    public:

        PropertyBase(const std::string& name)
            : name_(name)
        {
        }

        virtual std::type_index GetType() = 0;

        virtual std::any GetValue(Reflectable* object) const = 0;

        virtual void* GetPointerToValue(Reflectable* object) = 0;

        virtual std::any GetDefaultValue() const = 0;

        virtual void SetValue(Reflectable* object, std::any newValue) = 0;

        virtual void SetDefaultValue(std::any defaultValue) = 0;

        virtual void Serialize(YAML::Emitter &yamlEmitter, Reflectable* object) = 0;

        virtual void Deserialize(YAML::Node &yamlNode, Reflectable* object) = 0;

        virtual bool IsReflectedClass() const = 0;

        const std::string& GetPropertyName() const { return name_; }
        
    protected:

        const std::string name_;

    };

    using PropertyPtr = std::shared_ptr<PropertyBase>;

}
//---------------------------------------------------------------------------------------------------------------------
#endif
