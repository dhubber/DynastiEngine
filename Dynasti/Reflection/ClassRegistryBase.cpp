#include "Dynasti/Reflection/ClassRegistryBase.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    ClassReflectionPtr ClassRegistryBase::FindClass(const std::string& className)
    {
        auto it = classNameMap_.find(className);
        if (it != std::end(classNameMap_))
        {
            return it->second;
        }
        return nullptr;
    }


    ClassReflectionPtr ClassRegistryBase::FindClass(std::type_index classTypeIndex)
    {
        auto it = classTypeMap_.find(classTypeIndex);
        if (it != std::end(classTypeMap_))
        {
            return it->second;
        }
        return nullptr;
    }


    void ClassRegistryBase::Serialize(YAML::Emitter &yamlEmitter, Reflectable* object, std::optional<std::string> name)
    {
        auto classReflection = FindClass(object->GetType());
        DYNASTI_ASSERT(classReflection != nullptr, "Could not find class reflection");
        yamlEmitter << YAML::BeginMap;
        yamlEmitter << YAML::Key << name.value_or(classReflection->GetClassName()) << YAML::Value;
        yamlEmitter << YAML::BeginMap;
        SerializeProperties(yamlEmitter, object, classReflection);
        yamlEmitter << YAML::EndMap;
        yamlEmitter << YAML::EndMap;
    }


    void ClassRegistryBase::SerializeProperties(YAML::Emitter &yamlEmitter, Reflectable* object, ClassReflectionPtr classReflection)
    {
        const auto& propertyNameMap = classReflection->GetPropertyMap();
        for (auto it : propertyNameMap) {
            auto property = it.second;
            if (property->IsReflectedClass())
            {
                auto propertyClassReflection = FindClass(property->GetType());
                DYNASTI_ASSERT(propertyClassReflection != nullptr,
                               "Could not find class reflection for: " + property->GetPropertyName());
                Reflectable *propertyValuePointer = static_cast<Reflectable *>(property->GetPointerToValue(object));
                yamlEmitter << YAML::Key << property->GetPropertyName() << YAML::Value;
                yamlEmitter << YAML::BeginMap;
                SerializeProperties(yamlEmitter, propertyValuePointer, propertyClassReflection);
                yamlEmitter << YAML::EndMap;
            }
            else
            {
                property->Serialize(yamlEmitter, object);
            }
        }

        // Iterate through all base-classes and serialize their properties as well
        const auto& baseClassNameMap = classReflection->GetBaseClassMap();
        for (auto it : baseClassNameMap)
        {
            auto baseClassReflection = it.second;
            SerializeProperties(yamlEmitter, object, baseClassReflection);
        }
    }


    void ClassRegistryBase::Deserialize(YAML::Node &yamlNode, Reflectable* object)
    {
        auto classReflection = FindClass(object->GetType());
        DYNASTI_ASSERT(classReflection != nullptr, "Could not find class reflection");
        DeserializeProperties(yamlNode, object, classReflection);
    }


    void ClassRegistryBase::DeserializeProperties(YAML::Node &yamlNode, Reflectable* object, ClassReflectionPtr classReflection)
    {
        for (auto it = std::begin(yamlNode); it != std::end(yamlNode); ++it) {
            auto key = it->first.as<std::string>();
            auto value = it->second;
            auto property = classReflection->GetProperty(key);
            if (property != nullptr) {
                if (property->IsReflectedClass())
                {
                    Reflectable *propertyValuePointer = static_cast<Reflectable *>(property->GetPointerToValue(
                            object));
                    Deserialize(value, propertyValuePointer);
                }
                else
                {
                    property->Deserialize(value, object);
                }
            }
        }
    }

}
//---------------------------------------------------------------------------------------------------------------------