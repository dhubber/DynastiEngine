#ifndef DYNASTI_PROPERTY_H
#define DYNASTI_PROPERTY_H

#include "Dynasti/Reflection/PropertyBase.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Templated concrete implementation for all reflected class properties.
    /// \author  D. A. Hubber
    /// \date    03/07/2023
    //=================================================================================================================
    template <typename ClassName, typename PropertyType>
    class Property : public PropertyBase
    {
    public:

        Property(const std::string& name, PropertyType ClassName::*propertyPtr)
            : PropertyBase(name)
            , propertyPtr_(propertyPtr)
        {
        }

        std::type_index GetType() override { return std::type_index(typeid(PropertyType)); }

        std::any GetValue(Reflectable *object) const override
        {
            return std::make_any<PropertyType>(GetRawValue(object));
        }

        void* GetPointerToValue(Reflectable* object) override
        {
            ClassName *castObj = static_cast<ClassName*>(object);
            DYNASTI_ASSERT(castObj != nullptr, "Invalid object pointer provided for getting property");
            return &((*castObj).*propertyPtr_);
        }

        [[nodiscard]] std::any GetDefaultValue() const override { return std::make_any<PropertyType>(defaultValue_); }

        PropertyType ClassName::* GetPropertyClassPointer() { return propertyPtr_; }

        void SetValue(Reflectable* object, std::any newValue) override
        {
            SetRawValue(object, std::any_cast<PropertyType>(newValue));
        }

        void SetDefaultValue(std::any defaultValue) override
        {
            defaultValue_ = std::any_cast<PropertyType>(defaultValue);
        }

    protected:

        PropertyType GetRawValue(Reflectable* object) const
        {
            ClassName *castObj = static_cast<ClassName*>(object);
            DYNASTI_ASSERT(castObj != nullptr, "Invalid object pointer provided for getting property");
            return (*castObj).*propertyPtr_;
        }

        void SetRawValue(Reflectable* object, PropertyType newValue)
        {
            ClassName *castObj = static_cast<ClassName*>(object);
            DYNASTI_ASSERT(castObj != nullptr, "Invalid object pointer provided for getting property");
            (*castObj).*propertyPtr_ = newValue;
        }

        PropertyType ClassName::*propertyPtr_;             ///< Pointer to property variable in class
        PropertyType defaultValue_;                        ///< Default value of property

    };

    //=================================================================================================================
    /// \brief   Templated concrete implementation for all reflected class properties.
    /// \author  D. A. Hubber
    /// \date    03/07/2023
    //=================================================================================================================
    template <typename ClassName, typename PropertyType>
    class BasicProperty : public Property<ClassName,PropertyType>
    {
    public:

        using Property<ClassName,PropertyType>::defaultValue_;
        using Property<ClassName,PropertyType>::name_;
        using Property<ClassName,PropertyType>::GetRawValue;
        using Property<ClassName,PropertyType>::SetRawValue;

        BasicProperty(const std::string& name, PropertyType ClassName::*propertyPtr)
            : Property<ClassName,PropertyType>(name, propertyPtr)
        {
        }

        void Serialize(YAML::Emitter &yamlEmitter, Reflectable* object) override
        {
            const auto &value = GetRawValue(object);
            yamlEmitter << YAML::Key << name_ << YAML::Value << value;
        }

        void Deserialize(YAML::Node &yamlNode, Reflectable* object) override
        {
            auto value = yamlNode.as<PropertyType>();
            SetRawValue(object, value);
        }

        [[nodiscard]] bool IsReflectedClass() const override { return false; }

    };

    //=================================================================================================================
    /// \brief   Templated concrete implementation for all reflected class properties.
    /// \author  D. A. Hubber
    /// \date    03/07/2023
    //=================================================================================================================
    template <typename ClassName, typename PropertyType>
    class ReflectedProperty : public Property<ClassName,PropertyType>
    {
    public:

        ReflectedProperty(const std::string& name, PropertyType ClassName::*propertyPtr)
            : Property<ClassName,PropertyType>(name, propertyPtr)
        {
        }

        void Serialize(YAML::Emitter &yamlEmitter, Reflectable* object) override {};
        void Deserialize(YAML::Node &yamlNode, Reflectable* object) override {};

        [[nodiscard]] bool IsReflectedClass() const override { return true; }

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
