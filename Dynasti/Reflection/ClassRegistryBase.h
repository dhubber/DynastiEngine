#ifndef DYNASTI_CLASS_REGISTRY_BASE_H
#define DYNASTI_CLASS_REGISTRY_BASE_H


#include <any>
#include <memory>
#include <optional>
#include <string>
#include <typeindex>
#include <unordered_map>

#include "Dynasti/Core/Debug.h"
#include "Dynasti/Reflection/Reflectable.h"
#include "Dynasti/Reflection/ClassReflectionBase.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Registry for all reflected classes.
    /// \author  D. A. Hubber
    /// \date    03/07/2023
    //=================================================================================================================
    class ClassRegistryBase
    {
    public:

        //virtual ObjectPoolPtr CreateObjectPool(std::uint32_t maxNumObjects) = 0;

        ClassReflectionPtr FindClass(std::type_index classTypeIndex);
        ClassReflectionPtr FindClass(const std::string& className);

        void Serialize(YAML::Emitter &yamlEmitter, Reflectable* object, std::optional<std::string> name=std::nullopt);
        void Deserialize(YAML::Node &yamlNode, Reflectable* object);

        void SerializeProperties(YAML::Emitter &yamlEmitter, Reflectable* object, ClassReflectionPtr classReflection);
        void DeserializeProperties(YAML::Node &yamlNode, Reflectable* object, ClassReflectionPtr classReflection);


    protected:

        std::unordered_map<std::string, ClassReflectionPtr> classNameMap_;
        std::unordered_map<std::type_index, ClassReflectionPtr> classTypeMap_;

    };

    using ClassRegistryPtr = std::shared_ptr<ClassRegistryBase>;

}
//---------------------------------------------------------------------------------------------------------------------
#endif
