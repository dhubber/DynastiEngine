#ifndef DYNASTI_CLASS_REFLECTION_BASE_H
#define DYNASTI_CLASS_REFLECTION_BASE_H


#include <any>
#include <memory>
#include <optional>
#include <string>
#include <typeindex>
#include <unordered_map>
#include <vector>
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Reflection/PropertyBase.h"
#include "Dynasti/Reflection/Reflectable.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    class ClassReflectionBase;
    using ClassReflectionPtr = std::shared_ptr<ClassReflectionBase>;

    class ClassRegistryBase;
    using ClassRegistryPtr = std::shared_ptr<ClassRegistryBase>;

    class ObjectPoolBase;
    using ObjectPoolPtr = std::shared_ptr<ObjectPoolBase>;

    //=================================================================================================================
    /// \brief   Base class for class reflection.
    /// \author  D. A. Hubber
    /// \date    03/07/2023
    //=================================================================================================================
    class ClassReflectionBase
    {
    public:

        ClassReflectionBase(const std::string& className, std::vector<std::shared_ptr<PropertyBase>> properties);

        virtual std::type_index GetType() = 0;

        virtual bool IsScriptableClass() = 0;

        virtual bool IsPoolableClass() = 0;

        virtual ObjectPoolPtr CreateObjectPool(ClassRegistryPtr classRegistry, std::uint32_t maxNumObjects) = 0;

        void CopyProperties(Reflectable* source, Reflectable* target);

        bool RegisterProperty(std::shared_ptr<PropertyBase> property);

        void RegisterBaseClass(ClassReflectionPtr baseClassReflectionPtr);

        [[nodiscard]] std::shared_ptr<PropertyBase> GetProperty(const std::string &propertyName) const;

        [[nodiscard]] const std::string& GetClassName() const { return className_; }

        [[nodiscard]] const std::unordered_map<std::string, std::shared_ptr<PropertyBase>>& GetPropertyMap() const { return propertyNameMap_; }

        [[nodiscard]] const std::unordered_map<std::string, ClassReflectionPtr>& GetBaseClassMap() const { return baseClassNameMap_; }

    protected:

        const std::string className_;
        std::unordered_map<std::string, std::shared_ptr<PropertyBase>> propertyNameMap_;
        std::unordered_multimap<std::type_index, std::shared_ptr<PropertyBase>> propertyTypeMap_;
        std::unordered_map<std::string, ClassReflectionPtr> baseClassNameMap_;
        std::unordered_map<std::type_index, ClassReflectionPtr> baseClassTypeMap_;

    };

    using ClassReflectionPtr = std::shared_ptr<ClassReflectionBase>;

}
//---------------------------------------------------------------------------------------------------------------------
#endif
