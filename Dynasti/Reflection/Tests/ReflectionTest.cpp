#include <memory>
#include <string>
#include <vector>
#include "doctest/doctest.h"
#include "Dynasti/Reflection/Property.h"
#include "Dynasti/Reflection/ClassRegistry.h"
#include "Dynasti/Reflection/Reflectable.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti::Tests
{
    class TestClass : public Reflectable
    {
    public:
        int i;
        float f;
        std::string s;

        TestClass(int i=0, float f=0.0f, const std::string&& s="")
            : i(i)
            , f(f)
            , s(s)
        {
        }

        std::type_index GetType() override { return std::type_index(typeid(TestClass)); }

        static std::string Name() { return "TestClass"; }
        static std::vector<std::string> BaseClasses() { return {}; }
        static std::vector<PropertyPtr> Properties()
        {
            return std::vector<PropertyPtr>{
                std::make_shared<BasicProperty<TestClass, int>>("i", &TestClass::i),
                std::make_shared<BasicProperty<TestClass, float>>("f", &TestClass::f),
                std::make_shared<BasicProperty<TestClass, std::string>>("s", &TestClass::s)
            };
        }

    };

    class TestClass2 : public TestClass
    {
    public:

        TestClass2(int i2=0, float f2=0.0f)
            : TestClass()
            , i2(i2)
            , f2(f2)
        {
        }

        std::type_index GetType() override { return std::type_index(typeid(TestClass2)); }

        int i2;
        float f2;

        static std::string Name() { return "TestClass2"; }
        static std::vector<std::string> BaseClasses() { return {"TestClass"}; }
        static std::vector<PropertyPtr> Properties()
        {
            return std::vector<PropertyPtr>{
                std::make_shared<BasicProperty<TestClass2, int>>("i2", &TestClass2::i2),
                std::make_shared<BasicProperty<TestClass2, float>>("f2", &TestClass2::f2)
            };
        }
    };

    class TestClass3 : public Reflectable
    {
    public:
        bool b;
        TestClass tc;

        TestClass3(bool b = 0, TestClass tc = {})
            : b(b)
            , tc(tc)
        {
        }

        std::type_index GetType() override { return std::type_index(typeid(TestClass3)); }

        static std::string Name() { return "TestClass3"; }
        static std::vector<std::string> BaseClasses() { return {}; }
        static std::vector<PropertyPtr> Properties()
        {
            return std::vector<PropertyPtr>{
                    std::make_shared<BasicProperty<TestClass3, bool>>("b", &TestClass3::b),
                    std::make_shared<ReflectedProperty<TestClass3, TestClass>>("tc", &TestClass3::tc)
            };
        }
    };


    TEST_CASE("Simple class member reflection")
    {
        // Verify static properties of the reflection class
        ClassRegistry classRegistry;
        auto testClassReflection = classRegistry.RegisterClass<TestClass>();
        REQUIRE_EQ(testClassReflection->GetClassName(), "TestClass");
        REQUIRE_EQ(testClassReflection->GetType(), std::type_index(typeid(TestClass)));

        // Verify member properties can be accessed by name and possess the correct type
        auto intProperty = testClassReflection->GetProperty("i");
        auto floatProperty = testClassReflection->GetProperty("f");
        auto stringProperty = testClassReflection->GetProperty("s");
        REQUIRE_EQ(intProperty->GetType(), std::type_index(typeid(int)));
        REQUIRE_EQ(floatProperty->GetType(), std::type_index(typeid(float)));
        REQUIRE_EQ(stringProperty->GetType(), std::type_index(typeid(std::string)));

        // Verify non-existing names return null pointers
        auto fakeProperty = testClassReflection->GetProperty("fake");
        REQUIRE_EQ(fakeProperty, nullptr);

        // Verify object property values can be obtained via the reflection class
        TestClass testObj{1, 2.0f, "Three"};
        REQUIRE_EQ(testClassReflection->GetPropertyValue<int>(&testObj, "i"), 1);
        REQUIRE_EQ(testClassReflection->GetPropertyValue<float>(&testObj, "f"), 2.0f);
        REQUIRE_EQ(testClassReflection->GetPropertyValue<std::string>(&testObj, "s"), "Three");

        REQUIRE_EQ(testClassReflection->GetPropertyValue<int>(&testObj, "i"), 1);
        REQUIRE_EQ(testClassReflection->GetPropertyValue<float>(&testObj, "f"), 2.0f);
        REQUIRE_EQ(testClassReflection->GetPropertyValue<std::string>(&testObj, "s"), "Three");

        SUBCASE("Serialization")
        {
            YAML::Emitter yamlEmitter;
            classRegistry.Serialize(yamlEmitter, &testObj);
            std::string dataString = yamlEmitter.c_str();
            YAML::Node yamlNode = YAML::Load(dataString);
            std::cout << "DATA " << std::endl << dataString << std::endl;

            TestClass testObjDeserialize;
            auto propertyData = yamlNode["TestClass"];
            classRegistry.Deserialize(propertyData, &testObjDeserialize);
            REQUIRE_EQ(testObjDeserialize.i, testObj.i);
            REQUIRE_EQ(testObjDeserialize.f, testObj.f);
            REQUIRE_EQ(testObjDeserialize.s, testObj.s);
        }

        // Verify object property values can be set via the reflection class
        testClassReflection->SetPropertyValue<int>(&testObj, "i", 4);
        testClassReflection->SetPropertyValue<float>(&testObj, "f", 5.0f);
        testClassReflection->SetPropertyValue<std::string>(&testObj, "s", std::string{"Six"});
        REQUIRE_EQ(testClassReflection->GetPropertyValue<int>(&testObj, "i"), 4);
        REQUIRE_EQ(testClassReflection->GetPropertyValue<float>(&testObj, "f"), 5.0f);
        REQUIRE_EQ(testClassReflection->GetPropertyValue<std::string>(&testObj, "s"), "Six");
    }

    TEST_CASE("Class member reflection with inheritance")
    {
        ClassRegistry classRegistry;
        auto testClassReflection = classRegistry.RegisterClass<TestClass>();
        auto testClass2Reflection = classRegistry.RegisterClass<TestClass2>();

        // Verify class reflection static properties
        REQUIRE_EQ(testClass2Reflection->GetClassName(), "TestClass2");
        REQUIRE_EQ(testClass2Reflection->GetType(), std::type_index(typeid(TestClass2)));

        // Verify we can access properties in both the current class and its base
        auto intProperty = testClassReflection->GetProperty("i");
        auto int2Property = testClassReflection->GetProperty("i2");

        TestClass2 testObj2{4, 5.0f};
        REQUIRE_EQ(testClass2Reflection->GetPropertyValue<int>(&testObj2, "i"), 0);
        REQUIRE_EQ(testClass2Reflection->GetPropertyValue<float>(&testObj2, "f"), 0.0f);
        REQUIRE_EQ(testClass2Reflection->GetPropertyValue<std::string>(&testObj2, "s"), "");
        REQUIRE_EQ(testClass2Reflection->GetPropertyValue<int>(&testObj2, "i2"), 4);
        REQUIRE_EQ(testClass2Reflection->GetPropertyValue<float>(&testObj2, "f2"), 5.0f);


        SUBCASE("Serialization")
        {
            YAML::Emitter yamlEmitter;
            classRegistry.Serialize(yamlEmitter, &testObj2);
            std::string dataString = yamlEmitter.c_str();
            YAML::Node yamlNode = YAML::Load(dataString);
            std::cout << "DATA " << std::endl << dataString << std::endl;

            TestClass2 testObjDeserialize;
            auto propertyData = yamlNode["TestClass2"];
            classRegistry.Deserialize(propertyData, &testObjDeserialize);
            REQUIRE_EQ(testObjDeserialize.i, testObj2.i);
            REQUIRE_EQ(testObjDeserialize.f, testObj2.f);
            REQUIRE_EQ(testObjDeserialize.s, testObj2.s);
            REQUIRE_EQ(testObjDeserialize.i2, testObj2.i2);
            REQUIRE_EQ(testObjDeserialize.f2, testObj2.f2);
        }
    }

    TEST_CASE("Class member reflection with reflected members")
    {
        // Verify reflected member properties can be accessed by name and possess the correct type
        ClassRegistry classRegistry;
        auto testClassReflection = classRegistry.RegisterClass<TestClass>();
        auto testClass2Reflection = classRegistry.RegisterClass<TestClass2>();
        auto testClass3Reflection = classRegistry.RegisterClass<TestClass3>();
        auto tcProperty = testClass3Reflection->GetProperty("tc");
        REQUIRE_EQ(tcProperty->GetType(), std::type_index(typeid(TestClass)));

        TestClass3 testObj3{true, {1, 2.0f, "Three"}};
        REQUIRE_EQ(testClass3Reflection->GetPropertyValue<bool>(&testObj3, "b"), true);
        auto tcObj = testClass3Reflection->GetPropertyValue<TestClass>(&testObj3, "tc");
        REQUIRE_EQ(tcObj.i, 1);
        REQUIRE_EQ(tcObj.f, 2.0f);
        REQUIRE_EQ(tcObj.s, "Three");

        SUBCASE("Serialization")
        {
            YAML::Emitter yamlEmitter;
            classRegistry.Serialize(yamlEmitter, &testObj3);
            std::string dataString = yamlEmitter.c_str();
            YAML::Node yamlNode = YAML::Load(dataString);
            std::cout << dataString << std::endl;
            TestClass3 testObj3Deserialize;
            auto propertyData = yamlNode["TestClass3"];
            classRegistry.Deserialize(propertyData, &testObj3Deserialize);
            REQUIRE_EQ(testObj3Deserialize.b, testObj3.b);
            REQUIRE_EQ(testObj3Deserialize.tc.i, testObj3.tc.i);
            REQUIRE_EQ(testObj3Deserialize.tc.f, testObj3.tc.f);
            REQUIRE_EQ(testObj3Deserialize.tc.s, testObj3.tc.s);

            const auto tempFileName = "TestClass3.yaml";
            std::ofstream fout(tempFileName);
            fout << yamlEmitter.c_str();
        }
    }

}
//---------------------------------------------------------------------------------------------------------------------