#include <glm/gtc/matrix_transform.hpp>
#include "Dynasti/Camera/Camera.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    void Camera::SetOrthographicCamera(Vec2 pos, Vec2 size, float angle)
    {
        pos_ = pos;
        size_ = size;
        angle_ = angle;
        UpdateMatrices();
    }

    bool Camera::ContainsPoint(const glm::vec3& r) const
    {
        for (int i = 0; i < 6; ++i)
        {
            if (frustumPlanes[i].DistanceFromPlane(r) < 0.0f) return false;
        }
        return true;
    }

    bool Camera::ContainsSphere(const glm::vec3& rCentre, const float radius) const
    {
        for (int i = 0; i < 6; ++i)
        {
            if (frustumPlanes[i].DistanceFromPlane(rCentre) < -radius) return false;
        }
        return true;
    }

    void Camera::UpdateMatrices()
    {
        // Update all other matrices and vectors
        const auto aspectRatio = size_.x / size_.y;
        auto pos = glm::vec3(pos_.x, pos_.y, 0.0f);
        auto right = glm::cross(look, up);
        auto target = pos + look;
        V_      = glm::lookAt(pos, target, up);
        PHud_   = glm::ortho(0.0f, size_.x, 0.0f, size_.y);
        P_      = glm::ortho(-0.5f * size_.x, 0.5f * size_.x, -0.5f * size_.y, 0.5f * size_.y, zNear, zFar);
        SetOrthographicFrustum(pos, look, up, right, zNear, zFar, size_.x, size_.y);
    }

    void Camera::SetOrthographicFrustum(const glm::vec3 &pos, const glm::vec3 &look, const glm::vec3 &up,
                                        const glm::vec3 &right, const float zNear, const float zFar,
                                        const float width, const float height)
    {
        const float halfHeight = 0.5f * height;            // Half-height of frustum
        const float halfWidth = 0.5f * width;              // Half-width of frustum
        const auto rNear = pos + look * zNear;
        const auto rFar = pos + look * zFar;

        // Set the positions of the corners of the far culling plane
        farPoints[0] = rFar + halfHeight * up - halfWidth * right;
        farPoints[1] = rFar - halfHeight * up - halfWidth * right;
        farPoints[2] = rFar - halfHeight * up + halfWidth * right;
        farPoints[3] = rFar + halfHeight * up + halfWidth * right;

        // Set the position of the corners of the near culling plane
        nearPoints[0] = rNear + halfHeight * up - halfWidth * right;
        nearPoints[1] = rNear - halfHeight * up - halfWidth * right;
        nearPoints[2] = rNear - halfHeight * up + halfWidth * right;
        nearPoints[3] = rNear + halfHeight * up + halfWidth * right;

        frustumPlanes[0].CalculatePlaneFromPoints(nearPoints[3], nearPoints[0], farPoints[0]);
        frustumPlanes[1].CalculatePlaneFromPoints(nearPoints[1], nearPoints[2], farPoints[2]);
        frustumPlanes[2].CalculatePlaneFromPoints(nearPoints[0], nearPoints[1], farPoints[1]);
        frustumPlanes[3].CalculatePlaneFromPoints(nearPoints[2], nearPoints[3], farPoints[2]);
        frustumPlanes[4].CalculatePlaneFromPoints(nearPoints[0], nearPoints[3], nearPoints[2]);
        frustumPlanes[5].CalculatePlaneFromPoints(farPoints[3], farPoints[0], farPoints[1]);
    }

}
//---------------------------------------------------------------------------------------------------------------------
