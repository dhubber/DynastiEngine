#include <glm/glm.hpp>
#include "doctest/doctest.h"
#include "Dynasti/Camera/Camera.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    TEST_CASE("Camera")
    {
        glm::vec3 pos;
        Camera camera;
        camera.SetOrthographicCamera({0.0f, 0.0f}, {1.0f, 1.0f}, 0.f);

        SUBCASE("Frustum checks")
        {
            REQUIRE_EQ(camera.ContainsPoint(glm::vec3(0.0f, 0.0f, 0.0f)), true);
            REQUIRE_EQ(camera.ContainsPoint(glm::vec3(0.0f, 0.0f, 999.0f)), true);
            REQUIRE_EQ(camera.ContainsPoint(glm::vec3(0.0f, 0.0f, 1001.0f)), false);
            REQUIRE_EQ(camera.ContainsPoint(glm::vec3(0.0f, 0.0f, -999.0f)), true);
            REQUIRE_EQ(camera.ContainsPoint(glm::vec3(0.0f, 0.0f, -1001.0f)), false);
        }

    }
    

}
//---------------------------------------------------------------------------------------------------------------------
