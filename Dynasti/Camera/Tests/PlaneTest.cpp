#include "doctest/doctest.h"
#include <glm/glm.hpp>
#include "Dynasti/Camera/Plane.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    TEST_CASE("Plane")
    {
        
        // Different planes to test the orientation tests on
        Plane xPlane{glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f)};
        Plane yPlane{glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f)};
        Plane zPlane{glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)};

        SUBCASE("Plane Orientation")
        {
            REQUIRE_EQ(xPlane.ComputeOrientation(glm::vec3(-1.0f, 0.0f, 0.0f)), PlaneOrientation::Back);
            REQUIRE_EQ(xPlane.ComputeOrientation(glm::vec3(0.0f, 0.0f, 0.0f)), PlaneOrientation::Coplanar);
            REQUIRE_EQ(xPlane.ComputeOrientation(glm::vec3(1.0f, 0.0f, 0.0f)), PlaneOrientation::Front);

            REQUIRE_EQ(yPlane.ComputeOrientation(glm::vec3(0.0f, -1.0f, 0.0f)), PlaneOrientation::Back);
            REQUIRE_EQ(yPlane.ComputeOrientation(glm::vec3(0.0f, 0.0f, 0.0f)), PlaneOrientation::Coplanar);
            REQUIRE_EQ(yPlane.ComputeOrientation(glm::vec3(0.0f, 1.0f, 0.0f)), PlaneOrientation::Front);

            REQUIRE_EQ(zPlane.ComputeOrientation(glm::vec3(0.0f, 0.0f, -1.0f)), PlaneOrientation::Back);
            REQUIRE_EQ(zPlane.ComputeOrientation(glm::vec3(0.0f, 0.0f, 0.0f)), PlaneOrientation::Coplanar);
            REQUIRE_EQ(zPlane.ComputeOrientation(glm::vec3(0.0f, 0.0f, 1.0f)), PlaneOrientation::Front);
        }

        SUBCASE("Plane Distance")
        {
            REQUIRE((xPlane.DistanceFromPlane(glm::vec3(-1.0f, 0.0f, 0.0f)) == doctest::Approx(-1.0f)));
            REQUIRE((xPlane.DistanceFromPlane(glm::vec3(0.0f, 0.0f, 0.0f)) == doctest::Approx(0.0f)));
            REQUIRE((xPlane.DistanceFromPlane(glm::vec3(1.0f, 0.0f, 0.0f)) == doctest::Approx(1.0f)));

            REQUIRE((yPlane.DistanceFromPlane(glm::vec3(0.0f, -1.0f, 0.0f)) == doctest::Approx(-1.0f)));
            REQUIRE((yPlane.DistanceFromPlane(glm::vec3(0.0f, 0.0f, 0.0f)) == doctest::Approx(0.0f)));
            REQUIRE((yPlane.DistanceFromPlane(glm::vec3(0.0f, 1.0f, 0.0f)) == doctest::Approx(1.0f)));

            REQUIRE((zPlane.DistanceFromPlane(glm::vec3(0.0f, 0.0f, -1.0f)) == doctest::Approx(-1.0f)));
            REQUIRE((zPlane.DistanceFromPlane(glm::vec3(0.0f, 0.0f, 0.0f)) == doctest::Approx(0.0f)));
            REQUIRE((zPlane.DistanceFromPlane(glm::vec3(0.0f, 0.0f, 1.0f)) == doctest::Approx(1.0f)));
        }
    }

}
//---------------------------------------------------------------------------------------------------------------------
