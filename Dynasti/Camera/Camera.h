#ifndef DYNASTI_CAMERA_H
#define DYNASTI_CAMERA_H

#include <array>
#include <glm/glm.hpp>
#include "Dynasti/Camera/Plane.h"
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Reflection/Property.h"
#include "Dynasti/Reflection/Reflectable.h"
#include "Dynasti/Vector/Vector.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Main camera class.
    /// \author  D. A. Hubber
    /// \date    12/11/2022
    //=================================================================================================================
    class Camera : public Reflectable
    {
    public:

        // Static values of camera properties
        static constexpr float zNear = -1000.0f;
        static constexpr float zFar = 1000.0f;
        static constexpr glm::vec3 look = glm::vec3(0.0f, 0.0f, -1.0f);
        static constexpr glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);

        std::type_index GetType() override { return std::type_index(typeid(Camera)); }
        static std::string Name() { return "Camera"; }
        static std::vector<std::string> BaseClasses() { return {}; }
        static std::vector<PropertyPtr> Properties()
        {
            return std::vector<PropertyPtr>{
                std::make_shared<BasicProperty<Camera, Vec2>>("pos", &Camera::pos_),
                std::make_shared<BasicProperty<Camera, Vec2>>("size", &Camera::size_),
                std::make_shared<BasicProperty<Camera, float>>("angle", &Camera::angle_),
            };
        }

        /// Sets the camera to use an orthographic projection (e.g. for use in the editor or 2d games)
        /// \param[in] pos - Position of camera in world space
        /// \param[in] xSize - Physical width of camera viewport
        /// \param[in] angle - Angle (around z-axis) of camera viewport
        void SetOrthographicCamera(Vec2 pos, Vec2 size, float angle);

        /// Calculates exactly if a given point is within the view Frustum by comparing the orientation
        /// of the point to each of the Frustum planes. Returns true if inside the Frustum; else false.
        /// \param[in] r - Position of point
        /// \return - True if point is contained within frustum volume; otherwise false
        bool ContainsPoint(const glm::vec3& r) const;

        /// Calculates if a given sphere is within the view Frustum by checking that the distance of the sphere
        /// centre is greater than its radius away from each of the Frustum planes.
        /// \param[in] rCentre - Position of the centre of the sphere
        /// \param[in] radius - Radius of the sphere
        /// \return - True if any part of the sphere is inside the frustum; otherwise false
        bool ContainsSphere(const glm::vec3& rCentre, float radius) const;
        
        // Getters
        inline const glm::mat4& GetHudProjection() const { return PHud_; }
        inline const glm::mat4& GetProjectionMatrix() const { return P_; }
        inline const glm::mat4& GetViewMatrix() const { return V_; }

    protected:

        Vec2 pos_{0.0f, 0.0f};                             ///< Position of camera
        Vec2 size_{1.0f, 1.0f};                             ///< x-y size of camera
        float angle_ = 0.0f;                               ///< Angle around z-axis of camera
        glm::mat4 V_;                                      ///< View matrix
        glm::mat4 P_;                                      ///< Projection matrix
        glm::mat4 PHud_;                                   ///< HUD projection matrix
        glm::mat4 R_;                                      ///< Camera Euler angle rotation matrix
        std::array<glm::vec3,4> farPoints;                 ///< Points defining far clipping plane
        std::array<glm::vec3,4> nearPoints;                ///< Points defining near clipping plane
        std::array<Plane,6> frustumPlanes;                 ///< Frustum planes

        /// Updates the perspective and view matrices, as well as the various directional vectors (e.g. up, right).
        void UpdateMatrices();

        /// Calculates the frustum planes for an orthographic camera with the given parameters.
        /// If using the RHS rule, all planes point INWARDS towards centre of Frustum volume
        /// \param[in] pos - Camera position
        /// \param[in] look - Camera viewing direction
        /// \param[in] up - Camera up direction
        /// \param[in] right -  Camera right direction
        /// \param[in] zNear - Distance of near clipping plane
        /// \param[in] zFar - Distance of far clipping plane
        /// \param[in] width - Frustum width
        /// \param[in] height - Frustum height
        void SetOrthographicFrustum(const glm::vec3 &pos, const glm::vec3 &look, const glm::vec3 &up,
                                    const glm::vec3 &right, float zNear, float zFar, float width, float height);

    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif