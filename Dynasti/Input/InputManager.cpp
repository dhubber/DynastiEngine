#include <SDL_keyboard.h>
#include "yaml-cpp/yaml.h"
#include "Dynasti/Input/InputManager.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    InputManager::InputManager(std::shared_ptr<EventManager> eventManager)
        : eventManager_(eventManager)
        , activeInputMap_(nullptr)
    {
        DYNASTI_LOG_VERBOSE("Constructing InputManager object");
    }


    std::shared_ptr<InputMap> InputManager::FindOrCreateInputMap(const std::string& inputMapName)
    {
        auto inputMap = FindInputMap(inputMapName);
        return (inputMap == nullptr) ? CreateInputMap(inputMapName) : inputMap;
    }


    std::shared_ptr<InputMap> InputManager::CreateInputMap(const std::string& inputMapName)
    {
        DYNASTI_ASSERT(FindInputMap(inputMapName) == nullptr, "Input map name already registered: " + inputMapName);
        std::shared_ptr<InputMap> newInputMap = std::make_shared<InputMap>(inputMapName, eventManager_);
        inputMapNames_[inputMapName] = newInputMap;
        if (activeInputMap_ == nullptr)
            activeInputMap_ = newInputMap;
        return newInputMap;
    }
    
    
    std::shared_ptr<InputMap> InputManager::FindInputMap(const std::string& inputMapName)
    {
        auto it = inputMapNames_.find(inputMapName);
        if (it != std::end(inputMapNames_))
        {
            return it->second;
        }
        return nullptr;
    }
    
    
    bool InputManager::SelectActiveMap(const std::string& inputMapName)
    {
        auto it = inputMapNames_.find(inputMapName);
        if (it != std::end(inputMapNames_))
        {
            activeInputMap_ = it->second;
            return true;
        }
        return false;
    }
    
    
    void InputManager::OnKeyDown(KeyCode keyCode)
    {
        if (activeInputMap_)
        {
            activeInputMap_->OnKeyDown(keyCode);
        }
    }
    
    
    void InputManager::OnKeyUp(const KeyCode keyCode)
    {
        if (activeInputMap_)
        {
            activeInputMap_->OnKeyUp(keyCode);
        }
    }


    void InputManager::Serialize(YAML::Emitter& emitter)
    {
        DYNASTI_LOG_VERBOSE("Serializing InputManager Settings");
        for (const auto& mapIt : inputMapNames_) {
            emitter << YAML::BeginMap;
            emitter << YAML::Key << mapIt.first << YAML::Value;
            mapIt.second->Serialize(emitter);
            emitter << YAML::EndMap;
        }
    }


    void InputManager::Deserialize(YAML::Node &inputData)
    {
        if (!inputData)
            return;

        for (auto inputMapData : inputData)
        {
            const auto mapName = inputMapData.first.as<std::string>();
            auto mapData = inputMapData.second;
            auto newInputMap = FindOrCreateInputMap(mapName);
            DYNASTI_ASSERT(newInputMap, "Null pointer to input map");
            newInputMap->Deserialize(mapData);
        }
    }

}
//---------------------------------------------------------------------------------------------------------------------