#include "Dynasti/Input/InputMap.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    InputMap::InputMap(const std::string& name, std::shared_ptr<EventManager> eventManager)
        : name_(name)
        , eventManager_(eventManager)
    {
        DYNASTI_LOG_VERBOSE("Constructing new input mapping object : " + name);
        DYNASTI_ASSERT(eventManager_, "Invalid pointer to event manager");
    }


    bool InputMap::CreateActionKeyBindings(const std::string& actionName, const std::vector<KeyCode>& keyCodes)
    {
        auto action = FindOrCreateAction(actionName);
        for (const auto keyCode : keyCodes)
        {
            DYNASTI_WARNING_IF(FindActionByKeyCode(keyCode) != nullptr, "KeyCode " + std::to_string(keyCode) + " already assigned to existing action. Deleting existing mapping.");
            actionKeys_[keyCode] = action;
        }

        return true;
    }


    std::shared_ptr<Action> InputMap::FindOrCreateAction(const std::string &actionName)
    {
        auto action = FindActionByName(actionName);
        return (action == nullptr) ? CreateNewAction(actionName) : action;
    }


    std::shared_ptr<Action> InputMap::CreateNewAction(const std::string &actionName)
    {
        DYNASTI_ASSERT(FindActionByName(actionName) == nullptr, "Action name already registered");
        const std::string actionKeyDownEventName = actionName + "ActionKeyDown";
        const std::string actionKeyUpEventName = actionName + "ActionKeyUp";
        const auto actionKeyDownEventType = eventManager_->RegisterEvent(actionKeyDownEventName);
        const auto actionKeyUpEventType = eventManager_->RegisterEvent(actionKeyUpEventName);
        std::shared_ptr<Action> newAction = std::make_shared<Action>(actionName, actionKeyDownEventType, actionKeyUpEventType);
        actionNames_[actionName] = newAction;
        return newAction;
    }


    std::shared_ptr<Action> InputMap::FindActionByKeyCode(KeyCode keyCode)
    {
        auto it = actionKeys_.find(keyCode);
        return it != std::end(actionKeys_) ? it->second : nullptr;
    }


    std::shared_ptr<Action> InputMap::FindActionByName(const std::string &name)
    {
        auto it = actionNames_.find(name);
        return it != std::end(actionNames_) ? it->second : nullptr;
    }


    void InputMap::OnKeyDown(const KeyCode keyCode)
    {
        auto action = FindActionByKeyCode(keyCode);
        if (action)
        {
            const EventType actionKeyDownEventType = action->GetEventOnKeyDown(keyCode);
            if (actionKeyDownEventType != -1)
            {
                const Event actionKeyDownEvent(actionKeyDownEventType);
                eventManager_->BroadcastEvent(actionKeyDownEvent);
            }
        }
    }


    void InputMap::OnKeyUp(const KeyCode keyCode)
    {
        auto action = FindActionByKeyCode(keyCode);
        if (action)
        {
            const EventType actionKeyUpEventType = action->GetEventOnKeyUp(keyCode);
            if (actionKeyUpEventType != -1)
            {
                const Event actionKeyUpEvent(actionKeyUpEventType);
                eventManager_->BroadcastEvent(actionKeyUpEvent);
            }
        }
    }


    bool InputMap::IsActionPressed(const std::string& actionName)
    {
        auto action = FindActionByName(actionName);
        return action != nullptr ? action->IsActionKeyPressed() : false;
    }


    void InputMap::Serialize(YAML::Emitter &yamlEmitter)
    {
        yamlEmitter << YAML::BeginMap;
        for (const auto& it : actionKeys_)
        {
            yamlEmitter << YAML::Key << SDL_GetKeyName(it.first) << YAML::Key << it.second->GetName();
        }
        yamlEmitter << YAML::EndMap;
    }


    void InputMap::Deserialize(YAML::Node &yamlNode)
    {
        for (auto actionData : yamlNode)
        {
            const std::string actionKeyName = actionData.first.as<std::string>();
            const std::string actionName = actionData.second.as<std::string>();
            const auto actionKeyCode = static_cast<KeyCode>(SDL_GetKeyFromName(actionKeyName.c_str()));
            auto result = CreateActionKeyBindings(actionName, {actionKeyCode});
        }
    }

}
//---------------------------------------------------------------------------------------------------------------------