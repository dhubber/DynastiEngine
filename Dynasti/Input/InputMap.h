#ifndef DYNASTI_INPUT_MAP_H
#define DYNASTI_INPUT_MAP_H


#include <map>
#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <SDL_keyboard.h>
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Event/EventManager.h"
#include "Dynasti/Input/Action.h"
#include "Dynasti/Serialization/SerializableBase.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Class defining a single mapping from the input to broadcasted input events.
    /// \author  D. A. Hubber
    /// \date    09/09/2020
    //=================================================================================================================
    class InputMap : public SerializableBase
    {
    public:
        
        InputMap(const std::string& name, std::shared_ptr<EventManager> eventManager);

        // Required functions for SerializableBase
        void Serialize(YAML::Emitter &yamlEmitter) override;
        void Deserialize(YAML::Node &yamlNode) override;

        /// Registers a new action associating a keyboard (SDL) input code with an key input event
        /// \param[in] actionName - Name of input action event
        /// \param[in] keyCodes - Array of keycodes that are registered to trigger an action
        /// \return - True if the input action has been successfully registered; otherwise returns false
        bool CreateActionKeyBindings(const std::string& actionName, const std::vector<KeyCode>& keyCodes={});

        /// Finds an existing action object with the given name; otherwise creates a brand new object
        /// \param[in] actionName - Human-readable name of action
        /// \return - Pointer to action object
        std::shared_ptr<Action> FindOrCreateAction(const std::string& actionName);

        /// Returns a pointer to the action associated with the SDL keycode
        /// \param[in] keyCode - SDL keycode
        /// \return - Pointer to action bound to given keycode; if none found, then returns null
        std::shared_ptr<Action> FindActionByKeyCode(KeyCode keyCode);

        /// Returns a pointer to the action registered with a given name
        /// \param[in] name - Human-readable name of action
        /// \return - Pointer to action registered with given name; if none found, then returns null
        std::shared_ptr<Action> FindActionByName(const std::string& name);

        /// Called when a key-down event has been broadcast; updates any registered key with the correct scancode.
        /// \param[in] keyCode - KeyCode recorded by key-down event
        void OnKeyDown(KeyCode keyCode);
    
        /// Called when a key-up event has been broadcast; updates any registered key with the correct scancode.
        /// \param[in] keyCode - KeyCode recorded by key-up event
        void OnKeyUp(KeyCode keyCode);

        /// \param[in] keyActionName - Name of key-action being polled
        /// \return - True if any key associated with action is being pressed; otherwise returns false
        bool IsActionPressed(const std::string& keyActionName);

        
    private:
    
        std::string name_;                                                     ///< Name of input mapping
        std::shared_ptr<EventManager> eventManager_;                           ///< Main event manager object
        std::unordered_map<std::string, std::shared_ptr<Action>> actionNames_; ///< ..
        std::unordered_map<KeyCode, std::shared_ptr<Action>> actionKeys_;      ///< ..

        /// Returns pointer to newly created action object associated with a given unique name.
        /// Also registers any required events (e.g. KeyUp, KeyDown) with the event manager.
        /// \param[in] actionName - Unique human readable name of action
        /// \return - Pointer to new action object
        std::shared_ptr<Action> CreateNewAction(const std::string& actionName);

    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
