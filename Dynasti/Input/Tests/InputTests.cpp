#include "doctest/doctest.h"
#include "Dynasti/Event/EventListener.h"
#include "Dynasti/Input/InputManager.h"
#include "Dynasti/Input/InputMap.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    TEST_CASE("InputMap")
    {
        std::shared_ptr<EventManager> eventManager = std::make_shared<EventManager>();
        InputMap inputMap("TestInputMap", eventManager);

        // Verify various actions are not created and scan codes are not registered
        REQUIRE_EQ(eventManager->GetNumEventTypes(), 0);
        REQUIRE_EQ(inputMap.FindActionByKeyCode(SDLK_a), nullptr);
        REQUIRE_EQ(inputMap.FindActionByKeyCode(SDLK_SPACE), nullptr);
        REQUIRE_EQ(inputMap.FindActionByName("Left"), nullptr);
        REQUIRE_EQ(inputMap.FindActionByName("Fire"), nullptr);

        // Register some key actions and verify the keys are correctly created
        REQUIRE_EQ(inputMap.CreateActionKeyBindings("Left", {SDLK_a}), true);
        REQUIRE_EQ(inputMap.CreateActionKeyBindings("Fire", {SDLK_SPACE}), true);

        auto leftAction = inputMap.FindActionByName("Left");
        auto fireAction = inputMap.FindActionByName("Fire");
        REQUIRE_NE(leftAction, nullptr);
        REQUIRE_NE(fireAction, nullptr);
        REQUIRE_EQ(inputMap.FindActionByKeyCode(SDLK_a), leftAction);
        REQUIRE_EQ(inputMap.FindActionByKeyCode(SDLK_SPACE), fireAction);

        // Verify the key-up and key-down events are correctly registered with the event manager
        SUBCASE("Input events")
        {
            REQUIRE_EQ(eventManager->GetNumEventTypes(), 4);
            REQUIRE_EQ(eventManager->GetEventType("LeftActionKeyDown"), leftAction->GetActionKeyDownEventType());
            REQUIRE_EQ(eventManager->GetEventType("LeftActionKeyUp"), leftAction->GetActionKeyUpEventType());
            REQUIRE_EQ(eventManager->GetEventType("FireActionKeyDown"), fireAction->GetActionKeyDownEventType());
            REQUIRE_EQ(eventManager->GetEventType("FireActionKeyUp"), fireAction->GetActionKeyUpEventType());

            // Verify key-up and key-down events are correctly broadcast
            std::uint32_t keyDownCount = 0;
            std::uint32_t keyUpCount = 0;
            std::shared_ptr<EventListener> eventListener_ = std::make_shared<EventListener>(eventManager);
            eventListener_->SubscribeToEvent("LeftActionKeyDown", [&keyDownCount](Event event) { ++keyDownCount; });
            auto keyUpLambda = [&keyUpCount](Event event) { ++keyUpCount; };
            eventListener_->SubscribeToEvent("LeftActionKeyUp", keyUpLambda);
            REQUIRE_EQ(leftAction->IsActionKeyPressed(), false);
            inputMap.OnKeyDown(SDLK_a);
            REQUIRE_EQ(leftAction->IsActionKeyPressed(), true);
            REQUIRE_EQ(keyDownCount, 1);
            REQUIRE_EQ(keyUpCount, 0);
            inputMap.OnKeyUp(SDLK_a);
            REQUIRE_EQ(leftAction->IsActionKeyPressed(), false);
            REQUIRE_EQ(keyDownCount, 1);
            REQUIRE_EQ(keyUpCount, 1);
        }

        SUBCASE("Serialization")
        {
            YAML::Emitter emitter;
            inputMap.Serialize(emitter);
            std::string dataString = emitter.c_str();
            YAML::Node yamlNode = YAML::Load(dataString);
            REQUIRE_EQ(yamlNode["Space"].as<std::string>(), "Fire");
            REQUIRE_EQ(yamlNode["A"].as<std::string>(), "Left");

            InputMap inputMap2("TestInputMap2", eventManager);
            inputMap2.Deserialize(yamlNode);
            REQUIRE_NE(inputMap2.FindActionByName("Fire"), nullptr);
            REQUIRE_NE(inputMap2.FindActionByName("Left"), nullptr);
        }
    }

    TEST_CASE("InputManager")
    {
        // Verify there are no existing input maps when creating the input manager
        std::shared_ptr<EventManager> eventManager = std::make_shared<EventManager>();
        InputManager inputManager(eventManager);
        REQUIRE_EQ(inputManager.GetActiveInputMap(), nullptr);

        // Verify that the first created input map is active by default
        std::shared_ptr<InputMap> inputMap1 = inputManager.FindOrCreateInputMap("InputMap1");
        inputMap1->CreateActionKeyBindings("Right", {SDLK_d});
        inputMap1->CreateActionKeyBindings("Quit", {SDLK_ESCAPE});

        // Verify the key-up and key-down events are correctly registered with the event manager
        SUBCASE("Managing input maps")
        {
            REQUIRE_EQ(inputManager.GetActiveInputMap(), inputMap1);
            REQUIRE_EQ(inputManager.FindInputMap("InputMap1"), inputMap1);
            REQUIRE_EQ(inputManager.FindInputMap("AnotherInputMap"), nullptr);

            // Verify that creating further maps does not change the active map
            std::shared_ptr<InputMap> inputMap2 = inputManager.FindOrCreateInputMap("AnotherInputMap");
            REQUIRE_EQ(inputManager.GetActiveInputMap(), inputMap1);
            REQUIRE_EQ(inputManager.FindInputMap("InputMap1"), inputMap1);
            REQUIRE_EQ(inputManager.FindInputMap("AnotherInputMap"), inputMap2);

            // Verify functions for changing the active map
            REQUIRE_EQ(inputManager.SelectActiveMap("NonExistingInputMap"), false);
            REQUIRE_EQ(inputManager.GetActiveInputMap(), inputMap1);
            REQUIRE_EQ(inputManager.SelectActiveMap("AnotherInputMap"), true);
            REQUIRE_EQ(inputManager.GetActiveInputMap(), inputMap2);
        }

        SUBCASE("Serialization")
        {
            YAML::Emitter emitter;
            inputManager.Serialize(emitter);
            std::string dataString = emitter.c_str();
            YAML::Node yamlNode = YAML::Load(dataString);

            InputManager inputManager2(eventManager);
            REQUIRE_EQ(inputManager2.GetActiveInputMap(), nullptr);
            inputManager2.Deserialize(yamlNode);
            auto inputMap2 = inputManager2.FindInputMap("InputMap1");
            REQUIRE_NE(inputMap2->FindActionByName("Right"), nullptr);
            REQUIRE_NE(inputMap2->FindActionByName("Quit"), nullptr);
        }
    }

}
//---------------------------------------------------------------------------------------------------------------------