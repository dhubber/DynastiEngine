#ifndef DYNASTI_ACTION_H
#define DYNASTI_ACTION_H


#include <unordered_set>
#include <vector>
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Class defining a single mapping from the input key codes to broadcasted input events.
    /// \author  D. A. Hubber
    /// \date    11/10/2022
    //=================================================================================================================
    class Action
    {
    public:

        Action(const std::string &name, EventType actionKeyDownEventType, EventType actionKeyUpEventType)
            : name_(name)
            , actionKeyDownEventType_(actionKeyDownEventType)
            , actionKeyUpEventType_(actionKeyUpEventType)
        {
        }

        /// \param[in] keyCode - Key code of registered key bound to action
        /// \return - Event type to be broadcast by event manager when an action key is pressed
        EventType GetEventOnKeyDown(KeyCode keyCode)
        {
            keysDownSet_.insert(keyCode);
            return keysDownSet_.size() == 1 ? actionKeyDownEventType_ : -1;
        }

        /// \param[in] keyCode - Key code of registered key bound to action
        /// \return - Event type to be broadcast by event manager when an action key is pressed
        EventType GetEventOnKeyUp(KeyCode keyCode)
        {
            keysDownSet_.erase(keyCode);
            return keysDownSet_.size() == 0 ? actionKeyUpEventType_ : -1;
        }

        // Getters
        const std::string& GetName() const { return name_; }
        EventType GetActionKeyDownEventType() const { return actionKeyDownEventType_; }
        EventType GetActionKeyUpEventType() const { return actionKeyUpEventType_; }
        bool IsActionKeyPressed() const { return keysDownSet_.size() > 0; }


    protected:

        const std::string name_;                           ///< Human-readable name of action
        const EventType actionKeyDownEventType_;           ///< Global event type to broadcast when key is pressed
        const EventType actionKeyUpEventType_;             ///< Global event type to broadcast when key is released
        std::unordered_set<KeyCode> keysDownSet_;          ///< Currently pressed keys registered to action

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif