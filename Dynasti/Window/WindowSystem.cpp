#include <cstring>
#include "glad/glad.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Event/EventManager.h"
#include "Dynasti/Core/FrameTimer.h"
#include "Dynasti/Input/InputManager.h"
#include "Dynasti/Window/WindowSystem.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    void WindowSystem::Setup()
    {
        SetupSdl();
        SetupOpenGL();
        window_ = std::make_shared<Window>("Main Window", 640, 640, 1);

        // Initialise Glad
        if (!gladLoadGLLoader((GLADloadproc) SDL_GL_GetProcAddress))
        {
            DYNASTI_FATAL("Error initialising GLAD");
        }

        quitAppEventType_ = eventManager_->RegisterEvent("QuitApp");
    }
    
    
    void WindowSystem::Shutdown()
    {
        SDL_Quit();
    }
    
    
    void WindowSystem::Update()
    {
        DYNASTI_TIMER(WindowSystem)
        SDL_Event sdlEvent;
        
        while (SDL_PollEvent(&sdlEvent) != 0)
        {
            OnSdlEvent(sdlEvent);
        }

        window_->SetTitle("FPS: " + std::to_string(frameTimer_->GetFps()));

        DYNASTI_TIMER(SwappingBuffers)
        window_->SwapBuffers();
    }
    
    
    void WindowSystem::SetupSdl()
    {
        int sdlStatus;
        std::string sdlError;
    
        // System information
        std::string platformString = SDL_GetPlatform();
        DYNASTI_LOG("Detected platform        : " + platformString);
        DYNASTI_LOG("No. of logical CPU cores : " + std::to_string(SDL_GetCPUCount()));
        DYNASTI_LOG("System RAM (MB)          : " + std::to_string(SDL_GetSystemRAM()));
        DYNASTI_LOG("L1 Cache (B)             : " + std::to_string(SDL_GetCPUCacheLineSize()));
    
        // SDL version information
        SDL_VERSION(&sdlCompiled_);
        SDL_GetVersion(&sdlLinked_);
        DYNASTI_LOG("Compiled with SDL version : " + std::to_string(sdlCompiled_.major) + "." + std::to_string(sdlCompiled_.minor) + "." + std::to_string(sdlCompiled_.patch))
        DYNASTI_LOG("Linked with SDL version   : " + std::to_string(sdlLinked_.major) + "." + std::to_string(sdlLinked_.minor) + "." + std::to_string(sdlLinked_.patch))
    
        // Ensure that code is compiled with at least version 2.0.10
        sdlStatus = SDL_VERSION_ATLEAST(2, 0, 8);
        DYNASTI_ASSERT(sdlStatus, "Compiled SDL version is less than 2.0.8");
    
        // Initialise SDL
        sdlStatus = SDL_Init(SDL_INIT_EVERYTHING);
        if (sdlStatus < 0)
        {
            sdlError = SDL_GetError();
            DYNASTI_FATAL("SDL could not initialise : " + sdlError);
        }
        else
        {
            DYNASTI_LOG("SDL initialised successfully");
        }
    }


    void WindowSystem::SetupOpenGL()
    {
        int sdlStatus;
        std::string sdlError;

        DYNASTI_LOG("Setting OpenGL 4.6 attributes");
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 6);

        // Set other attributes
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

        // Video display information
        numDisplays_ = SDL_GetNumVideoDisplays();
        if (numDisplays_ < 1)
        {
            DYNASTI_FATAL("SDL could not detect any video displays");
        }
        DYNASTI_LOG("No. of SDL video displays : " + std::to_string(numDisplays_));

        // Available display modes
        sdlStatus = SDL_GetCurrentDisplayMode(0, &sdlDisplayMode_);
        if (sdlStatus != 0)
        {
            sdlError = SDL_GetError();
            DYNASTI_FATAL("SDL could not read current display mode : " + sdlError);
        }

        displayWidth_  = sdlDisplayMode_.w;
        displayHeight_ = sdlDisplayMode_.h;
        DYNASTI_LOG("Window width : " + std::to_string(displayWidth_) + "   height : " + std::to_string(displayHeight_));
    }
    
    
    bool WindowSystem::OnSdlEvent(const SDL_Event &sdlEvent)
    {
        switch (sdlEvent.type)
        {
            case SDL_AUDIODEVICEADDED:
            {
                DYNASTI_LOG_VERBOSE("SDL_Event: Audio device added");
                return true;
            }
            case SDL_AUDIODEVICEREMOVED:
                DYNASTI_LOG_VERBOSE("SDL_Event: Audio device removed");
                break;
        
            case SDL_CONTROLLERAXISMOTION:
                DYNASTI_LOG_VERBOSE("SDL_Event: Controller axis motion");
                break;
        
            case SDL_CONTROLLERBUTTONDOWN:
                DYNASTI_LOG_VERBOSE("SDL_Event: Controller button down!");
                break;
        
            case SDL_CONTROLLERBUTTONUP:
                DYNASTI_LOG_VERBOSE("SDL_Event: Controller button up");
                break;
        
            case SDL_CONTROLLERDEVICEADDED:
                DYNASTI_LOG_VERBOSE("SDL_Event: Controller device added : " + std::to_string(sdlEvent.cdevice.which));
                break;
        
            case SDL_CONTROLLERDEVICEREMAPPED:
                DYNASTI_LOG_VERBOSE("SDL_Event: Controller device remapped");
                break;
        
            case SDL_CONTROLLERDEVICEREMOVED:
                DYNASTI_LOG_VERBOSE("SDL_Event: Controller device removed : " + std::to_string(sdlEvent.cdevice.which));
                break;
    
            case SDL_CONTROLLERTOUCHPADDOWN:
            case SDL_CONTROLLERTOUCHPADMOTION:
            case SDL_CONTROLLERTOUCHPADUP:
                DYNASTI_LOG_VERBOSE("SDL_Event: Controller touchpad event: " + std::to_string(sdlEvent.cdevice.which));
                break;
    
            case SDL_CONTROLLERSENSORUPDATE:
                DYNASTI_LOG_VERBOSE("SDL_Event: Controller sensor update event: " + std::to_string(sdlEvent.cdevice
                .which));
                break;
                
            case SDL_DISPLAYEVENT:
                DYNASTI_LOG_VERBOSE("SDL_Event: Display Event : " + std::to_string(sdlEvent.jaxis.which) + "  " +
                std::to_string
                (sdlEvent.jaxis.axis) + "  " + std::to_string(sdlEvent.jaxis.value));
                break;
        
            case SDL_JOYAXISMOTION:
                DYNASTI_LOG_VERBOSE("SDL_Event: Joystick axis motion : " + std::to_string(sdlEvent.jaxis.which) + "  " + std::to_string(sdlEvent.jaxis.axis) + "  " + std::to_string(sdlEvent.jaxis.value));
                break;
        
            case SDL_JOYBALLMOTION:
                DYNASTI_LOG_VERBOSE("SDL_Event: Joystick ball motion detected");
                break;
        
            case SDL_JOYBUTTONDOWN:
                DYNASTI_LOG_VERBOSE("SDL_Event: Joystick button down : " + std::to_string(sdlEvent.jbutton.which) + "  " + std::to_string(sdlEvent.jbutton.button));
                break;
        
            case SDL_JOYBUTTONUP:
                DYNASTI_LOG_VERBOSE("SDL_Event: Joystick button up : " + std::to_string(sdlEvent.jbutton.which) + "  " + std::to_string(sdlEvent.jbutton.button));
                break;
        
            case SDL_JOYDEVICEADDED:
                DYNASTI_LOG_VERBOSE("SDL_Event: Joystick device added event : " + std::to_string(sdlEvent.jdevice.which));
                break;
        
            case SDL_JOYDEVICEREMOVED:
                DYNASTI_LOG_VERBOSE("SDL_Event: Joystick removed : " + std::to_string(sdlEvent.jdevice.which));
                break;
        
            case SDL_JOYHATMOTION:
                DYNASTI_LOG_VERBOSE("SDL_Event: Joystick hat motion detected");
                break;
                
            case SDL_LOCALECHANGED:
                DYNASTI_LOG_VERBOSE("SDL_Event: User locale changed");
                break;
                
            case SDL_POLLSENTINEL:
                DYNASTI_LOG_VERBOSE("SDL_Event: Poll Sentinel");
                break;
        
            case SDL_QUIT:
            {
                DYNASTI_LOG_VERBOSE("SDL_Event: Quit event");
                Event quitAppEvent(quitAppEventType_);
                eventManager_->BroadcastEvent(quitAppEvent);
                return true;
            }
            
            case SDL_SENSORUPDATE:
                DYNASTI_LOG_VERBOSE("SDL_Event: Sensor update");
                break;
            
            case SDL_SYSWMEVENT:
                DYNASTI_LOG_VERBOSE("SDL_Event: Sys wm event??");
                break;
        
            case SDL_TEXTEDITING:
            case SDL_TEXTEDITING_EXT:
                DYNASTI_LOG_VERBOSE("SDL_Event: Text editing event");
                break;
        
            case SDL_TEXTINPUT:
                DYNASTI_LOG_VERBOSE("SDL_Event: Text input event");
                break;
        
            case SDL_WINDOWEVENT:
            {
                std::shared_ptr<Window> primaryWindow = GetPrimaryWindow();
                DYNASTI_ASSERT(primaryWindow != nullptr, "Invalid pointer to primary window");
                return primaryWindow->OnSdlWindowEvent(sdlEvent);
            }
            case SDL_FIRSTEVENT:
            case SDL_LASTEVENT:
                DYNASTI_LOG_VERBOSE("SDL_Event: First/last event");
                break;
        
            case SDL_CLIPBOARDUPDATE:
                DYNASTI_LOG_VERBOSE("SDL_Event: Clipboard update");
                break;
        
            case SDL_RENDER_DEVICE_RESET:
            case SDL_RENDER_TARGETS_RESET:
                DYNASTI_LOG_VERBOSE("SDL_Event: Render event");
                break;
        
            case SDL_USEREVENT:
                DYNASTI_LOG_VERBOSE("SDL_Event: User event detected : " + std::to_string(sdlEvent.type));
                break;

#ifdef DYNASTI_DESKTOP
            case SDL_KEYDOWN:
                inputManager_->OnKeyDown(static_cast<KeyCode>(sdlEvent.key.keysym.sym));
                DYNASTI_LOG_VERBOSE("SDL_Event: Keydown: " + std::to_string(sdlEvent.key.keysym.sym));
                return true;
    
            case SDL_KEYMAPCHANGED:
                DYNASTI_LOG_VERBOSE("SDL_Event: Keymap changed");
                break;
    
            case SDL_KEYUP:
                inputManager_->OnKeyUp(static_cast<KeyCode>(sdlEvent.key.keysym.sym));
                DYNASTI_LOG_VERBOSE("SDL_Event: Keyup: " + std::to_string(sdlEvent.key.keysym.sym));
                return true;
    
            case SDL_MOUSEBUTTONDOWN:
                //inputManager_->OnSdlMouseButtonDown(sdlEvent);
                DYNASTI_LOG_VERBOSE("SDL_Event: Mouse button down");
                break;
    
            case SDL_MOUSEBUTTONUP:
                //inputManager_->OnSdlMouseButtonUp(sdlEvent);
                DYNASTI_LOG_VERBOSE("SDL_Event: Mouse button up");
                break;
    
            case SDL_MOUSEMOTION:
                //inputManager_->OnSdlMouseMotion(sdlEvent);
                DYNASTI_LOG_VERBOSE("SDL_Event: Mouse motion");
                break;
    
            case SDL_MOUSEWHEEL:
                //inputManager->OnSdlMouseWheel(sdlEvent);
                DYNASTI_LOG_VERBOSE("SDL_Event: Mouse wheel");
                break;
    
            case SDL_DROPFILE:
            case SDL_DROPBEGIN:
            case SDL_DROPCOMPLETE:
            case SDL_DROPTEXT:
                DYNASTI_LOG_VERBOSE("SDL_Event: Drop event detected");
                break;
#endif

#ifdef DYNASTI_MOBILE
            case SDL_APP_LOWMEMORY:
            case SDL_APP_TERMINATING:
            case SDL_APP_DIDENTERFOREGROUND:
            case SDL_APP_DIDENTERBACKGROUND:
            case SDL_APP_WILLENTERFOREGROUND:
            case SDL_APP_WILLENTERBACKGROUND:
                DYNASTI_LOG_VERBOSE("SDL_Event: Unused App event");
                break;
    
            case SDL_DOLLARGESTURE:
            case SDL_DOLLARRECORD:
                DYNASTI_LOG_VERBOSE("SDL_Event: Dollar gesture event?")
                break;
    
            case SDL_FINGERDOWN:
                //touchScreen.OnSdlFingerDown(sdlEvent);
                break;
    
            case SDL_FINGERMOTION:
                //touchScreen.OnSdlFingerMotion(sdlEvent);
                break;
    
            case SDL_FINGERUP:
                //touchScreen.OnSdlFingerUp(sdlEvent);
                break;
    
            case SDL_MULTIGESTURE:
                DYNASTI_LOG_VERBOSE("SDL_Event: Multi-gesture event");
                break;
#endif
        
            default:
            {
                DYNASTI_WARNING("Unrecognised SDL_Event: " + std::to_string(sdlEvent.type));
                return false;
            }
        }
        return false;
    }
    
}
//---------------------------------------------------------------------------------------------------------------------