#include "Dynasti/Core/Debug.h"
#include "Dynasti/Window/Window.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    Window::Window(const std::string title, const std::int32_t width, const std::int32_t height, const int vSync)
        : title_(title)
    {
        DYNASTI_ASSERT(width > 0, "Invalid window width : " + std::to_string(width));
        DYNASTI_ASSERT(height > 0, "Invalid window height : " + std::to_string(height));
        DYNASTI_ASSERT((vSync == 0 || vSync == 1), "Invalid value for vSync : " + std::to_string(vSync));
        std::string sdlError;

        sdlWindowInternal_ = SDL_CreateWindow(title_.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            width, height, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

        if (sdlWindowInternal_ == nullptr)
        {
            sdlError = SDL_GetError();
            DYNASTI_FATAL("SDL could not create a window: " + sdlError);
        }

        width_         = width;
        height_        = height;
        keyboardFocus_ = true;
        mouseFocus_    = true;
        sdlWindowId_   = SDL_GetWindowID(sdlWindowInternal_);
        DYNASTI_LOG("SDL window id: " + std::to_string(sdlWindowId_));

        // Create OpenGL context
        sdlGlContext_ = SDL_GL_CreateContext(sdlWindowInternal_);
        if (sdlGlContext_ == nullptr)
        {
            sdlError = SDL_GetError();
            SDL_DestroyWindow(sdlWindowInternal_);
            sdlWindowInternal_ = nullptr;
            DYNASTI_FATAL("SDL could not create a GL context: " + sdlError);
        }

        DYNASTI_LOG("Created SDL OpenGL 4.6 context")
        SDL_GL_MakeCurrent(sdlWindowInternal_, sdlGlContext_);
        SDL_ShowWindow(sdlWindowInternal_);
        SDL_GL_SetSwapInterval(vSync);
    }
    
    Window::~Window()
    {
        if (sdlWindowInternal_)
        {
            SDL_DestroyWindow(sdlWindowInternal_);
            sdlWindowInternal_ = nullptr;
        }
    }

    void Window::SwapBuffers()
    {
        SDL_GL_SwapWindow(sdlWindowInternal_);
    }
    
    bool Window::OnSdlWindowEvent(const SDL_Event &e)
    {
        DYNASTI_ASSERT(e.type == SDL_WINDOWEVENT, "OnSdlWindowEvent function received incompatible SDL event");
    
        // If an SDL event was not detected for this window, then ignore
        if (e.window.windowID == (unsigned int) sdlWindowId_)
        {
            switch(e.window.event)
            {
                case SDL_WINDOWEVENT_CLOSE:
                {
                    SDL_HideWindow(sdlWindowInternal_);
                    DYNASTI_LOG("SDL_WindowEvent: Window closed;  id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_DISPLAY_CHANGED:
                {
                    DYNASTI_LOG("SDL_WindowEvent: Display changed;  id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_ENTER:
                {
                    mouseFocus_ = true;
                    DYNASTI_LOG("SDL_WindowEvent: Mouse entered window;  id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_EXPOSED:
                {
                    //SDL_RenderPresent(sdlRenderer);
                    DYNASTI_LOG("SDL_WindowEvent: Window exposed;  id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_FOCUS_GAINED:
                {
                    keyboardFocus_ = true;
                    DYNASTI_LOG("SDL_WindowEvent: Window gained keyboard focus;  id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_FOCUS_LOST:
                {
                    DYNASTI_LOG("SDL_WindowEvent: Window lost keyboard focus;  id : " + std::to_string(sdlWindowId_));
                    keyboardFocus_ = false;
                    return true;
                }
                case SDL_WINDOWEVENT_HIDDEN:
                {
                    shown_ = false;
                    DYNASTI_LOG("SDL_WindowEvent: Window hidden;  id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_HIT_TEST:
                {
                    DYNASTI_LOG("SDL_WindowEvent: Hit test;  id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_ICCPROF_CHANGED:
                {
                    DYNASTI_LOG("SDL_WindowEvent: ICC profile changed;  id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_LEAVE:
                {
                    DYNASTI_LOG("SDL_WindowEvent: Mouse left;  id : " + std::to_string(sdlWindowId_));
                    mouseFocus_ = false;
                    return true;
                }
                case SDL_WINDOWEVENT_MAXIMIZED:
                {
                    minimized_ = false;
                    DYNASTI_LOG("SDL_WindowEvent: Window maximized;  id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_MINIMIZED:
                {
                    minimized_ = true;
                    DYNASTI_LOG("SDL_WindowEvent: Window minimized;  id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_MOVED:
                {
                    DYNASTI_LOG("SDL_WindowEvent: Window moved; id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_RESIZED:
                {
                    width_ = e.window.data1;
                    height_ = e.window.data2;
                    DYNASTI_LOG("SDL_WindowEvent: Window resized;  id : " + std::to_string(sdlWindowId_) + "  w : " +
                        std::to_string(width_) + "  h : " + std::to_string(height_));
                    return true;
                }
                case SDL_WINDOWEVENT_RESTORED:
                {
                    minimized_ = false;
                    DYNASTI_LOG("SDL_WindowEvent: Window restored;  id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_SHOWN:
                {
                    shown_ = true;
                    DYNASTI_LOG("SDL_WindowEvent: Window shown;  id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_SIZE_CHANGED:
                {
                    width_ = e.window.data1;
                    height_ = e.window.data2;
                    DYNASTI_LOG("SDL_WindowEvent: Window size changed;  id : " + std::to_string(sdlWindowId_) + "  w : " +
                                std::to_string(width_) + "  h : " + std::to_string(height_));
                    return true;
                }
                case SDL_WINDOWEVENT_TAKE_FOCUS:
                {
                    DYNASTI_LOG("SDL_WindowEvent: Window take focus;  id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                default:
                {
                    DYNASTI_LOG("SDL_WindowEvent unknown: " + std::to_string(e.window.event) + ";  id : " +
                                 std::to_string(sdlWindowId_));
                    return false;
                }
    
            }
        }
        
        return false;
    }

    void Window::SetTitle(const std::string newTitle)
    {
        SDL_SetWindowTitle(sdlWindowInternal_, newTitle.c_str());
    }

}
//---------------------------------------------------------------------------------------------------------------------