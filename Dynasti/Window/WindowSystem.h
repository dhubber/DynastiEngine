#ifndef DYNASTI_WINDOW_SYSTEM_H
#define DYNASTI_WINDOW_SYSTEM_H

#include <memory>
#include <SDL.h>
#include "Dynasti/Application/System.h"
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Window/Window.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    class EventManager;
    class InputManager;

    //=================================================================================================================
    /// \brief   System for creating and managing all Windows during the application lifetime.
    /// \author  D. A. Hubber
    /// \date    05/08/2022
    //=================================================================================================================
    class WindowSystem : public System
    {
    public:
        
        WindowSystem(const std::string& name,
                     std::shared_ptr<AssetManager> assetManager,
                     std::shared_ptr<ClassRegistry> classRegistry,
                     std::shared_ptr<EventManager> eventManager,
                     std::shared_ptr<FrameTimer> frameTimer,
                     std::shared_ptr<InputManager> inputManager)
            : System(name, assetManager, classRegistry, eventManager, frameTimer)
            , inputManager_(inputManager)
        {
        }

        // Implemented required by System
        void RegisterClasses() override {};
        void Setup() override;
        void Shutdown() override;
        void Update() override;
        
        bool quit = false;
        
    protected:

        // Performs all general SDL set-up
        void SetupSdl();

        // Sets-up all engine-wide OpenGL options based through SDL
        void SetupOpenGL();

        /// Processes a given SDL event polled from the SDL event queue
        bool OnSdlEvent(const SDL_Event &);

        // Getter
        std::shared_ptr<Window> GetPrimaryWindow() { return window_; }
        
        SDL_DisplayMode sdlDisplayMode_;                   ///< Detected display mode (by SDL)
        SDL_version sdlCompiled_;                          ///< Version of SDL that Dynasti is compiled with
        SDL_version sdlLinked_;                            ///< Version of SDL that Dynasti is linked with
        EventType quitAppEventType_;                       ///< Quit application event type
        std::int32_t numDisplays_;                         ///< Number of connected displays detected by SDL
        std::int32_t displayWidth_;                        ///< Width of primary display (in pixels)
        std::int32_t displayHeight_;                       ///< Height of primary display (in pixels)
        std::shared_ptr<InputManager> inputManager_;       ///< Global input manager
        std::shared_ptr<Window> window_;                   ///< Primary SDL window created during start-up
    
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
