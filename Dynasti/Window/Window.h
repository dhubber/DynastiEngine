#ifndef DYNASTI_WINDOW_H
#define DYNASTI_WINDOW_H


#include <SDL.h>
#include <memory>
#include <string>

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Class representing a single SDL2 window
    /// \author  D. A. Hubber
    /// \date    24/09/2022
    //=================================================================================================================
    class Window
    {
    public:
        
        Window(const std::string title, std::int32_t width, std::int32_t height, int vSync);
        ~Window();

        /// Swaps screen buffer when updating the display (instantly, or waits if vsync enabled)
        void SwapBuffers();

        /// Called whenever an SDL window event is polled from SDL
        /// \param[in] e - SDL window event
        bool OnSdlWindowEvent(const SDL_Event &e);

        /// Sets the displayed title of the window
        /// \param[in] newTitle - Human readable title
        void SetTitle(const std::string newTitle);
    
        // Getters
        inline std::int32_t GetHeight() const { return height_; }
        inline std::int32_t GetWidth() const { return width_; }
        inline const std::string& GetTitle() const { return title_; }
        inline SDL_Window* GetSdlInternalWindow() { return sdlWindowInternal_; }

    protected:
    
        std::string title_{};                              ///< Window display title
        std::int32_t height_{-1};                          ///< Height (in pixels) of window
        std::int32_t width_{-1};                           ///< Width (in pixels) of window

        bool hidden_{false};                               ///< Is Window hidden?
        bool keyboardFocus_{false};                        ///< Does Window have keyboard focus?
        bool minimized_{false};                            ///< Is Window in minimized state?
        bool mouseFocus_{false};                           ///< Does Window have mouse focus?
        bool shown_{false};                                ///< Is Window currently shown?
        uint32_t sdlWindowId_{0};                          ///< (Unique) id of Window object (internal to SDL API)
        SDL_Window* sdlWindowInternal_{nullptr};           ///< Pointer to SDL Window object (internal to SDL API)
        SDL_GLContext sdlGlContext_{nullptr};              ///< OpenGL context
    
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
