#include <functional>
#include <memory>
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Event/EventListener.h"
#include "Dynasti/Event/EventManager.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    EventListener::EventListener(std::shared_ptr<EventManager> eventManager, const std::string name)
        : eventManager_(eventManager)
        , name_(name)
    {
        DYNASTI_LOG_VERBOSE("Constructing EventListener object : " + name_);
    }

    EventType EventListener::SubscribeToEvent(const std::string& eventName,
                                              std::function<void(const Event)> callbackFunction)
    {
        DYNASTI_ASSERT(eventManager_, "Invalid pointer to EventManager");
        EventType eventType = eventManager_->SubscribeToEvent(eventName, shared_from_this());
        eventCallbackMap.insert(std::make_pair(eventType, callbackFunction));
        return eventType;
    }

    void EventListener::OnEvent(const Event event)
    {
        const EventType eventType = event.GetType();
        DYNASTI_LOG_VERBOSE("EventListener : " + GetName() + " receiving event type " + std::to_string(eventType));
        auto it = eventCallbackMap.find(eventType);
        if (it != std::end(eventCallbackMap))
        {
            std::function<void(Event)> callbackFunction = it->second;
            DYNASTI_WARNING_IF(callbackFunction == nullptr, "Event callback function not valid");
            if (callbackFunction)
            {
                callbackFunction(event);
            }
        }
        else
        {
            DYNASTI_WARNING("Subscribed event not found in local callback map");
        }
    }
    
}
//---------------------------------------------------------------------------------------------------------------------