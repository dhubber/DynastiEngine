#include <filesystem>
#include <iostream>
#include "doctest/doctest.h"
#include "Dynasti/Event/Event.h"
#include "Dynasti/Event/EventListener.h"
#include "Dynasti/Event/EventManager.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    /// Behaviour: Event objects should be read-only/immutable since transmitted information should be unaltered.
    TEST_CASE("Event")
    {
        Event event1(1);
        REQUIRE_EQ(event1.GetType(), 1);
        REQUIRE_EQ(event1.GetId(), -1);

        Event event2(24, 1234);
        REQUIRE_EQ(event2.GetType(), 24);
        REQUIRE_EQ(event2.GetId(), 1234);
    }


    /// Behaviour: Event types can be created using unique human-readable strings
    TEST_CASE("EventManager")
    {
        // Verify default state of EventManager
        EventManager eventManager;
        REQUIRE_EQ(eventManager.GetNumEventTypes(), 0);
        REQUIRE_EQ(eventManager.GetEventType("DummyEvent"), -1);
        REQUIRE_EQ(eventManager.GetEventType("DummyEvent2"), -1);

        // Verify registering an event with the EventManager and that all functions behave consistently
        EventType dummyEventType = eventManager.RegisterEvent("DummyEvent");
        REQUIRE_EQ(eventManager.GetNumEventTypes(), 1);
        REQUIRE_EQ(eventManager.GetEventType("DummyEvent"), dummyEventType);
        REQUIRE_EQ(eventManager.GetEventName(dummyEventType), "DummyEvent");

        // Verify registering a 2nd event with the EventManager
        EventType dummyEvent2Type = eventManager.RegisterEvent("DummyEvent2");
        REQUIRE_EQ(eventManager.GetNumEventTypes(), 2);
        REQUIRE_EQ(eventManager.GetEventType("DummyEvent2"), dummyEvent2Type);
        REQUIRE_EQ(eventManager.GetEventName(dummyEvent2Type), "DummyEvent2");

        // Verify we cannot register the same event more than once
        EventType duplicateEventType = eventManager.RegisterEvent("DummyEvent");
        REQUIRE_EQ(eventManager.GetNumEventTypes(), 2);
        REQUIRE_EQ(duplicateEventType, dummyEventType);
    }


    /// Verify the event manager behaves correctly.
    TEST_CASE("EventListener")
    {
        // Some dummy variables to manipulate via events
        int x = 1;
        int y = 1;

        // Verify that new event types have no subscribers
        std::shared_ptr<EventManager> eventManager = std::make_shared<EventManager>();
        EventType dummyEventType = eventManager->RegisterEvent("Dummy");
        REQUIRE_EQ(eventManager->GetNumEventTypes(), 1);
        REQUIRE_EQ(eventManager->GetNumSubscribersToEvent(dummyEventType), 0);

        // Register an event listener with an existing event type
        std::shared_ptr<EventListener> eventListener = std::make_shared<EventListener>(eventManager, "EL1");
        eventListener->SubscribeToEvent("Dummy", [](Event event) {});
        REQUIRE_EQ(eventManager->GetNumEventTypes(), 1);
        REQUIRE_EQ(eventManager->GetNumSubscribersToEvent(dummyEventType), 1);

        // Verify that we can register a new event type via the event listener
        EventType multiplyEventType = eventListener->SubscribeToEvent("Multiply",
                                                                      [&x](Event event) { x *= event.GetId(); });
        REQUIRE_EQ(eventManager->GetNumEventTypes(), 2);
        REQUIRE_EQ(eventManager->GetNumSubscribersToEvent(dummyEventType), 1);
        REQUIRE_EQ(eventManager->GetNumSubscribersToEvent(multiplyEventType), 1);

        // Verify event broadcast to a single listener
        Event multiplyEvent(multiplyEventType, 64);
        eventManager->BroadcastEvent(multiplyEvent);
        REQUIRE_EQ(x, 64);
        REQUIRE_EQ(y, 1);

        // Register a 2nd event listener with an existing event type
        std::shared_ptr<EventListener> eventListener2 = std::make_shared<EventListener>(eventManager, "EL2");
        eventListener2->SubscribeToEvent("Multiply", [&y](const Event event) { y *= event.GetId(); });
        REQUIRE_EQ(eventManager->GetNumEventTypes(), 2);
        REQUIRE_EQ(eventManager->GetNumSubscribersToEvent(dummyEventType), 1);
        REQUIRE_EQ(eventManager->GetNumSubscribersToEvent(multiplyEventType), 2);

        // Verify event broadcast to multiple listeners
        Event multiply2Event(multiplyEventType, 4);
        eventManager->BroadcastEvent(multiply2Event);
        REQUIRE_EQ(x, 256);
        REQUIRE_EQ(y, 4);

        // Verify that listeners can successfully unsubscrive from events
        eventManager->UnsubscribeToEvent("Multiply", eventListener);
        REQUIRE_EQ(eventManager->GetNumEventTypes(), 2);
        REQUIRE_EQ(eventManager->GetNumSubscribersToEvent(dummyEventType), 1);
        REQUIRE_EQ(eventManager->GetNumSubscribersToEvent(multiplyEventType), 1);
    }
}
//---------------------------------------------------------------------------------------------------------------------