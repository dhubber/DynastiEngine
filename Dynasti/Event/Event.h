#ifndef DYNASTI_EVENT_H
#define DYNASTI_EVENT_H


#include <variant>
#include <glm/glm.hpp>
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Vector/Vector.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Simple event class.
    /// \details The expected behaviour of the event system is
    ///          - Event objects should be read-only/immutable since transmitted information should be unaltered.
    ///          - Event types can be created using unique human-readable strings
    ///          - Event listeners can subscribe to all available event types with a given callback function
    ///          - When an event of a given type is broadcast, the callback function of all listeners is evoked
    /// \author  D. A. Hubber
    /// \date    07/10/2022
    //=================================================================================================================
    class Event
    {
    public:

        constexpr Event(const EventType type, const Uuid id=-1)
            : type_(type)
            , id_(id)
        {
        };

        template <typename T>
        constexpr Event(const EventType type, const Uuid id, const T& data)
            : type_(type)
            , data_(data)
            , id_(id)
        {
        };

        /// Returns the contained event data cast to the required templated type
        /// \return Reference to data contained in event object
        template <typename T>
        constexpr inline const T& GetData() const { return std::get<T>(data_); }

        // Getters
        constexpr inline EventType GetType() const { return type_; }
        constexpr inline Uuid GetId() const { return id_; }


    private:

        const EventType type_;                               ///< Event type id (as registered in the EventManager)
        const Uuid id_;                                      ///< id associated with event (e.g. entity or scene id)
        const std::variant<bool, uint32_t, int32_t,
                           Uuid, float, double,
                           Vec2, Vec4,
                           glm::mat4, 
                           std::string> data_;               ///< Event data variant
    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
