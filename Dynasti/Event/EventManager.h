#ifndef DYNASTI_EVENT_MANAGER_H
#define DYNASTI_EVENT_MANAGER_H

#include <map>
#include <memory>
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Event/Event.h"
#include "Dynasti/Event/EventListener.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Manages registering and broadcasting events between different engine systems.
    /// \author  D. A. Hubber
    /// \date    29/06/2020
    //=================================================================================================================
    class EventManager
    {
    public:
    
        EventManager()
        {
            DYNASTI_LOG_VERBOSE("Constructed EventManager object");
        }
        EventManager(EventManager const &) = delete;
        void operator=(EventManager const &) = delete;

        /// Register an event with a unique string id
        /// \param[in] eventName - Human-readable event name
        /// \return - Unique integer id for fast reference
        EventType RegisterEvent(const std::string& eventName);

        /// Subscribes an EventListener object to receive events with the given string name
        /// \param[in] eventName - Human-readable event name being subscribed to
        /// \param[in] eventListener - EventListener object
        /// \return - Unique event type for fast reference
        EventType SubscribeToEvent(const std::string& eventName, std::shared_ptr<EventListener>);
    
        /// Unsubscribes the given EventListener object from receiving events with the given string name
        /// \param[in] eventName - Human-readable event name being unsubscribed from
        /// \param[in] eventListener - EventListener object
        bool UnsubscribeToEvent(const std::string& eventName, std::shared_ptr<EventListener> eventListener);

        /// Broadcasts the given event to all subscribers of that event type
        /// \param[in] event - Event to be broadcast to all subscribers
        void BroadcastEvent(const Event event);

        /// \param[in] event - Human-readable name of event type
        /// \return - EventType id of the requested event
        EventType GetEventType(const std::string& eventName) const;

        /// \param[in] eventType - EventType integer id of the requested event
        /// \return - Human-readable event name
        std::string GetEventName(EventType eventType) const;
        
        /// \param[in] eventType - Event type being queried
        /// \return - Number of EventListeners currently subscribing to the given event type
        std::int32_t GetNumSubscribersToEvent(EventType eventType) const;

        /// \return - Number of registered event types
        inline std::int32_t GetNumEventTypes() const { return eventTypes_.size(); }
    
        /// Records all registered events to the log, including which listeners have subscribed to them
        void LogAllRegisteredEvents();
        
    private:

        std::map<std::string, EventType> eventTypes_;      ///< Map of event name to a registered event type id
        std::map<EventType, std::string> eventNames_;      ///< Map of event types to human-readable names
        std::multimap<EventType, std::shared_ptr<EventListener>> subscribers_;   ///< Multimap of an event type to all subscribed-listeners

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
