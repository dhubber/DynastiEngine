#ifndef DYNASTI_COLLISION2D_H
#define DYNASTI_COLLISION2D_H

#include <set>
#include <unordered_set>
#include "Dynasti/Application/System.h"
#include "Dynasti/Camera/Camera.h"
#include "Dynasti/Renderer/VertexBuffer.h"
#include "Dynasti/Shader/GlslShader.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    using Collision = std::pair<Uuid,Uuid>;
    using CollisionSet = std::set<Collision>;

    //=================================================================================================================
    /// \brief   ...
    /// \author  D. A. Hubber
    /// \date    08/08/2022
    //=================================================================================================================
    class Collision2dSystem : public System
    {
    public:

        Collision2dSystem(const std::string& name,
                          std::shared_ptr<AssetManager> assetManager,
                          std::shared_ptr<ClassRegistry> classRegistry,
                          std::shared_ptr<EventManager> eventManager,
                          std::shared_ptr<FrameTimer> frameTimer);

        // Required implementations of System functions
        void RegisterClasses() override;
        void Setup() override;
        void Shutdown() override;
        void Update() override;

    protected:

        std::int32_t counter_{0};

        EventType overlapBeginEventType_;
        EventType overlapEndEventType_;
        EventType updateWorldPositionEventType_;

        CollisionSet collisionsSetA_;
        CollisionSet collisionsSetB_;

        void FindCollisions(CollisionSet& collisionSet);
        void BroadcastNewCollisionEvents(CollisionSet& oldCollisionSet, CollisionSet& newCollisionSet);
        void BroadcastOverlapBegin(Collision collision);
        void BroadcastOverlapEnd(Collision collision);

        void OnUpdateWorldPosition(Event event);

        static Collision MakeCollision(Uuid collider1, Uuid collider2)
        {
            return (collider1 < collider2) ? std::make_pair(collider1, collider2) : std::make_pair(collider2, collider1);
        }

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif