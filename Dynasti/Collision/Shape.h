#ifndef DYNASTI_SHAPE_H
#define DYNASTI_SHAPE_H

#include "Dynasti/Reflection/Property.h"
#include "Dynasti/Reflection/Reflectable.h"
#include "Dynasti/Vector/Vector.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    class AxisAlignedBoundingBox
    {
    public:
        constexpr AxisAlignedBoundingBox(Vec2 min, Vec2 max)
            : min(min)
            , max(max)
        {
        }

        const Vec2 min;
        const Vec2 max;

        bool IsOverlapping(const AxisAlignedBoundingBox& other) const
        {
            if (min.x > other.max.x) return false;
            if (max.x < other.min.x) return false;
            if (min.y > other.max.y) return false;
            if (max.y < other.min.y) return false;
            return true;
        }
    };

    //=================================================================================================================
    /// \brief   Base class for defining the interface for all 2d shapes.
    /// \author  D. A. Hubber
    /// \date    17/08/2023
    //=================================================================================================================
    class Shape2d
    {
    public:

        virtual float Area() const = 0;
        virtual Vec2 COM() const = 0;
        virtual AxisAlignedBoundingBox GetAABB() const = 0;

    };

    //=================================================================================================================
    /// \brief   Component class containing all data for rendering a mesh to the viewport.
    /// \author  D. A. Hubber
    /// \date    13/02/2020
    //=================================================================================================================
    class Circle : public Shape2d, public Reflectable
    {
    public:

        Vec2 pos{0.0f, 0.0f};
        float radius{0.5f};

        Circle() = default;
        Circle(const Vec2 pos, const float radius)
            : pos{pos}
            , radius{radius}
        {
        }

        // Required by reflection system
        std::type_index GetType() override { return std::type_index(typeid(Circle)); }
        static std::string Name() { return "Circle"; }
        static std::vector<std::string> BaseClasses() { return {}; }
        static std::vector<PropertyPtr> Properties()
        {
            return std::vector<PropertyPtr>{
                std::make_shared<BasicProperty<Circle, Vec2>>("Pos", &Circle::pos),
                std::make_shared<BasicProperty<Circle, float>>("Radius", &Circle::radius)
            };
        }

        // Required overrides for Shape2d
        [[nodiscard]] float Area() const override { return twoPi * radius * radius; }
        [[nodiscard]] Vec2 COM() const override { return pos; }
        [[nodiscard]] AxisAlignedBoundingBox GetAABB() const override
        {
            return AxisAlignedBoundingBox{{pos.x - radius, pos.y - radius},{pos.x + radius, pos.y + radius}};
        }

    };

    //=================================================================================================================
    /// \brief   Component class containing all data for rendering a mesh to the viewport.
    /// \author  D. A. Hubber
    /// \date    13/02/2020
    //=================================================================================================================
    class Box : public Shape2d, public Reflectable
    {
    public:

        Vec2 pos;
        Vec2 size;

        Box() = default;
        Box(Vec2 pos, Vec2 size)
            : pos(pos)
            , size(size)
        {
        }

        // Required by reflection system
        std::type_index GetType() override { return std::type_index(typeid(Box)); }
        static std::string Name() { return "Box"; }
        static std::vector<std::string> BaseClasses() { return {}; }
        static std::vector<PropertyPtr> Properties()
        {
            return std::vector<PropertyPtr>{
                std::make_shared<BasicProperty<Box, Vec2>>("Pos", &Box::pos),
                std::make_shared<BasicProperty<Box, Vec2>>("Size", &Box::size),
            };
        }

        [[nodiscard]] float Area() const override { return size.x * size.y; }
        [[nodiscard]] Vec2 COM() const override { return pos; }
        [[nodiscard]] AxisAlignedBoundingBox GetAABB() const override
        {
            return AxisAlignedBoundingBox{{pos.x - 0.5f*size.x, pos.y - 0.5f*size.y},
                                          {pos.x + 0.5f*size.x, pos.y + 0.5f*size.y}};
        }

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
