#include "Dynasti/Collision/Collision2dSystem.h"

#include <algorithm>
#include "Dynasti/Collision/Shape2dComponent.h"
#include "Dynasti/Event/EventManager.h"
#include "Dynasti/Reflection/ClassRegistry.h"
#include "Dynasti/Scene/Scene.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    Collision2dSystem::Collision2dSystem(const std::string& name,
                    std::shared_ptr<AssetManager> assetManager,
                    std::shared_ptr<ClassRegistry> classRegistry,
                    std::shared_ptr<EventManager> eventManager,
                    std::shared_ptr<FrameTimer> frameTimer)
        : System(name, assetManager, classRegistry, eventManager, frameTimer)
    {
    }

    void Collision2dSystem::RegisterClasses()
    {
        classRegistry_->RegisterClass<Shape2dComponent>();
    }

    void Collision2dSystem::Setup()
    {
        overlapBeginEventType_ = eventManager_->RegisterEvent("OverlapBegin");
        overlapEndEventType_ = eventManager_->RegisterEvent("OverlapEnd");
        updateWorldPositionEventType_ = eventListener_->SubscribeToEvent("UpdateWorldPosition", std::bind(&Collision2dSystem::OnUpdateWorldPosition, this, std::placeholders::_1));
    }

    void Collision2dSystem::Shutdown()
    {
    }

    void Collision2dSystem::Update()
    {
        DYNASTI_TIMER(Collision2dSystem)

        // Double buffer the collision sets
        CollisionSet& oldCollisionSet = (counter_%2 == 0) ? collisionsSetA_ : collisionsSetB_;
        CollisionSet& newCollisionSet = (counter_%2 == 0) ? collisionsSetB_ : collisionsSetA_;
        FindCollisions(newCollisionSet);
        BroadcastNewCollisionEvents(oldCollisionSet, newCollisionSet);
        ++counter_;
    }

    void Collision2dSystem::FindCollisions(CollisionSet& collisionSet)
    {
        collisionSet.clear();

        if (Scene::mainScene == nullptr)
            return;

        const auto& shapePool = Scene::mainScene->FindComponentPool<Shape2dComponent>();
        if (shapePool == nullptr)
            return;

        auto numShapes = shapePool->NumObjects();
        for (int i = 0; i < numShapes - 1; ++i)
        {
            auto shape1 = shapePool->FindObjectWithLocalId(i);
            if (shape1->IsActive() == false) continue;
            const auto AABB1 = shape1->GetWorldAABB();
            for (int j = i + 1; j < numShapes; ++j)
            {
                auto shape2 = shapePool->FindObjectWithLocalId(j);
                if (shape2->IsActive() == false) continue;
                const auto AABB2 = shape2->GetWorldAABB();
                if (AABB1.IsOverlapping(AABB2))
                {
                    collisionSet.insert(MakeCollision(shape1->uuid, shape2->uuid));
                }
            }
        }
    }

    void Collision2dSystem::BroadcastNewCollisionEvents(CollisionSet &oldCollisionSet, CollisionSet &newCollisionSet)
    {
        CollisionSet oldDiff, newDiff;
        std::set_difference(oldCollisionSet.begin(), oldCollisionSet.end(), newCollisionSet.begin(),
                            newCollisionSet.end(), std::inserter(oldDiff, oldDiff.begin()));
        std::set_difference(newCollisionSet.begin(), newCollisionSet.end(), oldCollisionSet.begin(),
                            oldCollisionSet.end(), std::inserter(newDiff, newDiff.begin()));

        for (auto collision : oldDiff)
        {
            BroadcastOverlapEnd(collision);
        }

        for (auto collision : newDiff)
        {
            BroadcastOverlapBegin(collision);
        }
    }

    void Collision2dSystem::BroadcastOverlapBegin(Collision collision)
    {
        Event overlapBeginEvent1(overlapBeginEventType_, collision.first, collision.second);
        Event overlapBeginEvent2(overlapBeginEventType_, collision.second, collision.first);
        eventManager_->BroadcastEvent(overlapBeginEvent1);
        eventManager_->BroadcastEvent(overlapBeginEvent2);
    }

    void Collision2dSystem::BroadcastOverlapEnd(Collision collision)
    {
        Event overlapEndEvent1(overlapEndEventType_, collision.first, collision.second);
        Event overlapEndEvent2(overlapEndEventType_, collision.second, collision.first);
        eventManager_->BroadcastEvent(overlapEndEvent1);
        eventManager_->BroadcastEvent(overlapEndEvent2);
    }

    void Collision2dSystem::OnUpdateWorldPosition(Event event)
    {
        if (Scene::mainScene == nullptr) return;
        auto entityId = event.GetId();
        auto worldPos = event.GetData<Vec2>();
        auto shapePool = Scene::mainScene->FindComponentPool<Shape2dComponent>();
        if (shapePool) {
            auto shapeComp = shapePool->FindObjectWithUuid(entityId);
            if (shapeComp != nullptr) shapeComp->SetWorldPosition(worldPos);
        }
    }
}
//---------------------------------------------------------------------------------------------------------------------