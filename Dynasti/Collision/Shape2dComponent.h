#ifndef DYNASTI_SHAPE2D_COMPONENT_H
#define DYNASTI_SHAPE2D_COMPONENT_H

#include <array>
#include <memory>
#include <string>
#include <variant>
#include "Dynasti/Collision/Shape.h"
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Object/Object.h"
#include "Dynasti/Reflection/Property.h"
#include "Dynasti/Vector/Vector.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Component class containing all data for rendering a mesh to the viewport.
    /// \author  D. A. Hubber
    /// \date    13/02/2020
    //=================================================================================================================
    class Shape2dComponent : public PoolableObject
    {
    public:

        Shape2dComponent() = default;
        virtual ~Shape2dComponent() = default;

        // Required by reflection system
        std::type_index GetType() override { return std::type_index(typeid(Shape2dComponent)); }
        static std::string Name() { return "Shape2dComponent"; }
        static std::vector<std::string> BaseClasses() { return {}; }
        static std::vector<PropertyPtr> Properties()
        {
            return std::vector<PropertyPtr>{
                std::make_shared<ReflectedProperty<Shape2dComponent,Box>>("Box", &Shape2dComponent::localShape_)
            };
        }

        // Implementations of PoolableObject functions
        void OnCreate() override {};
        void OnDestroy() override {};

        void SetWorldPosition(Vec2 newPos)
        {
            worldShape_.pos = localShape_.pos + newPos;
        }

        void ToggleCollision(bool newActive) { isActive_ = newActive; }
        bool IsActive() const { return isActive_; }

        [[maybe_unused]] Box& GetLocalShape() { return localShape_; }
        [[maybe_unused]] Box& GetWorldShape() { return worldShape_; }
        [[nodiscard]] AxisAlignedBoundingBox GetWorldAABB() const { return worldShape_.GetAABB(); }

    protected:

        bool isActive_ = true;
        Box localShape_;
        Box worldShape_;
        //std::variant<Circle, Box> localShape_;
        //std::variant<Circle, Box> worldShape_;
        
    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
