#include "doctest/doctest.h"
#include "Dynasti/Asset/AssetManager.h"
#include "Dynasti/Collision/Collision2dSystem.h"
#include "Dynasti/Core/FrameTimer.h"
#include "Dynasti/Core/Uuid.h"
#include "Dynasti/Event/EventManager.h"
#include "Dynasti/Reflection/ClassRegistry.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti::Tests
{

    TEST_CASE("CollisionSystem")
    {
        auto uuidGenerator = std::make_shared<UuidGenerator>();
        auto assetManager = std::make_shared<AssetManager>(uuidGenerator);
        auto classRegistry = std::make_shared<ClassRegistry>();
        auto eventManager = std::make_shared<EventManager>();
        auto frameTimer = std::make_shared<FrameTimer>();

        auto collision2dSystem = std::make_shared<Collision2dSystem>("Collision2dSystem", assetManager, classRegistry,
                                                                     eventManager, frameTimer);
        collision2dSystem->Setup();

        SUBCASE("Serialization")
        {
        }

        collision2dSystem->Shutdown();
    }

}
//---------------------------------------------------------------------------------------------------------------------