#include "doctest/doctest.h"
#include "Dynasti/Collision/Shape.h"
#include "Dynasti/Reflection/ClassRegistry.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti::Tests
{

    TEST_CASE("Circle")
    {
        ClassRegistry classRegistry;
        auto testClassReflection = classRegistry.RegisterClass<Circle>();

        Circle circle{{0.0f, 1.0f}, 2.0f};
        REQUIRE_EQ(circle.Area(), twoPi * 4.0f);
        REQUIRE_EQ(circle.COM().x, 0.0f);
        REQUIRE_EQ(circle.COM().y, 1.0f);

        SUBCASE("Serialization")
        {
            YAML::Emitter yamlEmitter;
            classRegistry.Serialize(yamlEmitter, &circle);
            std::string dataString = yamlEmitter.c_str();
            YAML::Node yamlNode = YAML::Load(dataString);
            std::cout << "DATA: " << std::endl << dataString << std::endl;

            Circle circle2;
            auto propertyData = yamlNode["Circle"];
            classRegistry.Deserialize(propertyData, &circle2);
            REQUIRE_EQ(circle2.pos.x, circle.pos.x);
            REQUIRE_EQ(circle2.pos.y, circle.pos.y);
            REQUIRE_EQ(circle2.radius, circle.radius);
        }
    }

    TEST_CASE("Box")
    {
        ClassRegistry classRegistry;
        auto testClassReflection = classRegistry.RegisterClass<Box>();

        Box box{{2.0f, 1.0f}, {3.0f, 4.0f}};
        REQUIRE_EQ(box.Area(), 12.0f);
        REQUIRE_EQ(box.COM().x, 2.0f);
        REQUIRE_EQ(box.COM().y, 1.0f);

        SUBCASE("Serialization")
        {
            YAML::Emitter yamlEmitter;
            classRegistry.Serialize(yamlEmitter, &box);
            std::string dataString = yamlEmitter.c_str();
            YAML::Node yamlNode = YAML::Load(dataString);
            std::cout << "DATA: " << std::endl << dataString << std::endl;

            Box box2;
            auto propertyData = yamlNode["Box"];
            classRegistry.Deserialize(propertyData, &box2);
            REQUIRE_EQ(box2.pos.x, box.pos.x);
            REQUIRE_EQ(box2.pos.y, box.pos.y);
            REQUIRE_EQ(box2.size.x, box.size.x);
            REQUIRE_EQ(box2.size.y, box.size.y);
        }
    }

}
//---------------------------------------------------------------------------------------------------------------------