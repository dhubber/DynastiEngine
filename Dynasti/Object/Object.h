#ifndef DYNASTI_OBJECT_H
#define DYNASTI_OBJECT_H

#include <concepts>
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Reflection/Reflectable.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Class defining interface for poolable objects; must provide 'OnCreate' and 'OnDestroy' functions.
    /// \author  D. A. Hubber
    /// \date    09/07/2023
    //=================================================================================================================
    class Object : public Reflectable
    {
    public:

        Uuid uuid{-1};                                 ///< Global id of object stored in pool

    };

    //=================================================================================================================
    /// \brief   Class defining interface for poolable objects; must provide 'OnCreate' and 'OnDestroy' functions.
    /// \author  D. A. Hubber
    /// \date    09/07/2023
    //=================================================================================================================
    class PoolableObject : public Object
    {
    public:

        /// Required function for 'creating' new objects in the pool
        virtual void OnCreate() = 0;

        /// Required function for 'destroying' objects in the pool
        virtual void OnDestroy() = 0;

    };

    template <typename T>
    concept IsPoolableObjectType = std::derived_from<T,PoolableObject>;

}
//---------------------------------------------------------------------------------------------------------------------
#endif
