#include "Dynasti/Object/ObjectPool.h"
#include "Dynasti/Reflection/ClassReflectionBase.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    void ObjectPoolBase::Serialize(YAML::Emitter &yamlEmitter)
    {
        if (NumObjects() == 0)
        {
            DYNASTI_WARNING("Could not serialize empty pool");
            return;
        }

        auto classReflection = classRegistry_->FindClass(GetType());
        DYNASTI_ASSERT(classReflection != nullptr, "Could not find class reflection");

        yamlEmitter << YAML::BeginMap;
        yamlEmitter << YAML::Key << "Pool" << YAML::Value << classReflection->GetClassName();
        yamlEmitter << YAML::Key << "Size" << YAML::Value << NumObjects();
        yamlEmitter << YAML::Key << "Items" << YAML::Value << YAML::BeginSeq;
        for (int i = 0; i < NumObjects(); ++i)
        {
            auto object = FindObjectWithLocalId(i);
            yamlEmitter << YAML::BeginMap;
            yamlEmitter << YAML::Key << "UUID" << YAML::Value << object->uuid;
            classRegistry_->SerializeProperties(yamlEmitter, object, classReflection);
            yamlEmitter << YAML::EndMap;
        }
        yamlEmitter << YAML::EndSeq;
        yamlEmitter << YAML::EndMap;
    }


    void ObjectPoolBase::Deserialize(YAML::Node &yamlNode)
    {
        auto classReflection = classRegistry_->FindClass(GetType());
        DYNASTI_ASSERT(classReflection != nullptr, "Could not find class reflection");
        auto numElements = yamlNode["Size"].as<std::int32_t>();
        if (numElements > 0) {
            auto items = yamlNode["Items"];
            for (auto objectData: items) {
                auto uuid = objectData["UUID"].as<Uuid>();
                auto object = NewObject(uuid);
                classRegistry_->DeserializeProperties(objectData, object, classReflection);
            }
        }
    }

}
//--------------------------------------------------------------------------------------------------------------------