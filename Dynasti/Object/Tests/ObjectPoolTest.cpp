#include "doctest/doctest.h"
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Reflection/ClassRegistry.h"
#include "Dynasti/Reflection/Property.h"
#include "Dynasti/Reflection/Reflectable.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    enum PoolableState
    {
        Constructed,
        AllocatedInPool,
        Deallocated
    };

    class TestObject : public PoolableObject
    {
    public:
        TestObject()
            : state(PoolableState::Constructed)
        {
        };

        std::type_index GetType() override { return std::type_index(typeid(TestObject)); }
        void OnCreate() override { state = PoolableState::AllocatedInPool; }
        void OnDestroy() override { state = PoolableState::Deallocated; }
        int state;

        static std::string Name() { return "TestObject"; }
        static std::vector<std::string> BaseClasses() { return {}; }
        static std::vector<PropertyPtr> Properties()
        {
            return std::vector<PropertyPtr>{
                    std::make_shared<BasicProperty<TestObject, int>>("state", &TestObject::state)
            };
        }
    };


    TEST_CASE("ObjectPool")
    {
        auto classRegistry = std::make_shared<ClassRegistry>();
        auto testClassReflection = classRegistry->RegisterClass<TestObject>();

        // Verify that an empty pool indeed has no objects and potentially no memory actually allocated
        ObjectPool<TestObject> emptyPool(classRegistry);
        REQUIRE_GE(emptyPool.MaxNumObjects(), 0);
        REQUIRE_EQ(emptyPool.NumObjects(), 0);

        // Verify that an allocated pool has enough memory allocated but no objects assigned
        ObjectPool<TestObject> dummyPool(classRegistry, 32);
        REQUIRE_GE(dummyPool.MaxNumObjects(), 32);
        REQUIRE_EQ(dummyPool.NumObjects(), 0);
        for (std::int32_t i = 0; i < dummyPool.MaxNumObjects(); ++i)
        {
            const TestObject &dummyObject = dummyPool[i];
            REQUIRE_EQ(dummyObject.state, PoolableState::Constructed);
        }

        // Create several new objects in the pool
        for (std::int32_t i = 0; i < 8; ++i)
        {
            const Uuid id = 100 * i;
            auto newObject = dummyPool.NewObject(id);
            REQUIRE_EQ(newObject->uuid, id);
            REQUIRE_EQ(newObject->state, PoolableState::AllocatedInPool);
        }
        REQUIRE_EQ(dummyPool.NumObjects(), 8);

        SUBCASE("Serialization")
        {
            YAML::Emitter yamlEmitter;
            dummyPool.Serialize(yamlEmitter);
            std::string dataString = yamlEmitter.c_str();
            YAML::Node yamlNode = YAML::Load(dataString);
            std::cout << dataString << std::endl;

            const auto tempFileName = "ObjectPool.yaml";
            std::ofstream fout(tempFileName);
            fout << yamlEmitter.c_str();

            ObjectPool<TestObject> dummyPool2(classRegistry);
            dummyPool2.Deserialize(yamlNode);
            REQUIRE_EQ(dummyPool.NumObjects(), dummyPool2.NumObjects());
        }

        // Verify that destroying existing objects works correctly
        REQUIRE_EQ(dummyPool.FreeObjectWithUuid(700), true);
        REQUIRE_EQ(dummyPool.NumObjects(), 7);
        REQUIRE_EQ(dummyPool.FindObjectWithUuid(700), nullptr);

        REQUIRE_EQ(dummyPool.FreeObject(dummyPool.FindLocalId(400)), true);
        REQUIRE_EQ(dummyPool.NumObjects(), 6);
        REQUIRE_EQ(dummyPool.FindObjectWithUuid(400), nullptr);

        // Verify that attempting to destroy non-existing objects is handled correctly
        REQUIRE_EQ(dummyPool.FreeObjectWithUuid(888), false);
        REQUIRE_EQ(dummyPool.FreeObject(12), false);
        REQUIRE_EQ(dummyPool.NumObjects(), 6);

        // Verify clear function, for freeing/destroying all currently allocated objects
        dummyPool.Clear();
        REQUIRE_GE(dummyPool.MaxNumObjects(), 32);
        REQUIRE_EQ(dummyPool.NumObjects(), 0);
    }

    TEST_CASE("ObjectPoolBase")
    {
        auto classRegistry = std::make_shared<ClassRegistry>();
        auto testClassReflection = classRegistry->RegisterClass<TestObject>();

        auto smallPool = std::make_unique<ObjectPool<TestObject>>(classRegistry, 32);
        REQUIRE_NE(smallPool, nullptr);
        REQUIRE_EQ(smallPool->NumObjects(), 0);
        REQUIRE_GE(smallPool->MaxNumObjects(), 32);

        auto obj = smallPool->NewObject(1001);
        REQUIRE_EQ(smallPool->NumObjects(), 1);
        REQUIRE_EQ(smallPool->FindLocalId(1001), 0);
        REQUIRE_EQ(smallPool->FindObjectWithUuid(1001), obj);

        smallPool->FreeObject(0);
        REQUIRE_EQ(smallPool->NumObjects(), 0);
        REQUIRE_EQ(smallPool->FindLocalId(1001), -1);
        REQUIRE_EQ(smallPool->FindObjectWithUuid(1001), nullptr);
    }

}
//---------------------------------------------------------------------------------------------------------------------