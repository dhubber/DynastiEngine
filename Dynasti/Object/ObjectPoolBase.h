#ifndef DYNASTI_OBJECT_POOL_BASE_H
#define DYNASTI_OBJECT_POOL_BASE_H

#include <memory>
#include <typeindex>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Object/Object.h"
#include "Dynasti/Reflection/ClassRegistryBase.h"
#include "Dynasti/Serialization/SerializableBase.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Base class for object pools. Poolable objects must provide 'OnCreate' and 'OnDestroy' functions.
    /// \author  D. A. Hubber
    /// \date    17/10/2022
    //=================================================================================================================
    class ObjectPoolBase : public SerializableBase
    {
    public:
        
        ObjectPoolBase(ClassRegistryPtr classRegistry)
            : classRegistry_(classRegistry)
        {
        }

        // Implementations of SerializableBase functions
        void Serialize(YAML::Emitter &yamlEmitter) override;
        void Deserialize(YAML::Node &yamlNode) override;

        virtual std::type_index GetType() = 0;
        virtual std::string GetName() = 0;
    
        /// Returns the reference to a newly created poolable object
        /// \param[in] uuid - Unique id of the newly created object
        /// \return - Reference to the newly created object
        virtual PoolableObject* NewObject(Uuid uuid) = 0;
    
        /// Gets a pointer to the object with the given local id
        /// \param[in] localId - Local id of the requested object
        /// \return - Pointer to the requested object
        virtual PoolableObject* FindObjectWithLocalId(std::int32_t localId) = 0;
    
        /// Gets a pointer to the object with the given local id
        /// \param[in] localId - Local id of the requested object
        /// \return - Pointer to the requested object
        virtual const PoolableObject* const FindObjectWithLocalId(std::int32_t localId) const = 0;
    
        /// Gets a pointer to the object with the given global id
        /// \param[in] uuid - Global id of the requested object
        /// \return - Pointer to the requested object
        virtual PoolableObject* FindObjectWithUuid(Uuid uuid) = 0;
    
        /// Frees object in the pool.  Moved to end of pool to maintain contiguous array of used objects.
        /// \param[in] localId - Local id of the object in the pool to be freed.
        virtual bool FreeObject(std::int32_t localId) = 0;
    
        /// Free object with the given associated global id
        /// \param[in] uuid - Global id associated with the object to be freed
        virtual bool FreeObjectWithUuid(Uuid uuid) = 0;
    
        /// Free/destroy all objects in pool, but keep all allocated memory
        virtual void Clear() = 0;

        /// \return - Max. number of objects in the pool
        virtual std::int32_t MaxNumObjects() const = 0;
    
        /// \return - True if the given local id contains a valid object in the pool (assigned or not); otherwise false
        inline bool IsLocalIdValid(const int localId) const { return (localId >= 0 && localId < MaxNumObjects()); }
        
        /// \return - True if the given local id contains a valid object in the pool (assigned or not); otherwise false
        inline bool IsLocalIdAssigned(const int localId) const { return (localId >= 0 && localId < numObjects_); }

        /// \return - Current number of objects in the pool
        inline std::int32_t NumObjects() const { return numObjects_; }

        /// Flag an object (via its UUID) as dirty
        inline void FlagDirty(const Uuid uuid) { dirtyObjects_.insert(uuid); }

        /// \return - Set of all dirty objects
        inline const std::unordered_set<Uuid>& GetDirtyObjectIds() const { return dirtyObjects_; }

        /// Clear the list of dirty flags
        inline void ClearDirtyFlags() { dirtyObjects_.clear(); }

    protected:

        ClassRegistryPtr classRegistry_;                   ///< Global class registry
        std::int32_t numObjects_{0};                       ///< No. of elements in pool
        std::unordered_map<Uuid, std::int32_t> idHash_;    ///< Hash table for mapping global to local ids
        std::unordered_set<Uuid> dirtyObjects_;            ///< Set of currently dirty object UUIDs
        
    };

    using ObjectPoolPtr = std::shared_ptr<ObjectPoolBase>;

}
//---------------------------------------------------------------------------------------------------------------------
#endif
