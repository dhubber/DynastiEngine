#ifndef DYNASTI_OBJECT_POOL_H
#define DYNASTI_OBJECT_POOL_H

#include <memory>
#include <typeindex>
#include <unordered_map>
#include <vector>
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Object/ObjectPoolBase.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Object pool for objects that also have a unique global integer id.
    /// \author  D. A. Hubber
    /// \date    17/10/2022
    //=================================================================================================================
    template <IsPoolableObjectType T>
    class ObjectPool : public ObjectPoolBase
    {
    public:
        
        using ObjectPoolBase::IsLocalIdAssigned;

        explicit ObjectPool(ClassRegistryPtr classRegistry, std::int32_t maxNumObjects=128)
            : ObjectPoolBase(classRegistry)
        {
            DYNASTI_LOG_VERBOSE("Constructing object pool for class: " + T::Name() + "    Capacity: " + std::to_string(maxNumObjects));
            objects_.resize(maxNumObjects, T());
            idHash_.reserve(maxNumObjects);
        }

        // Prevent copying of large pool objects (for performance reasons)
        ObjectPool(ObjectPool const &) = delete;
        void operator=(ObjectPool const &) = delete;

        // Implementations of SerializableBase functions
        std::type_index GetType() override { return std::type_index(typeid(T)); }
        std::string GetName() override { return T::Name(); }

        /// Assignment operator
        T& operator[] (const std::int32_t localId)
        {
            DYNASTI_WARNING_IF(!IsLocalIdAssigned(localId), "Object with id " + std::to_string(localId) + " not assigned in object pool ");
            DYNASTI_ASSERT(IsLocalIdValid(localId), "Invalid localId for ObjectPool : " + std::to_string(localId) + "  " + std::to_string(numObjects_));
            return objects_[localId];
        }
    
        /// Assignment operator (for read-only look-up)
        const T& operator[] (const std::int32_t localId) const
        {
            DYNASTI_WARNING_IF(!IsLocalIdAssigned(localId), "Object with id " + std::to_string(localId) + " not assigned in object pool ");
            DYNASTI_ASSERT(IsLocalIdValid(localId), "Invalid localId for ObjectPool : " + std::to_string(localId) + "  " + std::to_string(numObjects_));
            return objects_[localId];
        }
        
        /// Creates a new object with a given associated global id, and returns a reference to that new object
        /// \param[in] uuid - Unique global id associated with the newly created object
        /// \return - Reference to the new object
        T* NewObject(const Uuid uuid) override
        {
            DYNASTI_ASSERT(!IsFull(), "Reached capacity of ObjectPool : " + std::to_string(objects_.capacity()));
            DYNASTI_ASSERT(uuid >= 0, "Invalid global id provided for new object in pool : " + std::to_string(uuid));
            DYNASTI_ASSERT(FindLocalId(uuid) == -1, "Global id already registered : " + std::to_string(uuid));
            idHash_[uuid] = numObjects_;
            T &obj = objects_[numObjects_++];
            obj.uuid = uuid;
            obj.OnCreate();
            FlagDirty(uuid);
            return &obj;
        }
    
        /// If the pool contains an object with a given associated global id, then returns its local id
        /// \param[in] Uuid - Global id associated with the object
        /// \return - Local id of the object with the given global id; -1 if global id is not found.
        inline int FindLocalId(const Uuid Uuid) const
        {
            auto it = idHash_.find(Uuid);
            if (it != idHash_.end())
            {
                DYNASTI_ASSERT(IsLocalIdAssigned(it->second), "Invalid local id found for Uuid : " + std::to_string(Uuid));
                return it->second;
            }
            else
            {
                return -1;
            }
        }
    
        /// Gets a pointer to the object with the given local ids
        /// \param[in] localId - Local id of the requested object
        /// \return - Pointer to the requested object
        T* FindObjectWithLocalId(const std::int32_t localId) override
        {
            return (IsLocalIdAssigned(localId)) ? &(objects_[localId]) : nullptr;
        }
    
        /// Gets a pointer to the object with the given local id
        /// \param[in] localId - Local id of the requested object
        /// \return - Pointer to the requested object
        const T* const FindObjectWithLocalId(const std::int32_t localId) const override
        {
            return FindObjectWithLocalId(localId);
        }
        
        /// Gets a pointer to the object with the given global id
        /// \param[in] Uuid - Global id of the requested object
        /// \return - Pointer to the requested object
        T* FindObjectWithUuid(const Uuid Uuid) override
        {
            const int localId = FindLocalId(Uuid);
            return (localId != -1) ? &(objects_[localId]) : nullptr;
        }
        
        /// Frees object in the pool.  Moved to end of pool to maintain contiguous array of used objects.
        /// \param[in] localId - Local id of the object in the pool to be freed.
        bool FreeObject(const std::int32_t localId) override
        {
            // Return immediately with failure if local id is not valid for this pool
            if (!IsLocalIdAssigned(localId)) return false;

            T& object = objects_[localId];
            const Uuid uuid = objects_[localId].uuid;
            idHash_.erase(uuid);
            dirtyObjects_.erase(uuid);
            object.OnDestroy();
            object.uuid = -2;
            
            // Swap empty object slot with final allocated object to maintain contiguous array of objects in pool
            if (localId < numObjects_ - 1)
            {
                const Uuid newUuid = objects_[numObjects_ - 1].uuid;
                objects_[localId] = objects_[numObjects_ - 1];
                idHash_[newUuid] = localId;
            }
            --numObjects_;
            return true;
        }
    
        /// Free object with the given associated global id
        /// \param[in] Uuid - Global id associated with the object to be freed
        bool FreeObjectWithUuid(const Uuid Uuid) override
        {
            const int localId = FindLocalId(Uuid);
            return (localId != -1) ? FreeObject(localId) : false;
        }
        
        /// Free/destroy all objects in pool, but keep all allocated memory
        void Clear() override
        {
            for (int i = 0; i < numObjects_; ++i)
            {
                T& object = objects_[i];
                object.OnDestroy();
                object.uuid = -2;
            }
            numObjects_ = 0;
            idHash_.clear();
            ClearDirtyFlags();
        }
    
        /// \return - True if object pool has reached maximum capacity; otherwise false
        inline bool IsFull() const { return (numObjects_ == static_cast<int>(objects_.capacity())); }
    
        /// \return - Max. number of objects in the pool
        inline std::int32_t MaxNumObjects() const override { return objects_.size(); }

    protected:

        using ObjectPoolBase::numObjects_;
        using ObjectPoolBase::idHash_;
        std::vector<T> objects_;                  ///< Vector containing all objects in pool
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
