#ifndef DYNASTI_SCENE_H
#define DYNASTI_SCENE_H

#include "Dynasti/Asset/Asset.h"
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Uuid.h"
#include "Dynasti/Event/EventManager.h"
#include "Dynasti/Object/ObjectPool.h"
#include "Dynasti/Scene/Entity.h"
#include "Dynasti/Serialization/SerializableBase.h"
#include "Dynasti/Transform/Transform2dComponent.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    class AssetManager;
    class EventListener;
    class EventManager;

    //=================================================================================================================
    /// \brief   Class defining a single scene with all entities and components.
    /// \details A scene is comprised of a tree of entities, with child-parent and sibling-sibling relationships
    ///          between them. An empty scene contains a single entity, the root node. If entities are added to the
    ///          scene, then they must be parented either to the root node, or to another entity.
    /// \author  D. A. Hubber
    /// \date    21/10/2022
    //=================================================================================================================
    class Scene : public Asset, SerializableBase
    {
    public:

        static std::shared_ptr<Scene> mainScene;

        Scene(Uuid sceneId, const std::string& name, std::shared_ptr<AssetManager> assetManager,
              std::shared_ptr<EventManager> eventManager, std::shared_ptr<UuidGenerator> uuidGenerator,
              ClassRegistryPtr classRegistry, bool generateRoot=true);
        Scene(Scene const &) = delete;
        void operator=(Scene const &) = delete;

        // Implementations of serialization functions
        void Serialize(YAML::Emitter &yamlEmitter) override;
        void Deserialize(YAML::Node &yamlNode) override;

        std::type_index GetType() override { return std::type_index(typeid(Scene)); }

        void InstantiateIntoScene(Scene* targetScene, Uuid parentEntityInTarget=-1);

        /// Creates an entity with the given UUID
        Entity* CreateNewEntity(Uuid id, Uuid parentId, Uuid childId, Uuid nextId, const std::string name);

        /// Creates an entity with the given UUID
        Entity* CreateNewEntityWithUuid(Uuid id, Uuid parentId=-1, const std::string name="");

        /// Creates a new entity in the scene with an optional name
        Entity* CreateNewEntity(Uuid parentId=-1, const std::string name="");

        PoolableObject* CreateComponent(const Uuid entityId, std::type_index typeIndex);

        /// Creates and returns a new object pool containing components of the templated type (T).
        /// If T is derived from ScriptComponent, then this new pool is also registered for the script system.
        /// \param[in] componentName - Name of component class
        /// \param[in] maxNumComponents - Number of components to allocate in the pool (optional)
        /// \return - Pointer to the new component object pool
        std::shared_ptr<ObjectPoolBase> CreateComponentPool(std::type_index typeIndex, std::int32_t maxNumComponents=128);

        std::shared_ptr<ObjectPoolBase> CreateComponentPool(const std::string& name, std::int32_t maxNumComponents=128);

        ObjectPoolPtr FindOrCreateComponentPool(const std::string& name);

        ObjectPoolPtr FindOrCreateComponentPool(std::type_index typeIndex);

        /// Attach an entity to a given parent entity
        /// \param[inout] entity - Reference to entity that will be attached as a child
        /// \param[inout] parentEntity - Reference to entity that will parent the other entity
        /// \return - True if entity successfully atttached; otherwise false
        bool Attach(Uuid entityId, Uuid parentId);

        /// \param[in] entityId - Global id of entity to locate in scene
        /// \return - Pointer to entity with given id if found; otherwise returns nullptr
        inline Entity* FindEntity(const Uuid entityId) { return entityPool_.FindObjectWithUuid(entityId); }

        /// \return - Number of entites in the scene
        inline int NumEntities() const { return entityPool_.NumObjects(); }

        void BroadcastEvent(const Event event) { eventManager_->BroadcastEvent(event); }

        /// Creates a component of the type T to the entity with the given global id.  If the component itself has not
        /// yet been registered, then an object pool of that type is created before creating a component of that type.
        /// Also ensures that only a single script component type for any given entity can be created.
        /// \param entityId - Global id of the entity for which the component is being created
        /// \return - Pointer to newly formed component, or nullptr if no component was created
        template <typename T>
        T* CreateComponent(const Uuid entityId)
        {
            auto typeIndex = std::type_index(typeid(T));
            return static_cast<T*>(CreateComponent(entityId, typeIndex));
        }

        /// Finds and returns a pointer to the component of type T for an entity if one exists, or nullptr.
        /// \param entityId - Global id of entity to find component for
        /// \return - Pointer to the component type for the entity; if none exists, then returns nullptr
        template <typename T>
        inline T* FindComponent(const Uuid entityId)
        {
            std::shared_ptr<ObjectPool<T>> componentPool = FindComponentPool<T>();
            auto comp = (componentPool != nullptr) ? componentPool->FindObjectWithUuid(entityId) : nullptr;
            DYNASTI_WARNING_IF(comp == nullptr, "Could not find component for entity " + std::to_string(entityId));
            return comp;
        }

        /// Returns a pointer to the object pool containing all components of the templated type (T).
        /// \return - Pointer to the component object pool; if not yet created, then returns nullptr.
        template <typename T>
        inline std::shared_ptr<ObjectPool<T>> FindComponentPool()
        {
            auto typeIndex = std::type_index(typeid(T));
            return std::dynamic_pointer_cast<ObjectPool<T>>(FindComponentPool(typeIndex));
        }

        /// Returns a pointer to the object pool containing all components of the templated type (T).
        /// If a pool of that component type does not yet exist, then it is created, registered and returned.
        /// \return - Pointer to the component object pool
        template <typename T>
        inline std::shared_ptr<ObjectPool<T>> FindOrCreateComponentPool()
        {
            auto existingPool = FindComponentPool<T>();
            return (existingPool != nullptr) ? existingPool : CreateComponentPool<T>();
        }

        /// Creates and returns a new object pool containing components of the templated type (T).
        /// If T is derived from ScriptComponent, then this new pool is also registered for the script system.
        /// \param[in] maxNumComponents - Number of components to allocate in the pool (optional)
        /// \return - Point to the new component object pool
        template <typename T>
        std::shared_ptr<ObjectPool<T>> CreateComponentPool(const int maxNumComponents=128)
        {
            return std::dynamic_pointer_cast<ObjectPool<T>>(CreateComponentPool(std::type_index(typeid(T)), maxNumComponents));
        }

        // Getters
        inline const std::string& GetName() const { return name_; }
        inline Uuid GetRootId() const { return rootId_; }
        inline Uuid GetSceneId() const { return sceneId_; }
        inline std::vector<std::shared_ptr<ObjectPoolBase>> GetScriptPools() { return scriptComponentPools_; }
        inline std::shared_ptr<AssetManager> GetAssetManager() const { return assetManager_; }


    protected:

        Uuid sceneId_;                                         ///< Scene UUID
        Uuid rootId_;                                          ///< Scene entity root UUID
        std::shared_ptr<AssetManager> assetManager_;           ///< Global asset manager
        std::shared_ptr<UuidGenerator> uuidGenerator_;         ///< Global UUID generator
        ClassRegistryPtr classRegistry_;                       ///< Global class registry object
        std::shared_ptr<EventManager> eventManager_;       ///< Global event manager
        //std::shared_ptr<EventListener> eventListener_;     ///< Event listener for this system
        ObjectPool<Entity> entityPool_;                        ///< Object pool for creating new entities
        std::unordered_map<std::type_index, ObjectPoolPtr> componentPoolMap_;  ///< Type-map of all component pools
        std::unordered_map<std::string, ObjectPoolPtr> componentNameMap_;      ///< Name-map of all component pools
        std::vector<std::shared_ptr<ObjectPoolBase>> scriptComponentPools_;    ///< Array of all script pools

        /// Detach an entity from any parent it may be attached to
        /// \param[inout] entity - Reference to entity being detached
        bool Detach(Uuid entityId);

        /// Returns a pointer to the base object pool containing components of the class with the given type index.
        /// \return - Pointer to the component object pool; if not yet created, then returns nullptr.
        std::shared_ptr<ObjectPoolBase> FindComponentPool(const std::type_index typeIndex);

        /// Returns a pointer to the base object pool containing components of the class with the given class name
        /// \return - Pointer to the component object pool; if not yet created, then returns nullptr.
        std::shared_ptr<ObjectPoolBase> FindComponentPool(const std::string& className);

    };

    using ScenePtr = std::shared_ptr<Scene>;

}
//---------------------------------------------------------------------------------------------------------------------
#endif
