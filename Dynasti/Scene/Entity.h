#ifndef DYNASTI_ENTITY_H
#define DYNASTI_ENTITY_H


#include "Dynasti/Core/Constants.h"
#include "Dynasti/Object/Object.h"
#include "Dynasti/Reflection/Property.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Basic class defining a single entity and its relation to other entities in the scene graph.
    /// \author  D. A. Hubber
    /// \date    21/10/2022
    //=================================================================================================================
    class Entity : public PoolableObject
    {
    public:
        
        Uuid childId{-1};                                  ///< (1st) child entity UUID
        Uuid nextId{-1};                                   ///< UUID of next sibling (i.e. owned by same parent)
        Uuid parentId{-1};                                 ///< Parent entity UUID
        std::string name{};                                ///< Human-readable name of entity

        static std::string Name() { return "Entity"; }
        static std::vector<std::string> BaseClasses() { return {}; }
        static std::vector<PropertyPtr> Properties()
        {
            return std::vector<PropertyPtr>{
                std::make_shared<BasicProperty<Entity, Uuid>>("Child ID", &Entity::childId),
                std::make_shared<BasicProperty<Entity, Uuid>>("Next ID", &Entity::nextId),
                std::make_shared<BasicProperty<Entity, Uuid>>("Parent ID", &Entity::parentId),
                std::make_shared<BasicProperty<Entity, std::string>>("Name", &Entity::name),
            };
        }

        std::type_index GetType() override { return std::type_index(typeid(Entity)); }
        void OnCreate() override {};
        void OnDestroy() override
        {
            uuid = -1;
            childId = -1;
            nextId = -1;
            parentId = -1;
            name = "";
        };

    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
