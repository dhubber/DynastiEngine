#ifndef DYNASTI_SCRIPT_COMPONENT_H
#define DYNASTI_SCRIPT_COMPONENT_H

#include <memory>
#include "Dynasti/Event/Event.h"
#include "Dynasti/Object/Object.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    class FrameTimer;
    class Scene;

    //=================================================================================================================
    /// \brief   Base class for all scriptable components.
    /// \author  D. A. Hubber
    /// \date    13/02/2020
    //=================================================================================================================
    class ScriptComponentBase : public PoolableObject
    {
    public:

        /// Function for updating the entity every frame
        virtual void OnUpdate(std::shared_ptr<Scene> scene, std::shared_ptr<FrameTimer> frameTimer) {};

        /// Function for responding to any general events involving this entity
        /// (Note: This should include special events, such as overlap events, which are treated in special functions)
        /// \param[in] event - Event object containing all relevant data
        /// \param[in] scene - Pointer to scene that entity exists in
        virtual void OnEvent(const Event event, std::shared_ptr<Scene> scene) {};

        /// Callback when a collider attached to this entity begins overlapping with another collider
        /// \param[in] event - OverlapBegin event object containing all relevant data
        /// \param[in] scene - Pointer to scene that entity exists in
        virtual void OnOverlapBeginEvent(const Event event, std::shared_ptr<Scene> scene) {};

        /// Function for responding to any event involving this entity
        /// \param[in] event - OverlapEnd event object containing all relevant data
        /// \param[in] scene - Pointer to scene that entity exists in
        virtual void OnOverlapEndEvent(const Event event, std::shared_ptr<Scene> scene) {};

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
