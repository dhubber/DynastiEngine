#include "Dynasti/Application/Application.h"
#include "Dynasti/Event/EventManager.h"
#include "Dynasti/Reflection/ClassRegistry.h"
#include "Dynasti/Scene/Scene.h"
#include "Dynasti/Scene/SceneSystem.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    void SceneSystem::RegisterClasses()
    {
        classRegistry_->RegisterClass<Entity>();
        classRegistry_->RegisterClass<Transform2d>();
        classRegistry_->RegisterClass<Transform2dComponent>();
    }

    void SceneSystem::Setup()
    {
        updateWorldPositionEventType_ = eventManager_->RegisterEvent("UpdateWorldPosition");
        updateModelMatrixEventType_ = eventManager_->RegisterEvent("UpdateModelMatrix");
        overlapBeginEventType_ = eventListener_->SubscribeToEvent("OverlapBegin", std::bind(&SceneSystem::OnOverlapBeginEvent, this, std::placeholders::_1));
        overlapEndEventType_ = eventListener_->SubscribeToEvent("OverlapEnd", std::bind(&SceneSystem::OnOverlapEndEvent, this, std::placeholders::_1));

        eventListener_->SubscribeToEvent("LeftActionKeyDown", std::bind(&SceneSystem::OnEvent, this, std::placeholders::_1));
        eventListener_->SubscribeToEvent("LeftActionKeyUp", std::bind(&SceneSystem::OnEvent, this, std::placeholders::_1));
        eventListener_->SubscribeToEvent("RightActionKeyDown", std::bind(&SceneSystem::OnEvent, this, std::placeholders::_1));
        eventListener_->SubscribeToEvent("RightActionKeyUp", std::bind(&SceneSystem::OnEvent, this, std::placeholders::_1));
        eventListener_->SubscribeToEvent("FireActionKeyDown", std::bind(&SceneSystem::OnEvent, this, std::placeholders::_1));

        mainScene_ = CreateNewScene("MainScene");
        Scene::mainScene = mainScene_;
    }

    void SceneSystem::Update()
    {
        DYNASTI_TIMER(SceneSystem)

        if (mainScene_ != nullptr)
        {
            // Call update function on all scripts in the main scene
            UpdateScriptComponents(mainScene_);

            // Broadcast updated model matrix for any entities with dirty transforms
            auto transformPool = mainScene_->FindOrCreateComponentPool<Transform2dComponent>();
            for (int i = 0; i < transformPool->NumObjects(); ++i) {
                Transform2dComponent& tComp = (*transformPool)[i];
                if (tComp.IsDirty()) {
                    tComp.UpdateModelMatrix();
                    Event updateModelMatrixEvent(updateModelMatrixEventType_, tComp.uuid, tComp.GetModelMatrix());
                    eventManager_->BroadcastEvent(updateModelMatrixEvent);

                    Event updateWorldPositionEvent(updateWorldPositionEventType_, tComp.uuid, tComp.GetPosition());
                    eventManager_->BroadcastEvent(updateWorldPositionEvent);
                }
            }
        }
    }

    void SceneSystem::Shutdown()
    {
    }

    std::shared_ptr<Scene> SceneSystem::CreateNewScene(const std::string &name)
    {
        auto uuid = uuidGenerator_->GenerateUuid();
        return CreateNewScene(name, uuid);
    }

    std::shared_ptr<Scene> SceneSystem::CreateNewScene(const std::string &name, Uuid sceneId)
    {
        auto newScene = std::make_shared<Scene>(sceneId, name, assetManager_, eventManager_, uuidGenerator_, classRegistry_);
        sceneMap_[sceneId] = newScene;
        return newScene;
    }

    void SceneSystem::UpdateScriptComponents(ScenePtr scene)
    {
        auto scriptPools = scene->GetScriptPools();
        for (auto scriptPool : scriptPools) {
            for (int i = 0; i < scriptPool->NumObjects(); ++i) {
                ScriptComponentBase* scriptComp = dynamic_cast<ScriptComponentBase*>(scriptPool->FindObjectWithLocalId(i));
                DYNASTI_ASSERT(scriptComp != nullptr, "Error casting to ScriptComponent");
                scriptComp->OnUpdate(scene, frameTimer_);
            }
        }
    }

    void SceneSystem::OnOverlapBeginEvent(Event event)
    {
        DYNASTI_ASSERT(event.GetType() == overlapBeginEventType_, "Wrong event type in callback");
        OnScriptEvent(event, &ScriptComponentBase::OnOverlapBeginEvent);
    }

    void SceneSystem::OnOverlapEndEvent(Event event)
    {
        DYNASTI_ASSERT(event.GetType() == overlapEndEventType_, "Wrong event type in callback");
        OnScriptEvent(event, &ScriptComponentBase::OnOverlapEndEvent);
    }

    void SceneSystem::OnEvent(Event event)
    {
        OnScriptEvent(event, &ScriptComponentBase::OnEvent);
    }

    void SceneSystem::OnScriptEvent(Event event, void(ScriptComponentBase::*fp)(Event, std::shared_ptr<Scene>))
    {
        const auto entityId = event.GetId();
        if (mainScene_ != nullptr)
        {
            // Call update functions on all attached scripts
            auto scriptPools = mainScene_->GetScriptPools();
            for (auto scriptPool: scriptPools)
            {
                if (entityId == -1)
                {
                    for (std::size_t i = 0; i < scriptPool->NumObjects(); ++i)
                    {
                        auto obj = scriptPool->FindObjectWithLocalId(i);
                        ScriptComponentBase *scriptComp = dynamic_cast<ScriptComponentBase *>(obj);
                        if (scriptComp)
                        {
                            (scriptComp->*fp)(event, mainScene_);
                        }
                    }
                }
                else
                {
                    auto obj = scriptPool->FindObjectWithUuid(entityId);
                    ScriptComponentBase *scriptComp = dynamic_cast<ScriptComponentBase *>(obj);
                    if (scriptComp)
                    {
                        (scriptComp->*fp)(event, mainScene_);
                    }
                }
            }
        }
    }

}
//---------------------------------------------------------------------------------------------------------------------
