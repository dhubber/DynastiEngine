#ifndef DYNASTI_SCENE_SYSTEM_H
#define DYNASTI_SCENE_SYSTEM_H

#include <functional>
#include <memory>
#include <unordered_map>
#include "Dynasti/Application/System.h"
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Scene/ScriptComponent.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    class EventManager;
    class Scene;
    class UuidGenerator;

    //=================================================================================================================
    /// \brief   System for creating and managing all Windows during the application lifetime.
    /// \author  D. A. Hubber
    /// \date    05/08/2022
    //=================================================================================================================
    class SceneSystem : public System
    {
    public:

        SceneSystem(const std::string& name,
                    std::shared_ptr<AssetManager> assetManager,
                    std::shared_ptr<ClassRegistry> classRegistry,
                    std::shared_ptr<EventManager> eventManager,
                    std::shared_ptr<FrameTimer> frameTimer,
                    std::shared_ptr<UuidGenerator> uuidGenerator)
            : System(name, assetManager, classRegistry, eventManager, frameTimer)
            , uuidGenerator_(uuidGenerator)
        {
        }

        // Implementations of System virtual functions
        void RegisterClasses() override;
        void Setup() override;
        void Shutdown() override;
        void Update() override;

        std::shared_ptr<Scene> CreateNewScene(const std::string& name);
        std::shared_ptr<Scene> CreateNewScene(const std::string& name, Uuid sceneId);

        std::size_t NumRegisteredScenes() const { return sceneMap_.size(); }

    protected:

        ScenePtr mainScene_{nullptr};
        std::shared_ptr<UuidGenerator> uuidGenerator_;
        std::unordered_map<Uuid, std::shared_ptr<Scene>> sceneMap_;

        EventType overlapBeginEventType_;
        EventType overlapEndEventType_;
        EventType updateModelMatrixEventType_;
        EventType updateWorldPositionEventType_;

        EventType leftActionKeyDownEventType_;
        EventType leftActionKeyUpEventType_;
        EventType rightActionKeyDownEventType_;
        EventType rightActionKeyUpEventType_;
        EventType fireActionKeyDownEventType_;

        void UpdateScriptComponents(ScenePtr scene);

        void OnOverlapBeginEvent(Event event);
        void OnOverlapEndEvent(Event event);
        void OnEvent(Event event);
        void OnScriptEvent(Event event, void(ScriptComponentBase::*fp)(Event,std::shared_ptr<Scene>));
    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif