#include "doctest/doctest.h"
#include "Dynasti/Asset/AssetManager.h"
#include "Dynasti/Core/FrameTimer.h"
#include "Dynasti/Event/EventManager.h"
#include "Dynasti/Object/ObjectPool.h"
#include "Dynasti/Reflection/ClassRegistry.h"
#include "Dynasti/Scene/Scene.h"
#include "Dynasti/Scene/SceneSystem.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    class TestComponent : public PoolableObject
    {
    public:

        std::string dummyData;

        void OnCreate() override {};
        void OnDestroy() override {};

        std::type_index GetType() override { return std::type_index(typeid(TestComponent)); }

        static std::string Name() { return "TestComponent"; }
        static std::vector<std::string> BaseClasses() { return {}; }
        static std::vector<PropertyPtr> Properties()
        {
            return std::vector<PropertyPtr>{
                std::make_shared<BasicProperty<TestComponent, std::string>>("dummyData", &TestComponent::dummyData)
            };
        }
    };

    class TestScriptComponent : public ScriptComponentBase
    {
    public:

        int counter = 0;

        void OnCreate() override {};
        void OnDestroy() override {};

        void OnUpdate(std::shared_ptr<Scene> scene, std::shared_ptr<FrameTimer> frameTimer) override
        {
        }

        void OnEvent(const Event event, std::shared_ptr<Scene> scene) override
        {
            if (uuid == event.GetId())
            {
                ++counter;
            }
        }

        std::type_index GetType() override { return std::type_index(typeid(TestScriptComponent)); }

        static std::string Name() { return "TestScriptComponent"; }
        static std::vector<std::string> BaseClasses() { return {}; }
        static std::vector<PropertyPtr> Properties()
        {
            return std::vector<PropertyPtr>{
                std::make_shared<BasicProperty<TestScriptComponent, int>>("i", &TestScriptComponent::counter)
            };
        }

    };

    void PopulateSimpleScene(std::shared_ptr<Scene> scene)
    {
        Entity* child1 = scene->CreateNewEntity(scene->GetRootId(), "Child1");
        Entity* child2 = scene->CreateNewEntity(scene->GetRootId(), "Child2");
        Entity* grandChild1 = scene->CreateNewEntity(child1->uuid, "GrandChild1");

        scene->CreateComponent<TestComponent>(child1->uuid);
        scene->CreateComponent<TestComponent>(grandChild1->uuid);
    }

    TEST_CASE("Scene")
    {
        auto uuidGenerator = std::make_shared<UuidGenerator>();
        auto classRegistry = std::make_shared<ClassRegistry>();
        auto assetManager = std::make_shared<AssetManager>(uuidGenerator);
        auto eventManager = std::make_shared<EventManager>();

        classRegistry->RegisterClass<Entity>();
        classRegistry->RegisterClass<TestComponent>();
        classRegistry->RegisterClass<TestScriptComponent>();

        // Verify that scene is constructed correctly, including adding a default root entity
        SUBCASE("Scene constructor")
        {
            const Uuid sceneId = uuidGenerator->GenerateUuid();
            Scene trivialScene(sceneId, "TrivialScene", assetManager, eventManager, uuidGenerator, classRegistry);
            REQUIRE_EQ(trivialScene.GetName(), "TrivialScene");
            auto rootId = trivialScene.GetRootId();
            auto rootEntity = trivialScene.FindEntity(rootId);
            REQUIRE_NE(rootEntity, nullptr);
            REQUIRE_EQ(rootEntity->uuid, rootId);
            REQUIRE_EQ(rootEntity->name, "TrivialScene_Root");
        }

        SUBCASE("Hierarchical Scene")
        {
            auto simpleScene = std::make_shared<Scene>(uuidGenerator->GenerateUuid(), "SimpleScene", assetManager,
                                                      eventManager, uuidGenerator, classRegistry);
            PopulateSimpleScene(simpleScene);
            REQUIRE_EQ(simpleScene->NumEntities(), 4);
        }

        SUBCASE("Serialization")
        {
            auto simpleScene = std::make_shared<Scene>(uuidGenerator->GenerateUuid(), "SimpleScene", assetManager,
                                                       eventManager, uuidGenerator, classRegistry);
            PopulateSimpleScene(simpleScene);
            YAML::Emitter yamlEmitter;
            simpleScene->Serialize(yamlEmitter);
            std::string dataString = yamlEmitter.c_str();
            std::cout << dataString << std::endl;
            YAML::Node yamlNode = YAML::Load(dataString);

            const auto tempFileName = "SceneTest.yaml";
            std::ofstream fout(tempFileName);
            fout << yamlEmitter.c_str();
            fout.close();

            auto simpleScene2 = std::make_shared<Scene>(uuidGenerator->GenerateUuid(), "TestScene2", assetManager,
                                                        eventManager, uuidGenerator, classRegistry, false);
            simpleScene2->Deserialize(yamlNode);
            REQUIRE_EQ(simpleScene2->NumEntities(), simpleScene->NumEntities());
            REQUIRE_EQ(simpleScene2->GetRootId(), simpleScene->GetRootId());
        }
    }

    TEST_CASE("SceneSystem")
    {
        auto classRegistry = std::make_shared<ClassRegistry>();
        auto uuidGenerator = std::make_shared<UuidGenerator>();
        auto assetManager = std::make_shared<AssetManager>(uuidGenerator);
        auto eventManager = std::make_shared<EventManager>();
        auto frameTimer = std::make_shared<FrameTimer>();
        auto sceneSystem = SceneSystem("TestSceneSystem", assetManager, classRegistry, eventManager, frameTimer, uuidGenerator);
        REQUIRE_EQ(sceneSystem.NumRegisteredScenes(), 0);

        auto scene1 = sceneSystem.CreateNewScene("Scene 1", 111);
        REQUIRE_EQ(sceneSystem.NumRegisteredScenes(), 1);

        auto scene2 = sceneSystem.CreateNewScene("Scene 2", 222);
        REQUIRE_EQ(sceneSystem.NumRegisteredScenes(), 2);

        SUBCASE("Instantiation")
        {
        }

        SUBCASE("Serialization")
        {
        }

    }

}
//---------------------------------------------------------------------------------------------------------------------