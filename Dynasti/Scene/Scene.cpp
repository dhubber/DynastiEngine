#include "Dynasti/Scene/Scene.h"

#include "Dynasti/Event/EventManager.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    std::shared_ptr<Scene> Scene::mainScene = nullptr;


    Scene::Scene(Uuid sceneId, const std::string& name, std::shared_ptr<AssetManager> assetManager,
                 std::shared_ptr<EventManager> eventManager, std::shared_ptr<UuidGenerator> uuidGenerator,
                 ClassRegistryPtr classRegistry, bool generateRoot)
        : Asset(name)
        , sceneId_(sceneId)
        , assetManager_(assetManager)
        , uuidGenerator_(uuidGenerator)
        , classRegistry_(classRegistry)
        , eventManager_(eventManager)
        , entityPool_(classRegistry)
    {
        if (generateRoot) {
            rootId_ = uuidGenerator->GenerateUuid();
            Entity* rootEntity = entityPool_.NewObject(rootId_);
            rootEntity->name = name + "_Root";
        }
    }

    Entity* Scene::CreateNewEntity(Uuid id, Uuid parentId, Uuid childId, Uuid nextId, const std::string name)
    {
        Entity* newEntity = entityPool_.NewObject(id);
        newEntity->parentId = parentId;
        newEntity->childId = childId;
        newEntity->nextId = nextId;
        newEntity->name = name;
        return newEntity;
    }

    Entity* Scene::CreateNewEntityWithUuid(Uuid id, Uuid parentId, const std::string name)
    {
        Entity* newEntity = entityPool_.NewObject(id);
        newEntity->name = name;
        if (parentId != -1)
            Attach(id, parentId);
        else
            Attach(id, rootId_);
        return newEntity;
    }

    Entity* Scene::CreateNewEntity(const Uuid parentId, const std::string name)
    {
        Uuid entityId = uuidGenerator_->GenerateUuid();
        return CreateNewEntityWithUuid(entityId, parentId, name);
    }

    PoolableObject* Scene::CreateComponent(const Uuid entityId, std::type_index typeIndex)
    {
        auto componentPool = FindOrCreateComponentPool(typeIndex);
        DYNASTI_ASSERT(componentPool, "Pointer to component pool invalid");

        if (componentPool->FindObjectWithUuid(entityId) != nullptr)
        {
            DYNASTI_WARNING("Component of type T already exists for entity " + std::to_string(entityId));
            return nullptr;
        }
        return componentPool->NewObject(entityId);
    }

    bool Scene::Attach(Uuid entityId, Uuid parentId)
    {
        Entity* entity = FindEntity(entityId);
        Entity* parent = FindEntity(parentId);
        DYNASTI_ASSERT(entity != nullptr, "Invalid pointer to entity");
        DYNASTI_ASSERT(parent != nullptr, "Invalid pointer to parent entity");

        // If already attached to another entity, then detach first before attaching to the new parent
        if (entity->parentId != -1)
        {
            Detach(entityId);
        }

        // Record the old child id (if any were present)
        const Uuid oldChildId = parent->childId;
        parent->childId = entityId;

        // Set reciprocal id of parent to child, and update linked list of children (if more than one)
        entity->parentId = parentId;
        if (oldChildId != -1) entity->nextId = oldChildId;

        return true;
    }

    bool Scene::Detach(Uuid entityId)
    {
        Entity* entity = FindEntity(entityId);
        DYNASTI_ASSERT(entity != nullptr, "Invalid pointer to entity");

        const Uuid nextId = entity->nextId;
        const Uuid parentId = entity->parentId;
        DYNASTI_ASSERT(parentId != -1, "Invalid parent id for entity when detaching");

        entity->nextId = -1;
        entity->parentId = -1;

        Entity* parent = entityPool_.FindObjectWithUuid(parentId);
        DYNASTI_ASSERT(parent != nullptr, "Invalid pointer to parent entity when detaching");

        // If entity is the first in the linked list of siblings, then make parent point directly to the second child
        Uuid id = parent->childId;
        if (id == entity->uuid)
        {
            parent->childId = nextId;
            return true;
        }

        // Otherwise iterate through all children to find the required entity in the linked list
        while (id != -1)
        {
            Entity* nextEntity = entityPool_.FindObjectWithUuid(id);
            id = nextEntity->nextId;
            if (id == entity->uuid)
            {
                nextEntity->nextId = nextId;
                return true;
            }
        }

        // Should not get here really so print an error that something is wrong
        DYNASTI_FATAL("Could not find entity in linked list of parent#s children : " + std::to_string(entity->uuid));
        return false;
    }

    void Scene::InstantiateIntoScene(Scene* targetScene, Uuid parentEntityInTarget)
    {
        // Iterate through scene template and make list of uuids
        // Generate new uuids for all entities and create map of old to new entities
        std::unordered_map<Uuid, Uuid> instanceIdMap;
        for (std::int32_t i = 0; i < entityPool_.NumObjects(); ++i)
        {
            Uuid entityId = entityPool_[i].uuid;
            instanceIdMap[entityId] = uuidGenerator_->GenerateUuid();
        }

        for (std::int32_t i = 0; i < entityPool_.NumObjects(); ++i)
        {
            const auto& entity = entityPool_[i];
            Uuid entityId = instanceIdMap[entity.uuid];
            Uuid parentId = instanceIdMap[entity.parentId];
            Uuid childId = instanceIdMap[entity.childId];
            Uuid nextId = instanceIdMap[entity.nextId];
            targetScene->CreateNewEntity(entityId, parentId, childId, nextId, entity.name);
        }

        // Attach instance of root to the parent in the target scene
        targetScene->Attach(instanceIdMap[rootId_], parentEntityInTarget);

        // Create copies of all components from template scene to current scene (map to new ids)
        for (auto it : componentPoolMap_)
        {
            auto typeIndex = it.first;
            auto pool = it.second;
            auto classReflection = classRegistry_->FindClass(typeIndex);
            for (std::int32_t i = 0; i < pool->NumObjects(); ++i)
            {
                auto origComp = pool->FindObjectWithLocalId(i);
                Uuid entityId = instanceIdMap[origComp->uuid];
                auto targetComp = targetScene->CreateComponent(entityId, typeIndex);
                classReflection->CopyProperties(origComp, targetComp);
            }
        }
    }

    std::shared_ptr<ObjectPoolBase> Scene::CreateComponentPool(std::type_index typeIndex, std::int32_t maxNumComponents)
    {
        DYNASTI_ASSERT(FindComponentPool(typeIndex) == nullptr, "Component pool already exists");
        auto classReflection = classRegistry_->FindClass(typeIndex);
        auto newPool = classReflection->CreateObjectPool(classRegistry_, maxNumComponents);
        componentPoolMap_[typeIndex] = newPool;
        componentNameMap_[classReflection->GetClassName()] = newPool;

        // If component type is a scriptable component, then record pool separately for script updating
        if (classReflection->IsScriptableClass())
        {
            scriptComponentPools_.push_back(newPool);
        }

        return newPool;
    }

    std::shared_ptr<ObjectPoolBase> Scene::CreateComponentPool(const std::string& name, std::int32_t maxNumComponents)
    {
        DYNASTI_ASSERT(FindComponentPool(name) == nullptr, "Component pool already exists");
        auto classReflection = classRegistry_->FindClass(name);
        auto newPool = classReflection->CreateObjectPool(classRegistry_, maxNumComponents);
        componentPoolMap_[classReflection->GetType()] = newPool;
        componentNameMap_[name] = newPool;

        // If component type is a scriptable component, then record pool separately for script updating
        if (classReflection->IsScriptableClass())
        {
            scriptComponentPools_.push_back(newPool);
        }

        return newPool;
    }

    ObjectPoolPtr Scene::FindOrCreateComponentPool(const std::type_index typeIndex)
    {
        auto existingPool = FindComponentPool(typeIndex);
        return (existingPool != nullptr) ? existingPool : CreateComponentPool(typeIndex);
    }

    ObjectPoolPtr Scene::FindOrCreateComponentPool(const std::string& name)
    {
        auto existingPool = FindComponentPool(name);
        return (existingPool != nullptr) ? existingPool : CreateComponentPool(name);
    }

    std::shared_ptr<ObjectPoolBase> Scene::FindComponentPool(const std::type_index typeIndex)
    {
        auto it = componentPoolMap_.find(typeIndex);
        return (it != componentPoolMap_.end()) ? it->second : nullptr;
    }

    std::shared_ptr<ObjectPoolBase> Scene::FindComponentPool(const std::string& className)
    {
        auto it = componentNameMap_.find(className);
        return (it != componentNameMap_.end()) ? it->second : nullptr;
    }

    void Scene::Serialize(YAML::Emitter &yamlEmitter)
    {
        yamlEmitter << YAML::BeginMap;
        yamlEmitter << YAML::Key << "Scene Name" << YAML::Value << name_;
        yamlEmitter << YAML::Key << "Scene ID" << YAML::Value << sceneId_;
        yamlEmitter << YAML::Key << "Root ID" << YAML::Value << rootId_;

        yamlEmitter << YAML::Key << "Entities" << YAML::Value;
        entityPool_.Serialize(yamlEmitter);

        yamlEmitter << YAML::Key << "Components" << YAML::Value << YAML::BeginSeq;
        for (auto it : componentPoolMap_)
        {
            auto pool = it.second;
            pool->Serialize(yamlEmitter);
        }
        yamlEmitter << YAML::EndSeq;
        yamlEmitter << YAML::EndMap;
    }

    void Scene::Deserialize(YAML::Node &yamlNode)
    {
        // Clear scene data before deserializing new data into scene object
        entityPool_.Clear();
        for (auto pool: componentPoolMap_) {
            pool.second->Clear();
        }

        name_ = yamlNode["Scene Name"].as<std::string>();
        sceneId_ = yamlNode["Scene ID"].as<Uuid>();
        rootId_ = yamlNode["Root ID"].as<Uuid>();

        auto entities = yamlNode["Entities"];
        if (entities) {
            entityPool_.Deserialize(entities);
        }

        auto components = yamlNode["Components"];
        if (components) {
            for (auto componentTypeData : components) {
                auto className = componentTypeData["Pool"].as<std::string>();
                auto numComponents = componentTypeData["Size"].as<std::int32_t>();
                DYNASTI_ASSERT(numComponents > 0, "Deserializing class with no components: " + className);
                auto pool = FindOrCreateComponentPool(className);
                DYNASTI_ASSERT(pool != nullptr, "Could not find named component pool: " + className);
                pool->Deserialize(componentTypeData);
            }
        }
    }

}
//---------------------------------------------------------------------------------------------------------------------