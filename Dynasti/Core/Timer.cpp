#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>
#include <thread>
#include "Dynasti/Core/Timer.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    TimerBlock::TimerBlock(const std::string& name)
        : threadId_(std::this_thread::get_id())
        , name_(name)
        , startTime_(std::chrono::high_resolution_clock::now())
    {
    }


    TimerBlock::~TimerBlock()
    {
        Timer &timer = Timer::GetInstance();
        timer.RecordTimerBlock(*this);
    }


    double TimerBlock::GetAccumulatedTime() const
    {
        const auto currentTime = std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<std::chrono::duration<double>>(currentTime - startTime_).count();
    }


    Timer::Timer(const std::string& timerTag)
        : filename_(timerTag + ".timing")
        , globalTimerBlock_("GlobalTime")
    {
    }


    Timer::~Timer()
    {
        WriteTimingStatistics(filename_);
    }


    void Timer::RecordTimerBlock(const TimerBlock &block)
    {
        std::unique_lock<std::mutex> lock(timerMutex_);
        const std::string key = block.GetName();
        if (totalTimes_.find(key) == totalTimes_.end())
        {
            totalTimes_[key] = 0.0;
        }
        totalTimes_[key] += block.GetAccumulatedTime();
    }


    void Timer::WriteTimingStatistics(const std::string filename)
    {
        std::unique_lock<std::mutex> lock(timerMutex_);
        const auto totalTime = globalTimerBlock_.GetAccumulatedTime();
        if (totalTime <= 0.0) return;

        std::ofstream outfile;
        outfile.open(filename.c_str());

        std::map <std::string, double>::iterator it;
        for (it=totalTimes_.begin(); it != totalTimes_.end(); ++it)
        {
            outfile << std::fixed << std::setw(30) << it->first << "    t : "
                    << std::setw(8) << std::setprecision(5) << it->second << "     % : "
                    << std::setw(8) << std::setprecision(5) << 100.0*it->second/totalTime << std::endl;
        }

        outfile.close();
    }

}
//---------------------------------------------------------------------------------------------------------------------
