#ifndef DYNASTI_UUID_H
#define DYNASTI_UUID_H


#include <random>
#include <string>
#include "Dynasti/Core/Debug.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Generates 64-bit Universally-unique ids (UUIDs) using C++ random number algorithms.
    /// \author  D. A. Hubber
    /// \date    12/06/2020
    //=================================================================================================================
    class UuidGenerator
    {
    public:

        UuidGenerator()
            : randomDevice{}
            , twisterEngine(randomDevice())
            , intDist{}
        {
            DYNASTI_WARNING_IF(randomDevice.entropy() == 0, "Zero entropy detected in random device");
        }

        UuidGenerator(UuidGenerator const &) = delete;
        void operator=(UuidGenerator const &) = delete;

        Uuid GenerateUuid()
        {
            return intDist(twisterEngine);
        }


    protected:

        std::random_device randomDevice;
        std::mt19937_64 twisterEngine;
        std::uniform_int_distribution<std::int64_t> intDist;

    };

    using UuidGeneratorPtr = std::shared_ptr<UuidGenerator>;

}
//---------------------------------------------------------------------------------------------------------------------
#endif