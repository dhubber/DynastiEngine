#ifndef DYNASTI_DEBUG_H
#define DYNASTI_DEBUG_H


#include <assert.h>
#include <cmath>
#include <iostream>
#include "Dynasti/Core/Logger.h"
#include "Dynasti/Core/Timer.h"


#if defined(DYNASTI_USE_LOGGER)
#define DYNASTI_LOG_INIT(LOGTAG, LOGMODE, TERMINALMODE) Dynasti::Logger::GetInstance(LOGTAG, LOGMODE, TERMINALMODE);
#define DYNASTI_LOG(MESSAGE) Dynasti::Logger::GetInstance().Log(__FILE__, __LINE__, MESSAGE);
#define DYNASTI_LOG_IF(CONDITION, MESSAGE) if (CONDITION) Dynasti::Logger::GetInstance().Log(__FILE__, __LINE__, MESSAGE);
#define DYNASTI_LOG_VERBOSE(MESSAGE) Dynasti::Logger::GetInstance().LogVerbose(__FILE__, __LINE__, MESSAGE);
#define DYNASTI_LOG_VERBOSE_IF(CONDITION, MESSAGE) if (CONDITION) Dynasti::Logger::GetInstance().LogVerbose(__FILE__, __LINE__, MESSAGE);
#define DYNASTI_LOG_DEBUG(MESSAGE) Dynasti::Logger::GetInstance().LogDebug(__FILE__, __LINE__, MESSAGE);
#define DYNASTI_LOG_DEBUG_IF(CONDITION, MESSAGE) if (CONDITION) Dynasti::Logger::GetInstance().LogDebug(__FILE__, __LINE__, MESSAGE);
#define DYNASTI_FATAL(MESSAGE) {Dynasti::Logger::GetInstance().LogError(__FILE__, __LINE__, MESSAGE); exit(0);}
#define DYNASTI_WARNING(MESSAGE) Dynasti::Logger::GetInstance().LogWarning(__FILE__, __LINE__, MESSAGE);
#define DYNASTI_WARNING_IF(CONDITION, MESSAGE) if ((CONDITION)) Dynasti::Logger::GetInstance().LogWarning(__FILE__, __LINE__, MESSAGE);
#define DYNASTI_ASSERT(CONDITION, MESSAGE) {if (!(CONDITION)) Dynasti::Logger::GetInstance().LogError(__FILE__, __LINE__, MESSAGE); assert(CONDITION);}
#else
#define DYNASTI_LOG_INIT(LOGTAG, LOGMODE, TERMINALMODE)
#define DYNASTI_LOG(MESSAGE)
#define DYNASTI_LOG_IF(CONDITION, MESSAGE)
#define DYNASTI_LOG_VERBOSE(MESSAGE);
#define DYNASTI_LOG_VERBOSE_IF(CONDITION, MESSAGE);
#define DYNASTI_LOG_DEBUG(MESSAGE);
#define DYNASTI_LOG_DEBUG_IF(CONDITION, MESSAGE);
#define DYNASTI_FATAL(MESSAGE) exit(0);
#define DYNASTI_WARNING(MESSAGE)
#define DYNASTI_WARNING_IF(CONDITION, MESSAGE)
#define DYNASTI_ASSERT(CONDITION, MESSAGE) assert(CONDITION);
#endif

#if defined(DYNASTI_USE_TIMER)
#define DYNASTI_TIMER_INIT(TIMERTAG) Dynasti::Timer::GetInstance(TIMERTAG);
#define DYNASTI_TIMER(NAME) Dynasti::TimerBlock NAME ## TimerBlock(#NAME);
#else
#define DYNASTI_TIMER_INIT(FILENAME)
#define DYNASTI_TIMER(NAME)
#endif


#endif
