#ifndef DYNASTI_TIMER_H
#define DYNASTI_TIMER_H


#include <cstdlib>
#include <string>
#include <iostream>
#include <map>
#include <mutex>
#include <thread>
#include "Dynasti/Core/Constants.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Class representing a single timing block with functions for starting and stopping the timer clock.
    /// \author  D. A. Hubber
    /// \date    06/10/2022
    //=================================================================================================================
    class TimerBlock
    {
    public:

        /// Constructor to create timing block and register with main Timer singleton
        /// \param[in] _name - Unique human readable name of this timing block
        TimerBlock(const std::string& name);

        /// Destructor of timing block which unregisters with the main Timer singleton.
        ~TimerBlock();

        /// \return - Total accumulated time since creation of timing block instance
        double GetAccumulatedTime() const;
        
        /// Getters for private variables values
        inline const std::string GetName() const { return name_; }
        inline const std::thread::id GetThreadId() const { return threadId_; }


    private:

        const std::thread::id threadId_;                             ///< Id of thread that created timing block
        const std::string name_;                                     ///< Human-readable name of timing block
        std::chrono::high_resolution_clock::time_point startTime_;   ///< Start time when timing block created

    };



    //=================================================================================================================
    /// \brief   Singleton class for managing the internal timing routines.
    /// \author  D. A. Hubber
    /// \date    06/10/2022
    //=================================================================================================================
    class Timer
    {
    public:

        /// Delete the copy constructor and operator to prevent additional instances from being created
        Timer(Timer const &)           = delete;
        void operator= (Timer const &) = delete;

        /// Registers a new TimerBlock and records its time.  If the TimerBlock has been previously registered,
        /// the newly timed segment is added to the previously recorded time.
        /// \param[in] timerBlock - TimerBlock instance to be added to the records
        void RecordTimerBlock(const TimerBlock &timerBlock);

        /// Writes all recorded timing blocks to file with a given filename.
        /// \param[in] filename - Filename to be written to with timing statistics
        void WriteTimingStatistics(const std::string filename);

        /// Static function for creating the singleton instance and returning it
        /// \return - The singleton instance of the Timer class
        static inline Timer& GetInstance(const std::string& timerTag="")
        {
            static Timer instance(timerTag);
            return instance;
        }

        /// \return - The filename containing the outputted statistics
        inline const std::string& GetFilename() const {return filename_;}
        inline std::size_t GetNumTimerBlocks() const {return totalTimes_.size();}


    private:

        std::mutex timerMutex_;                        ///< Mutex for main timer object
        std::string filename_;                         ///< Name of file for outputing statistics
        std::map <std::string, double> totalTimes_;    ///< Map of (total) wall clock times
        TimerBlock globalTimerBlock_;                  ///< Timer block for measuring total running time of code

        /// Singleton constructor which starts the global code timer
        Timer(const std::string& timerTag);

        /// Singleton destructor which stops timing and writes all timing statistics before being destroyed
        ~Timer();

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
