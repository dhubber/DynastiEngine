#ifndef DYNASTI_FRAME_TIMER_H
#define DYNASTI_FRAME_TIMER_H


#include <chrono>
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Class used to calculate the time between frames (used for system updates) and the current fps.
    /// \author  D. A. Hubber
    /// \date    18/11/2020
    //=================================================================================================================
    class FrameTimer
    {
    public:

        FrameTimer(const double fpsUpdateInterval = 1.0);
        
        /// Updates all values for the current frame, including the current time, frame timestep and the fps
        void Update(const std::chrono::high_resolution_clock::time_point& newTimePoint);

        // Getter functions
        inline const double GetFrameInterval() const { return deltaTime_.count(); }
        inline const int GetNumFrames() const { return numFrames_; }
        inline const double GetTime() const { return totalTime_.count(); }
        inline const double GetFps() const { return fps_; }


    private:
        
        int numFrames_;                                                    ///< Total no. of frames
        int numFramesLastFps_;                                             ///< No. of frames of last fps calculation
        double fps_;                                                       ///< Frames per second
        double fpsUpdateInterval_;                                         ///< Interval (in sec) between computing fps
        std::chrono::duration<double> deltaTime_;                          ///< Time interval of last frame
        std::chrono::duration<double> totalTime_;                          ///< Accumulated running wall-clock time
        std::chrono::high_resolution_clock::time_point startTimePoint_;    ///< Initial time point
        std::chrono::high_resolution_clock::time_point currentTimePoint_;  ///< Current time point
        std::chrono::high_resolution_clock::time_point lastTimePoint_;     ///< Previous step time point
        std::chrono::high_resolution_clock::time_point lastFpsTimePoint_;  ///< Time point of last fps calculation

    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
