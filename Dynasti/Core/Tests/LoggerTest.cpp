#include <filesystem>
#include <iostream>
#include "doctest/doctest.h"
#include "Dynasti/Core/Logger.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    /// Test an individual log file object in isolation.
    TEST_CASE("LogFile")
    {
        // Clean-up of temporary files from previous running of this test
        const std::string testLogFileName =  "testLogFile.log";
        std::filesystem::remove(testLogFileName);
        REQUIRE_EQ(std::filesystem::exists(testLogFileName), false);

        // Behaviour 1: Opening a file with the given absolute path should create that file
        LogFile testLogFile(testLogFileName);
        REQUIRE_EQ(testLogFile.GetFileName(), testLogFileName);
        REQUIRE_EQ(std::filesystem::exists(testLogFileName), true);

        // Behaviour 2: Duplicate file names are forbidden
        // TODO: Not yet implemented

        // Behaviour 3: Writing a message to the log displays both the filename and line number
        testLogFile.WriteLog(__FILE__, __LINE__, "A simple test message");
    }


    /// Verify the logger singleton behaves correctly.
    TEST_CASE("Logger")
    {
        Logger &logger = Logger::GetInstance();
        REQUIRE_EQ(logger.GetLogMode(), LogMode::Verbose);
        REQUIRE_EQ(logger.GetTerminalMode(), LogMode::Tacit);

        // Verify logger is a singleton by trying (and failing) to create a second separate instance
        Logger &logger2 = Logger::GetInstance();
        REQUIRE_EQ(&logger, &logger2);
    }

}
//---------------------------------------------------------------------------------------------------------------------
