#include <chrono>
#include <iostream>
#include <thread>
#include "doctest/doctest.h"
#include "Dynasti/Core/Timer.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    /// Verify the Timer singleton behaves correctly.
    TEST_CASE("Timer")
    {
        // Verify default values when creating singleton
        Timer &timer = Timer::GetInstance("DynastiTests");
        REQUIRE_EQ(timer.GetFilename(), "DynastiTests.timing");
        REQUIRE_EQ(timer.GetNumTimerBlocks(), 0);

        // Verify that the singleton is indeed a singleton
        Timer &timer2 = Timer::GetInstance();
        REQUIRE_EQ(&timer, &timer2);
    }


    /// Test the basic functionality of an individual TimerBlock object
    TEST_CASE("TimerBlock")
    {
        Timer &timer = Timer::GetInstance();
        {
            // Verify that the name of the output file persists from the previous test
            REQUIRE_EQ(timer.GetFilename(), "DynastiTests.timing");

            TimerBlock timerBlock("testblock");
            REQUIRE_EQ(timerBlock.GetName(), "testblock");
            REQUIRE_EQ(timerBlock.GetThreadId(), std::this_thread::get_id());

            // Sleep duration time should be at least that specified
            std::this_thread::sleep_for(std::chrono::milliseconds (1));
            REQUIRE_GE(timerBlock.GetAccumulatedTime(), 0.001);
            
            // Verify that timer block is not yet registered by singleton
            REQUIRE_EQ(timer.GetNumTimerBlocks(), 0);
        }
    
        // Verify that timer block is registered with timer singleton correctly after destruction
        REQUIRE_EQ(timer.GetNumTimerBlocks(), 1);
    }

}
//---------------------------------------------------------------------------------------------------------------------
