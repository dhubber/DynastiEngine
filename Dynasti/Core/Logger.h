#ifndef DYNASTI_LOGGER_H
#define DYNASTI_LOGGER_H


#include <assert.h>
#include <cstdarg>
#include <cstdlib>
#include <string>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <mutex>
#include "Dynasti/Core/Constants.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    /// Enumeration of all possible logging modes
    enum class LogMode
    {
        Tacit,
        Default,
        Verbose,
        Debug
    };


    //=================================================================================================================
    /// \brief   Default class wrapping all functions for opening, closing and writing to log-files.
    /// \details Default log-file class, with the following expected behaviour:
    ///          - Opening a file with the given absolute path should create that file
    ///          - Duplicate file names are forbidden
    ///          - Writing a message to the log displays both the filename and line number
    ///          - A message is only written if the message priority level equals of exceeds the user-defined level
    /// \author  D. A. Hubber
    /// \date    04/10/2022
    //=================================================================================================================
    class LogFile
    {
    public:

        // TODO: Verify that a file with the same name/absolute path does not yet exist.
        LogFile(const std::string& fileName)
                : fileName_(fileName) {
            fileStream_.open(fileName);
        }

        ~LogFile()
        {
            fileStream_.close();
        };

        /// Write a single line to the log stream object, including the source code file name and line number
        /// \param[in] file - Char string of the header/source file (including the relative path) invoking the log
        /// \param[in] line - Line no. in header/source file invoking the log
        /// \param[in] message - Message to write in the log
        inline void WriteLog(const char *file, const int line, std::string_view message)
        {
            std::unique_lock<std::mutex> lock(logMutex_);
            static const std::uint32_t pathWidth = 50;
            const auto enginePathString = enginePath.string();
            const auto pathLength = enginePathString.length();
            const auto shortFileString = std::string(file).substr(pathLength + 1);
            std::stringstream locationString, lineStream, messageString;
            locationString << "[" << shortFileString << "." << line << "]  ";
            lineStream << std::setw(pathWidth) << std::left << locationString.str() << message;
            const auto &lineString = lineStream.str();
            fileStream_ << lineString << std::endl;
        }

        /// Getters for returning values of private variables
        inline const std::string GetFileName() const {return fileName_.string();}


    private:

        std::mutex logMutex_;                              ///< Mutex for writing to the log
        std::filesystem::path fileName_;                   ///< Human-readable filename of log-file
        std::ofstream fileStream_;                         ///< Stream-object for writing to file

    };


    //=================================================================================================================
    /// \brief   Singleton class for writing messages to logs from any point in the code.
    /// \author  D. A. Hubber
    /// \date    05/10/2022
    //=================================================================================================================
    class Logger
    {
    public:

        /// Delete the copy constructor and operator to prevent additional instances from being created
        Logger(Logger const &)         = delete;
        void operator=(Logger const &) = delete;

        /// Write a message to the terminal and/or log, if either is set to at least LOG_BASIC logging mode
        /// \param[in] file - Char string of the header/source file (including the relative path) invoking the log
        /// \param[in] line - Line no. in header/source file invoking the log
        /// \param[in] message - Message to write in the log
        inline void Log(const char *file, const int line, std::string message)
        {
            if (terminalMode_ >= LogMode::Default) std::cout << message << std::endl;
            if (logMode_ >= LogMode::Default) mainLog_.WriteLog(file, line, message);
        }

        /// Write a message to the terminal and/or log when a fatal error has been encountered.
        /// \param[in] file - Char string of the header/source file (including the relative path) invoking the log
        /// \param[in] line - Line no. in header/source file invoking the log
        /// \param[in] message - Message to write in the log
        inline void LogError(const char *file, const int line, std::string message)
        {
            std::unique_lock<std::mutex> lock(loggerMutex_);
            std::string errorMessage = "Error: " + message;
            Log(file, line, errorMessage);
        }

        /// Write a message to the terminal and/or log, if either is set to at least LOG_VERBOSE logging mode
        /// \param[in] file - Char string of the header/source file (including the relative path) invoking the log
        /// \param[in] line - Line no. in header/source file invoking the log
        /// \param[in] message - Message to write in the log
        inline void LogVerbose(const char *file, const int line, std::string message)
        {
            if (terminalMode_ >= LogMode::Verbose) std::cout << message << std::endl;
            if (logMode_ >= LogMode::Verbose) mainLog_.WriteLog(file, line, message);
        }

        /// Write a message to the terminal and/or log, if either is set to at least LOG_DEBUG logging mode
        /// \param[in] file - Char string of the header/source file (including the relative path) invoking the log
        /// \param[in] line - Line no. in header/source file invoking the log
        /// \param[in] message - Message to write in the log
        inline void LogDebug(const char *file, const int line, std::string message)
        {
            if (terminalMode_ >= LogMode::Debug) std::cout << message << std::endl;
            if (logMode_ >= LogMode::Debug) mainLog_.WriteLog(file, line, message);
        }

        /// Write a warning message to the terminal and/or log
        /// \param[in] file - Char string of the header/source file (including the relative path) invoking the log
        /// \param[in] line - Line no. in header/source file invoking the log
        /// \param[in] message - Message to write in the log
        inline void LogWarning(const char *file, const int line, std::string message)
        {
            std::string warningMessage = "Warning: " + message;
            Log(file, line, warningMessage);
        }

        /// Static function for creating the singleton instance and returning it
        /// \return - The singleton instance of the Logger class
        static inline Logger& GetInstance(const std::string& logTag="", LogMode logMode=LogMode::Verbose,
                                          LogMode terminalMode=LogMode::Tacit)
        {
            static Logger instance(logTag, logMode, terminalMode);
            return instance;
        }

        /// Getters for private members
        inline const std::string GetMainLogFilename() const {return mainLog_.GetFileName();}
        inline LogMode GetLogMode() const {return logMode_;}
        inline const std::string& GetLogTag() const {return logTag_;}
        inline LogMode GetTerminalMode() const {return terminalMode_;}


    private:

        std::mutex loggerMutex_;                     ///< Mutex for main logger object
        std::string logTag_;                         ///< String tag for logfiles
        LogFile mainLog_;                            ///< Main (and currently only) log object
        LogMode logMode_;                            ///< Verbosity mode for main log file
        LogMode terminalMode_;                       ///< Verbosity mode for terminal output

        /// Singleton constructor for the global logger object.  Initialises all user-changeable variables
        Logger(const std::string& logTag, LogMode logMode, LogMode terminalMode)
            : logTag_(logTag)
            , mainLog_(logTag_ + ".log")
            , logMode_(LogMode::Verbose)
            , terminalMode_(LogMode::Tacit)
        {
            mainLog_.WriteLog(__FILE__, __LINE__, "Initialised Logger Singleton");
        }

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
