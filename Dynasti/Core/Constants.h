#ifndef DYNASTI_CONSTANTS_H
#define DYNASTI_CONSTANTS_H

#include <algorithm>
#include <cstdint>
#include <filesystem>
#include <string>
#include <SDL_keycode.h>

#define QUOTE(name) #name
#define STR(macro) QUOTE(macro)
#define DYNASTI_PATH_STRING STR(DYNASTI_PATH)

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    // Type aliases
    using EventType = std::int32_t;
    using Uuid = std::int64_t;
    using KeyCode = SDL_KeyCode;

    // Constants
    static const std::filesystem::path enginePath = std::string(DYNASTI_PATH_STRING);
    static constexpr float pi = M_PI;
    static constexpr float twoPi = 2.0f * pi;

}
//---------------------------------------------------------------------------------------------------------------------
#endif
