#ifndef DYNASTI_RANDOM_H
#define DYNASTI_RANDOM_H

#include <random>
#include <string>
#include "Dynasti/Core/Debug.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Conveniece class for generating random numbers from C++ algorithms
    /// \author  D. A. Hubber
    /// \date    24/08/2023
    //=================================================================================================================
    class RandomFloat
    {
    public:

        RandomFloat(float minValue=0.0f, float maxValue=1.0f)
            : randomDevice{}
            , twisterEngine(randomDevice())
            , floatDist(minValue, maxValue)
        {
            DYNASTI_WARNING_IF(randomDevice.entropy() == 0, "Zero entropy detected in random device");
        }

        float Generate(){ return floatDist(twisterEngine); }

    protected:

        std::random_device randomDevice;
        std::mt19937_64 twisterEngine;
        std::uniform_real_distribution<float> floatDist;

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
