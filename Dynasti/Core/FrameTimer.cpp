#include "Dynasti/Core/FrameTimer.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    FrameTimer::FrameTimer(const double fpsUpdateInterval)
        : numFrames_(0)
        , numFramesLastFps_(0)
        , fps_(0.0)
        , fpsUpdateInterval_(fpsUpdateInterval)
    {
        DYNASTI_ASSERT(std::isnormal(fpsUpdateInterval_), "Invalid fps update interval : " + std::to_string(fpsUpdateInterval_));
        startTimePoint_ = std::chrono::high_resolution_clock::now();
        currentTimePoint_ = startTimePoint_;
        lastFpsTimePoint_ = startTimePoint_;
    };


    void FrameTimer::Update(const std::chrono::high_resolution_clock::time_point& newTimePoint)
    {
        ++numFrames_;
        lastTimePoint_ = currentTimePoint_;
        currentTimePoint_ = newTimePoint;
        deltaTime_ = std::chrono::duration_cast<std::chrono::duration<double>>(currentTimePoint_ - lastTimePoint_);
        totalTime_ = std::chrono::duration_cast<std::chrono::duration<double>>(currentTimePoint_ - startTimePoint_);

        // If needed, recompute the fps
        const double deltaTimeFps = std::chrono::duration_cast<std::chrono::duration<double>>(currentTimePoint_ - lastFpsTimePoint_).count();
        if (deltaTimeFps / fpsUpdateInterval_ >= 1.0)
        {
            fps_ = static_cast<float>(numFrames_ - numFramesLastFps_) / deltaTimeFps;
            numFramesLastFps_ = numFrames_;
            lastFpsTimePoint_ = currentTimePoint_;
            DYNASTI_LOG("fps : " + std::to_string(fps_));
        }
    }

}
//---------------------------------------------------------------------------------------------------------------------