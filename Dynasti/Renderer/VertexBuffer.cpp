#include "Dynasti/Renderer/VertexBuffer.h"

#include "Dynasti/Renderer/Mesh.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    VertexBuffer::VertexBuffer(Mesh* mesh)
    {
        CopyDataToGpu(mesh->GetNumVertices(), mesh->GetNumIndices(),
                      mesh->VertexArrayPointer(), mesh->IndexArrayPointer());
    }


    VertexBuffer::~VertexBuffer()
    {
        DeleteDataFromGpu();
    }


    bool VertexBuffer::CopyDataToGpu(std::int32_t numVertices, std::int32_t numIndices,
                                     GLfloat* vertices, unsigned int *indices)
    {
        DYNASTI_LOG("Adding buffer data to GPU : " + std::to_string(numVertices) + "  " + std::to_string(numIndices));
        //DYNASTI_ASSERT(format.GetSize() > 0, "Cannot add zero-size buffer to GPU");
        DYNASTI_ASSERT(numVertices > 0, "Cannot add zero-vertex buffer to GPU");
        DYNASTI_ASSERT(numIndices > 0, "Cannot add zero-index buffer to GPU");

        const std::int32_t vertexSize = 2;
        const std::int32_t vertexSizeBytes = vertexSize * sizeof(GLfloat);

        // Generate vertex-array object
        glGenVertexArrays(1, &vao_);
        glBindVertexArray(vao_);
    
        // Create VBO for main vertex data
        glGenBuffers(1, &vboVertices_);
        glBindBuffer(GL_ARRAY_BUFFER, vboVertices_);
        glBufferData(GL_ARRAY_BUFFER, vertexSizeBytes * numVertices, vertices, GL_STATIC_DRAW);

        // Positions
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, vertexSizeBytes, (const GLvoid*) 0);
        glEnableVertexAttribArray(0);

        // Create buffer for indices
        numIndices_ = numIndices;
        glGenBuffers(1, &vboIndices_);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndices_);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(GLuint), (GLuint *) indices, GL_STATIC_DRAW);

        glBindVertexArray(0);

        return true;
    }
    
    
    void VertexBuffer::DeleteDataFromGpu()
    {
        glDeleteBuffers(1, &vboIndices_);
        glDeleteBuffers(1, &vboVertices_);
        glDeleteVertexArrays(1, &vao_);
    }

}
//---------------------------------------------------------------------------------------------------------------------