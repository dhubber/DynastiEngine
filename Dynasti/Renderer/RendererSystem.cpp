#include <random>
#include "glad/glad.h"
#include "Dynasti/Asset/AssetManager.h"
#include "Dynasti/Renderer/Mesh.h"
#include "Dynasti/Renderer/RenderableComponent.h"
#include "Dynasti/Renderer/RendererSystem.h"
#include "Dynasti/Reflection/ClassRegistry.h"
#include "Dynasti/Scene/Scene.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    void RendererSystem::RegisterClasses()
    {
        classRegistry_->RegisterClass<Camera>();
        classRegistry_->RegisterClass<RenderableComponent>();
    }


    void RendererSystem::Setup()
    {
        changeColorEventType_ = eventListener_->SubscribeToEvent("ChangeColorActionKeyDown", std::bind(&RendererSystem::ChangeColor, this, std::placeholders::_1));
        updateModelMatrixEventType_ = eventListener_->SubscribeToEvent("UpdateModelMatrix", std::bind(&RendererSystem::OnUpdateModelMatrix, this, std::placeholders::_1));

        shader_.Create();

        camera_ = std::make_shared<Camera>();
        camera_->SetOrthographicCamera({0.0f, 0.0f}, {1.0f, 1.0f}, 0.0f);

        RegisterDefaultAssets();
    }


    void RendererSystem::Shutdown()
    {

    }


    void RendererSystem::Update()
    {
        DYNASTI_TIMER(RendererSystem)

        glClearColor(bgColor_.r, bgColor_.g, bgColor_.b, bgColor_.a);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);
        glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
        //sglPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

        if (Scene::mainScene == nullptr)
            return;

        //const auto& renderables = Scene::mainScene->GetRenderablePool();
        const auto& renderables = Scene::mainScene->FindComponentPool<RenderableComponent>();
        if (renderables == nullptr)
            return;

        shader_.Use();
        shader_.SetGlobalUniforms(camera_);

        for (int i = 0; i < renderables->NumObjects(); ++i) {
            auto& rComp = (*renderables)[i];
            auto vertexBuffer = rComp.GetVertexBuffer();  //quadVertexBuffer_;
            shader_.SetLocalUniforms(rComp);
            glBindVertexArray(vertexBuffer->GetVertexArrayObject());
            glDrawElements(vertexBuffer->GetPrimitiveType(), vertexBuffer->GetNumIndices(), vertexBuffer->GetIndexType(), 0);
        }

        shader_.UnUse();
    }


    void RendererSystem::RegisterDefaultAssets()
    {
        auto& meshAssets = assetManager_->GetMeshRegistry();
        meshAssets.RegisterNewAsset(Mesh::TriangleMesh());
        meshAssets.RegisterNewAsset(Mesh::QuadMesh());
        meshAssets.RegisterNewAsset(Mesh::CircularMesh(0.5f));
        meshAssets.RegisterNewAsset(Mesh::AnnulusMesh(0.5f, 0.25f));
    }


    void RendererSystem::ChangeColor(Event event)
    {
        std::random_device randomDevice;
        std::mt19937_64 twisterEngine(randomDevice());
        std::uniform_real_distribution<float> floatDist(0.0, 1.0f);
        bgColor_ = glm::vec4(floatDist(twisterEngine), floatDist(twisterEngine), floatDist(twisterEngine), 1.0f);
        DYNASTI_LOG("New Color: " + std::to_string(bgColor_.r) + "  " + std::to_string(bgColor_.g) + "  " +
                    std::to_string(bgColor_.b));
    }


    void RendererSystem::OnUpdateModelMatrix(const Event event)
    {
        if (Scene::mainScene == nullptr) return;
        auto entityId = event.GetId();
        auto modelMatrix = event.GetData<glm::mat4>();
        auto renderables = Scene::mainScene->FindComponentPool<RenderableComponent>();
        if (renderables) {
            auto rComp = renderables->FindObjectWithUuid(entityId);
            if (rComp != nullptr) rComp->SetModelMatrix(modelMatrix);
        }

    }

}
//---------------------------------------------------------------------------------------------------------------------