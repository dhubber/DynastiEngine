#include "Dynasti/Renderer/Mesh.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    void Mesh::AddIndex(const unsigned int index)
    {
        DYNASTI_ASSERT(index >= 0, "Invalid index : " + std::to_string(index));
        indices_.push_back(index);
    }


    void Mesh::AddIndices(std::initializer_list<unsigned int> newIndices)
    {
        for (const int index : newIndices)
        {
            AddIndex(index);
        }
    }


    void Mesh::AddVertex(const Vec2& pos)
    {
        vertices_.push_back(pos.x);
        vertices_.push_back(pos.y);
    }


    void Mesh::AddVertices(std::initializer_list<Vec2> newVertices)
    {
        for (const Vec2 pos : newVertices)
        {
            AddVertex(pos);
        }
    }


    std::shared_ptr<Mesh> Mesh::TriangleMesh(Vec2 size)
    {
        DYNASTI_LOG_VERBOSE("Generating triangular mesh");
        auto mesh = std::make_shared<Mesh>("Triangle");
        const float halfWidth = 0.5f * size.x;
        const float halfHeight = 0.5f * size.y;

        mesh->AddVertices(
                {{-halfWidth, -halfHeight},
                 {halfWidth, halfHeight},
                 {0.0f, halfHeight}
                });
        mesh->AddIndices({0, 1, 2});
        mesh->CreateVertexBuffer();
        return mesh;
    }


    std::shared_ptr<Mesh> Mesh::QuadMesh(Vec2 size)
    {
        DYNASTI_LOG_VERBOSE("Generating quad mesh");
        auto mesh = std::make_shared<Mesh>("Quad");
        const float halfWidth = 0.5f * size.x;
        const float halfHeight = 0.5f * size.y;

        mesh->AddVertices(
                {{-halfWidth, -halfHeight},
                 {halfWidth, -halfHeight},
                 {halfWidth, halfHeight},
                 {-halfWidth, halfHeight}
                });
        mesh->AddIndices({0, 1, 3, 3, 1, 2});
        mesh->CreateVertexBuffer();
        return mesh;
    }

    std::shared_ptr<Mesh> Mesh::EllipticalMesh(Vec2 axes)
    {
        const std::int32_t numDivisions = 64;
        DYNASTI_LOG_VERBOSE("Generating ellipse mesh; numDivision : " + std::to_string(numDivisions));
        auto mesh = std::make_shared<Mesh>("Ellipse");
        float dTheta = twoPi/ static_cast<float>(numDivisions);

        mesh->AddVertex({0.0f, 0.0f});
        for (std::uint32_t i = 0; i < numDivisions; ++i)
        {
            const float theta = dTheta * static_cast<float>(i);
            mesh->AddVertex({axes.x*cos(theta),axes.y*sin(theta)});
        }

        for (std::uint32_t i = 0; i < numDivisions; ++i)
        {
            mesh->AddIndices({0, (i + 1)%numDivisions + 1, i%numDivisions + 1});
        }
        mesh->CreateVertexBuffer();
        return mesh;
    }


    std::shared_ptr<Mesh> Mesh::CircularMesh(float radius)
    {
        return EllipticalMesh({radius, radius});
    }


    std::shared_ptr<Mesh> Mesh::AnnulusMesh(float outerRadius, float innerRadius)
    {
        const std::int32_t numDivisions = 64;
        DYNASTI_LOG_VERBOSE("Generating annulus mesh; numDivision : " + std::to_string(numDivisions));
        auto mesh = std::make_shared<Mesh>("Annulus");
        float dTheta = twoPi / static_cast<float>(numDivisions);

        // Create vertex buffer
        for (std::uint32_t i = 0; i < numDivisions; ++i)
        {
            const float theta = dTheta * static_cast<float>(i);
            mesh->AddVertex({innerRadius*cos(theta), innerRadius*sin(theta)});
            mesh->AddVertex({outerRadius*cos(theta), outerRadius*sin(theta)});
        }

        for (std::uint32_t i = 0; i < numDivisions; ++i)
        {
            mesh->AddIndices({2*(i%numDivisions), 2*((i + 1)%numDivisions), 2*(i%numDivisions) + 1,
                              2*(i%numDivisions) + 1, 2*((i + 1)%numDivisions), 2*((i + 1)%numDivisions) + 1});
        }

        mesh->CreateVertexBuffer();
        return mesh;
    }

}
//---------------------------------------------------------------------------------------------------------------------