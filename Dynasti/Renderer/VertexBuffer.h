#ifndef DYNASTI_VERTEX_BUFFER_H
#define DYNASTI_VERTEX_BUFFER_H


#include <initializer_list>
#include <vector>
#include "glad/glad.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Core/Constants.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    class Mesh;
    
    //=================================================================================================================
    /// \brief   Virtual base class for all GPU vertex buffer implementations.
    /// \author  D. A. Hubber
    /// \date    16/01/2021
    //=================================================================================================================
    class VertexBuffer
    {
    public:

        VertexBuffer() = default;
        VertexBuffer(Mesh* mesh);
        ~VertexBuffer();

        /// Allocates the required memory on the GPU and copies all index and vertex data to the GPU
        /// \param[in] format - Format of data in vertex buffer
        /// \param[in] numVertices - Number of vertices to be copied to GPU
        /// \param[in] numIndices - Number of indices to be copied to GPU
        /// \param[in] vertices - Pointer to vertex array on CPU
        /// \param[in] indices - Pointer to index array on CPU
        bool CopyDataToGpu(std::int32_t numVertices, std::int32_t numIndices,
                           float* vertices, unsigned int *indices);

        /// Deallocates memory allocated for vertex buffer on the GPU
        void DeleteDataFromGpu();

        // Getters
        inline GLuint GetVertexBufferObject() const { return vboVertices_; }
        inline GLuint GetIndexBufferObject() const { return vboIndices_; }
        inline GLenum GetIndexType() const { return indexType_; }
        inline std::int32_t GetNumIndices() const { return numIndices_; }
        inline GLenum GetPrimitiveType() const { return primType_; }
        inline GLuint GetVertexArrayObject() const { return vao_; }


    protected:

        std::int32_t numIndices_{0};                       ///< No. of indices in index buffer
        GLenum indexType_{GL_UNSIGNED_INT};                ///< OpenGL index type (e.g. GL_SHORT, GL_UNSIGNED_INT)
        GLenum primType_{GL_TRIANGLES};                    ///< OpenGL primitive type for rendering
        GLuint vao_{0};                                    ///< GPU vertex attribute object pointer
        GLuint vboVertices_{0};                            ///< GPU vertex buffer object pointer
        GLuint vboIndices_{0};                             ///< GPU index buffer object pointer
    
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
