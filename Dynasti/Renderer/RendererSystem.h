#ifndef DYNASTI_RENDERER_H
#define DYNASTI_RENDERER_H


#include "Dynasti/Application//System.h"
#include "Dynasti/Camera/Camera.h"
#include "Dynasti/Renderer/VertexBuffer.h"
#include "Dynasti/Shader/GlslShader.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   ...
    /// \author  D. A. Hubber
    /// \date    08/08/2022
    //=================================================================================================================
    class RendererSystem : public System
    {
    public:

        RendererSystem(const std::string& name,
                       std::shared_ptr<AssetManager> assetManager,
                       std::shared_ptr<ClassRegistry> classRegistry,
                       std::shared_ptr<EventManager> eventManager,
                       std::shared_ptr<FrameTimer> frameTimer)
            : System(name, assetManager, classRegistry, eventManager, frameTimer)
        {
        }

        // Required implementations of System functions
        void RegisterClasses() override;
        void Setup() override;
        void Shutdown() override;
        void Update() override;

        void ChangeColor(Event event);

        /// Callback function for updating the model matrix of a given entity for rendering
        /// \param[in] event - Event containing updated model matrix data
        void OnUpdateModelMatrix(const Event event);


    protected:

        glm::vec4 bgColor_{0.0f, 0.0f, 0.0f, 1.0f};

        EventType changeColorEventType_;
        EventType changePrimitiveVertexBufferEventType_;
        EventType updateModelMatrixEventType_;

        BasicColorShader shader_;
        std::shared_ptr<Camera> camera_;

        std::shared_ptr<VertexBuffer> triangleVertexBuffer_;
        std::shared_ptr<VertexBuffer> quadVertexBuffer_;
        std::shared_ptr<VertexBuffer> circleVertexBuffer_;
        std::shared_ptr<VertexBuffer> annulusVertexBuffer_;

        void RegisterDefaultAssets();

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif