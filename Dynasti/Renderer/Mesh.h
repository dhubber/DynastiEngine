#ifndef DYNASTI_MESH_H
#define DYNASTI_MESH_H


#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <memory>
#include <vector>
#include <unordered_map>
#include "glad/glad.h"
#include "Dynasti/Asset/Asset.h"
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Renderer/VertexBuffer.h"
#include "Dynasti/Vector/Vector.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Base class for all mesh assets.  Contains functions for index buffers.
    /// \author  D. A. Hubber
    /// \date    04/08/2020
    //=================================================================================================================
    class Mesh : public Asset
    {
    public:

        // Factory functions for generating primitive meshes
        static std::shared_ptr<Mesh> TriangleMesh(Vec2 size = {1.0f, 1.0f});
        static std::shared_ptr<Mesh> QuadMesh(Vec2 size = {1.0f, 1.0f});
        static std::shared_ptr<Mesh> EllipticalMesh(Vec2 axes = {1.0f, 1.0f});
        static std::shared_ptr<Mesh> CircularMesh(float radius = 1.0f);
        static std::shared_ptr<Mesh> AnnulusMesh(float outerRadius = 1.0f, float innerRadius = 0.5f);

        Mesh(const std::string& name)
            : Asset(name)
        {
        }

        std::type_index GetType() override { return std::type_index(typeid(Mesh)); }

        void CreateVertexBuffer() { vertexBuffer_ = std::make_shared<VertexBuffer>(this); }

        /// Adds a single vertex index to the index array.
        /// \param[in] index - Vertex index to be added to indices array
        void AddIndex(const unsigned int index);

        /// Adds a list of array indices to the index array
        /// \param[in] newIndices - List of vertex indices to be added to the array
        void AddIndices(std::initializer_list<unsigned int> newIndices);

        /// Adds a single vertex object to the vertex buffer
        /// \param[in] vertex - Vertex struct containing all data to be added
        void AddVertex(const Vec2& pos);

        void AddVertices(std::initializer_list<Vec2> newVertices);

        /// \param[in] i - Array index inthe index buffer
        /// \return - Vertex array index at the ith position in the index buffer
        inline GLuint GetIndex(const int i) const
        {
            DYNASTI_ASSERT(i < GetNumIndices(), "Invalid index : " + std::to_string(i));
            return indices_[i];
        }

        inline Vec2 GetVertex(const int i) const
        {
            DYNASTI_ASSERT(i < GetNumVertices(), "Invalid vertex : " + std::to_string(i));
            return Vec2{vertices_[2*i], vertices_[2*i * 1]};
        }

        inline unsigned int *IndexArrayPointer() { return indices_.data(); }
        inline float* VertexArrayPointer() { return vertices_.data(); }
        inline std::size_t GetNumIndices() const { return indices_.size(); }
        inline std::size_t GetNumVertices() const { return vertices_.size(); }

        inline std::shared_ptr<VertexBuffer> GetVertexBuffer() const { return vertexBuffer_; }


    protected:

        std::vector<unsigned int> indices_;                ///< Mesh triangle indices
        std::vector<float> vertices_;                      ///< Mesh vertices
        std::shared_ptr<VertexBuffer> vertexBuffer_;       ///< Vertex buffer generated for loading mesh to GPU

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
