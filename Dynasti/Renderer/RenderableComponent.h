#ifndef DYNASTI_RENDERABLE_COMPONENT_H
#define DYNASTI_RENDERABLE_COMPONENT_H


#define GLM_FORCE_RADIANS
#include <assert.h>
#include <map>
#include <memory>
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "glad/glad.h"
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Object/Object.h"
#include "Dynasti/Reflection/Property.h"
#include "Dynasti/Renderer/Mesh.h"
#include "Dynasti/Vector/Vector.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Component class containing all data for rendering a mesh to the viewport.
    /// \author  D. A. Hubber
    /// \date    13/02/2020
    //=================================================================================================================
    class RenderableComponent : public PoolableObject
    {
    public:

        RenderableComponent()
            : color_(defaultColor_)
        {
        }
        virtual ~RenderableComponent() = default;

        // Required by reflection system
        std::type_index GetType() override { return std::type_index(typeid(RenderableComponent)); }
        static std::string Name() { return "RenderableComponent"; }
        static std::vector<std::string> BaseClasses() { return {}; }
        static std::vector<PropertyPtr> Properties()
        {
            return std::vector<PropertyPtr>{
                std::make_shared<BasicProperty<RenderableComponent, Vec4>>("Color", &RenderableComponent::color_)
            };
        }

        // Implementations of PoolableObject functions
        void OnCreate() override {};
        void OnDestroy() override
        {
            color_ = defaultColor_;
            mesh_ = nullptr;
        }

        // Getters and setters
        std::shared_ptr<VertexBuffer> GetVertexBuffer() const
        {
            DYNASTI_ASSERT(mesh_ != nullptr, "Mesh pointer is invalid");
            return mesh_->GetVertexBuffer();
        }
        const Vec4& GetColor() const { return color_; }
        const glm::mat4& GetModelMatrix() const { return modelMatrix_; }
        void SetColor(const Vec4& color) { color_ = color; }
        void SetModelMatrix(const glm::mat4& modelMatrix) { modelMatrix_ = modelMatrix; }
        void SetMesh(std::shared_ptr<Mesh> mesh) { mesh_ = mesh; }

    protected:

        static constexpr Vec4 defaultColor_ = Vec4{1.0f, 1.0f, 1.0f, 1.0f};

        Vec4 color_{defaultColor_};                        ///< Fragment color of renderable
        glm::mat4 modelMatrix_;                            ///< Model matrix
        std::shared_ptr<Mesh> mesh_{nullptr};              ///< Mesh model

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
