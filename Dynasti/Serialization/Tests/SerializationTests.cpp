#include <iostream>
#include <string>
#include <vector>
#include "doctest/doctest.h"
#include "Dynasti/Vector/Vector.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti::Tests
{

    TEST_CASE("Serialize simple types")
    {
        SUBCASE("Vec2")
        {
            YAML::Emitter yamlEmitter;
            Vec2 v1{1.0f, 2.0f};
            yamlEmitter << v1;
            std::string dataString = yamlEmitter.c_str();
            YAML::Node yamlNode = YAML::Load(dataString);
            std::cout << "DATA VEC: " << std::endl << dataString << std::endl;

            Vec2 v2 = yamlNode.as<Vec2>();
            REQUIRE_EQ(v2.x, v1.x);
            REQUIRE_EQ(v2.y, v1.y);
        }

        SUBCASE("std::vector")
        {
            YAML::Emitter yamlEmitter;
            std::vector<Vec2> v1{{0.0f, 1.0f}, {2.0f, 3.0f}, {4.0f, 5.0f}};
            yamlEmitter << v1;
            std::string dataString = yamlEmitter.c_str();
            YAML::Node yamlNode = YAML::Load(dataString);
            std::cout << "DATA VECTOR: " << std::endl << dataString << std::endl;

            auto v2 = yamlNode.as<std::vector<Vec2>>();
            for (std::size_t i = 0; i < v2.size(); ++i)
            {
                REQUIRE_EQ(v2[i].x, v1[i].x);
                REQUIRE_EQ(v2[i].y, v1[i].y);
            }
        }
    }

}
//---------------------------------------------------------------------------------------------------------------------