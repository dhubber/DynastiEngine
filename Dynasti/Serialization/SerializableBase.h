#ifndef DYNASTI_SERIALIZABLE_BASE_H
#define DYNASTI_SERIALIZABLE_BASE_H


#include <typeindex>
#include <yaml-cpp/yaml.h>


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Abstract interface class for all classes that can be serialized to/from YAML
    /// \author  D. A. Hubber
    /// \date    04/07/2023
    //=================================================================================================================
    class SerializableBase
    {
    public:

        virtual ~SerializableBase() = default;

        /// Serialize all data contained within a concrete class using a YAML emitter
        /// \param[inout] yamlEmitter - Ref to YAML Emitter object for streaming serialized data
        virtual void Serialize(YAML::Emitter &yamlEmitter) = 0;

        /// Serialize all data contained within a concrete class using a YAML emitter
        /// \param[inout] yamlEmitter - Ref to YAML Emitter object for streaming serialized data
        virtual void Deserialize(YAML::Node &yamlNode) = 0;

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif