#ifndef DYNASTI_GLSL_SHADER_H
#define DYNASTI_GLSL_SHADER_H


#define GLM_FORCE_RADIANS
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <assert.h>
#include <map>
#include <string>
#include <string_view>
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Renderer/RenderableComponent.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    class Camera;

    //=================================================================================================================
    /// \brief   GLSL shader class for OpenGL and Vulkan rendering
    /// \author  D. A. Hubber
    /// \date    04/11/2022
    //=================================================================================================================
    class GlslShader
    {
    public:
        
        GlslShader();

        // Compiles and links the complete shader program
        virtual void Create() = 0;

        void CreateAndLinkShader(const std::string& vertexShaderCode, const std::string& fragmentShaderCode);
        
        /// Sets all OpenGL flags and states required for using this shader
        virtual void SetGlFlags() {};
    
        /// Sets all global uniforms for the shader, such as the camera matrices and light variables
        /// \param[in] camera - Current camera being used to render scene
        /// \param[in] scene - Scene being rendered
        virtual void SetGlobalUniforms(std::shared_ptr<Camera> camera) {};
        
        /// Sets local uniform values for specific renderable currently being drawn
        virtual void SetLocalUniforms(const RenderableComponent &renderable) {};

        void Use();
        void UnUse();

        /// Add a named attribute to the GLSL shader program
        /// \param[in] attribute - Name of new attribute
        inline void AddAttribute(const std::string& attribute)
        {
            glBindAttribLocation(program_, numAttributes_++, attribute.c_str());
        }

        GLuint GetAttributeLocation(const std::string& attributeName) { return glGetAttribLocation(program_, attributeName.c_str()); }
    
        inline void AddUniform(const std::string& uniform)
        {
            GLuint location = glGetUniformLocation(program_, uniform.c_str());
            uniformLocationList_[uniform] = location;
            DYNASTI_ASSERT(location != GL_INVALID_VALUE, "Invalid uniform location");
            DYNASTI_ASSERT(location != GL_INVALID_OPERATION, "Invalid operation");
        }
        
        /// \return - Human-readable name of GLSL shader
        //inline const std::string& GetName() const {return name_;}
    
        inline GLuint GetUniform(const std::string& uniform)
        {
            return uniformLocationList_[uniform];
        }
    
        inline void SetUniform(const std::string &uniform, const GLfloat x, const GLfloat y, const GLfloat z)
        {
            GLint location = GetUniform(uniform);
            glUniform3f(location, x, y, z);
        }
        
        inline void SetUniform(const std::string &uniform, const glm::vec3 &v)
        {
            this->SetUniform(uniform, v.x, v.y, v.z);
        }
        
        inline void SetUniform(const std::string &uniform, const glm::vec4 &v)
        {
            GLint location = GetUniform(uniform);
            glUniform4f(location, v.x, v.y, v.z, v.w);
        }
        
        inline void SetUniform(const std::string &uniform, const glm::vec2 &v)
        {
            GLint location = GetUniform(uniform);
            glUniform2f(location, v.x, v.y);
        }
        
        inline void SetUniform(const std::string &uniform, const glm::mat4 &m)
        {
            GLint location = GetUniform(uniform);
            glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(m));
        }
        
        inline void SetUniform(const std::string &uniform, const glm::mat3 &m)
        {
            GLint location = GetUniform(uniform);
            glUniformMatrix3fv(location, 1, GL_FALSE, glm::value_ptr(m));
        }
        
        inline void SetUniform(const std::string &uniform, const GLfloat val)
        {
            GLint location = GetUniform(uniform);
            glUniform1f(location, val);
        }

        inline void SetUniform(const std::string &uniform, const int val)
        {
            GLint location = GetUniform(uniform);
            glUniform1i(location, val);
        }
        
        inline void SetUniform(const std::string &uniform, const bool val)
        {
            GLint location = GetUniform(uniform);
            glUniform1i(location, val);
        }
        

    protected:
        
        int shaderId_;                                     ///< Internal (to OpenGL) shader id number
        int numAttributes_;                                ///< No. of attributes
        GLuint program_;                                   ///< Linked shader program id
        GLuint vertexShader_;                              ///< Vertex shader program id
        GLuint fragmentShader_;                            ///< Fragment shader program id
        std::map<std::string,GLuint> uniformLocationList_; ///< List of uniform locations

        /// Loads a shader program from a char string containing GLSL code
        /// \param shaderType - Enumerated type of shader
        /// \param shaderCode - Pointer to string containing shader code
        /// \return - True if shader code loaded and compiled successfully; otherwise returns false
        bool LoadFromString(GLenum shaderType, const std::string& shaderCode);
    
        /// Deletes the main shader program from the GPU
        void DeleteShaderProgram();
    };


    //=================================================================================================================
    /// \brief   Basic shader for rendering a mesh with a uniform color in the fragment shader.
    /// \author  D. A. Hubber
    /// \date    11/09/2014
    //=================================================================================================================
    class BasicColorShader : public GlslShader
    {
    public:

        void Create() override;

        virtual void SetGlFlags() override;
        virtual void SetGlobalUniforms(std::shared_ptr<Camera> camera) override;
        virtual void SetLocalUniforms(const RenderableComponent &renderable) override;

    };


}
//---------------------------------------------------------------------------------------------------------------------
#endif
