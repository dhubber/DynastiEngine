#include "Dynasti/Camera/Camera.h"
#include "Dynasti/Core/Debug.h"
//#include "Dynasti/Renderer/RenderableComponent.h"
#include "Dynasti/Shader/GlslShader.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    const char *vShaderCode = "#version 460 core\n"
                              "layout (location = 0) in vec2 aPos;\n"
                              "uniform mat4 view;\n"
                              "uniform mat4 projection;\n"
                              "uniform mat4 model;\n"
                              "void main()\n"
                              "{\n"
                              "   gl_Position = projection*view*model*vec4(aPos.x, aPos.y, 0.0, 1.0);\n"
                              "}\0";
    const char *fShaderCode = "#version 460 core\n"
                              "out vec4 FragColor;\n"
                              "uniform vec4 color;\n"
                              "void main()\n"
                              "{\n"
                              "   FragColor = color;\n"
                              "}\0";


    void BasicColorShader::Create()
    {
        CreateAndLinkShader(vShaderCode, fShaderCode);
        Use();
        AddUniform("color");
        AddUniform("view");
        AddUniform("projection");
        AddUniform("model");
        UnUse();
    }


    void BasicColorShader::SetGlFlags()
    {
        //SetUniform("color", glm::vec3(1.0f, 0.0f, 0.0f));
        //glEnable(GL_DEPTH_TEST);
        //glDisable(GL_BLEND);
        //glCullFace(GL_FRONT);
        //glCullFace(GL_BACK);
        //glPolygonMode( GL_FRONT_AND_BACK, GL_ );
    }


    void BasicColorShader::SetGlobalUniforms(std::shared_ptr<Camera> camera)
    {
        SetUniform("color", glm::vec3(1.0f, 0.0f, 0.0f));
        SetUniform("view", camera->GetViewMatrix());
        SetUniform("projection", camera->GetProjectionMatrix());
    }
    
    
    void BasicColorShader::SetLocalUniforms(const RenderableComponent &renderableComp)
    {
        const glm::mat4 &M = renderableComp.GetModelMatrix();
        const Vec4& color = renderableComp.GetColor();

        SetUniform("model", M);
        SetUniform("color", glm::vec4(color.x, color.y, color.z, color.z));
    }
    
}
//---------------------------------------------------------------------------------------------------------------------
