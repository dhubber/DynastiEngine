#include <istream>
#include <sstream>
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Shader/GlslShader.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    GlslShader::GlslShader()
        : shaderId_(-1)
        , numAttributes_(0)
        , program_(0)
        , vertexShader_(0)
        , fragmentShader_(0)
    {
        DYNASTI_LOG("Constructing GlslShader object");
    }


    void GlslShader::CreateAndLinkShader(const std::string& vertexShaderCode, const std::string& fragmentShaderCode)
    {
        LoadFromString(GL_VERTEX_SHADER, vertexShaderCode);
        LoadFromString(GL_FRAGMENT_SHADER, fragmentShaderCode);
        DYNASTI_ASSERT(vertexShader_ != 0 && fragmentShader_ != 0, "Invalid vertex or fragment shader");

        program_ = glCreateProgram();
        DYNASTI_ASSERT(program_ != 0, "Invalid GlslShader program id returned by OpenGL");

        // Attach all shader stages, link and check whether the program links fine
        glAttachShader(program_, vertexShader_);
        glAttachShader(program_, fragmentShader_);
        glLinkProgram(program_);

        GLint status;
        glGetProgramiv(program_, GL_LINK_STATUS, &status);
        if (status == GL_FALSE)
        {
            GLint infoLogLength;
            glGetProgramiv(program_, GL_INFO_LOG_LENGTH, &infoLogLength);
            GLchar *infoLog = new GLchar[infoLogLength];
            glGetProgramInfoLog(program_, infoLogLength, NULL, infoLog);
            std::string infoString = infoLog;
            DYNASTI_LOG("Problem linking GlslShader; Link log: " + infoString);
            delete[] infoLog;
            DYNASTI_FATAL("Terminating program after shader linkage error");
        }

        glDeleteShader(fragmentShader_);
        glDeleteShader(vertexShader_);

        DYNASTI_LOG("Successfully linked GlslShader");
    }


    bool GlslShader::LoadFromString(GLenum shaderType, const std::string& shaderCode)
    {
        DYNASTI_LOG_VERBOSE("Loading GLSL shader from string");

        // Check whether the shader loads and compiles fine
        auto shader = glCreateShader(shaderType);
        DYNASTI_ASSERT(shader != 0, "GlslShader id given invalid value by OpenGL");
        const GLchar* const shaderCodePtr = shaderCode.c_str();
        glShaderSource(shader, 1, &shaderCodePtr, NULL);
        glCompileShader(shader);

        // Write error message if there is a problem with shader compilation
        GLint status;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
        if (status == GL_FALSE)
        {
            GLint infoLogLength;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);
            GLchar *infoLog = new GLchar[infoLogLength];
            glGetShaderInfoLog(shader, infoLogLength, NULL, infoLog);
            std::string infoString = infoLog;
            DYNASTI_LOG("Problem compiling shader stage; Compile log: " + infoString);
            delete[] infoLog;
            return false;
        }

        // Set shader identifier depending on shader type
        if (shaderType == GL_VERTEX_SHADER)
        {
            DYNASTI_LOG_VERBOSE("Successfully created vertex shader from string");
            vertexShader_ = shader;
        }
        else if (shaderType == GL_FRAGMENT_SHADER)
        {
            DYNASTI_LOG_VERBOSE("Successfully created fragment shader from string");
            fragmentShader_ = shader;
        }
        else
        {
            DYNASTI_LOG("Invalid shader stage type selected");
            return false;
        }

        return true;
    }


    void GlslShader::DeleteShaderProgram()
    {
        glDeleteProgram(program_);
    }


    void GlslShader::Use()
    {
        glUseProgram(program_);
    }


    void GlslShader::UnUse()
    {
        glUseProgram(0);
    }

}
//---------------------------------------------------------------------------------------------------------------------
