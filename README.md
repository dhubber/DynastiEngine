# DynastiEngine

Dynasti is a simple cross-platform 2d game framework/engine written as an educational project but also with the goal of eventually creating simple games.  Dynasti 
- Is written in modern C++17 with extensive use of STL
- Uses Cmake for cross-platform compilation
- Is designed using an Entity-Component-System architecture to leverage better data-driven performance
- Uses events to communicate between sub-systems therefore minimising coupling

Implemented
- SDL2 window with OpenGL 4.6 context created
- Orthographic (2d) camera modes
- ECS implementation allowing easy creation of new component types via templates
- Event system, where events are processed immediately using the Observer pattern
- Keyboard input mapping system which trigger key up/down events, or can be polled
- Loggers and time profilers
- C++ script components
- YAML serialisation (e.g. saving scenes to disk)
- Simple 2d renderer (OpenGL 4.0 with simple shaders)
- Axis-aligned bounding box collision detection
- Example Breakout clone game 'Dong'

Currently under development
- Scene instancing with override properties

To-do/Wish-list (in no particular order)
- Texture mapping
- Camera components
- Framebuffers for implementing post-process effects (e.g. anti-aliasing)
- Basic Editor GUI (e.g. Dear IMGUI)
- Event queues with system mailboxes (e.g. the Actor model)
- Thread pool for running parallel/concurrent engine tasks
- Mouse and Gamepad input
- Batched rendering
- Multiple rendering viewports for split-screen gameplay
- Audio assets and components
- Font assets and text rendering
- Frustum culling
- OpenGL ES 2.0 or 3.0
- Android compilation using the NDK
- Touch screen input

## Dependencies
Dynasti requires the following dependencies, which may need to be installed beforehand or can be installed during the build process depending on your chosen platform
- A C++ compiler (e.g. Visual Studio C++ for Windows)
- OpenGL 4.6 drivers (e.g. NVIDIA)
- [git](https://git-scm.com/)
- [cmake](https://cmake.org/)

## Installation

### Windows (Visual Studio or CLion)
- Open powershell or git-bash and change into the directory you wish to install the code
```bash
mkdir /path/to/code
cd /path/to/code
```
- Download the code from gitlab using git (including simultaneously downloading all submodules)
```bash
git clone --recurse-submodules https://gitlab.com/dhubber/DynastiEngine.git
```
- Open the repository in Visual Studio 2022 or CLion
- Execute CMake
- Build the project to compile all libraries and executables
- Select and execute the 'Dong' target to run the example game

### Linux
(TBD)