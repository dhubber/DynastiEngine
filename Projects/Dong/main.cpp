#include "Dynasti/Application/Application.h"
#include "Dynasti/Asset/AssetManager.h"
#include "Dynasti/Collision/Shape2dComponent.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Core/FrameTimer.h"
#include "Dynasti/Core/Random.h"
#include "Dynasti/Event/EventManager.h"
#include "Dynasti/Reflection/ClassRegistry.h"
#include "Dynasti/Scene/Scene.h"
#include "Dynasti/Scene/ScriptComponent.h"
#include "Dynasti/Renderer/RenderableComponent.h"
#include "Dynasti/Transform/Transform2dComponent.h"


namespace Dynasti::Dong
{
    static constexpr Vec4 brickInitColor{0.2f, 0.2f, 0.7f, 1.0f};
    static constexpr Vec4 brickHitColor{0.5f, 0.5f, 0.9f, 1.0f};

    static constexpr float batSize = 0.1f;
    static constexpr float wallSize = 0.05f;
    static constexpr float ballSize = 0.025f;
    static constexpr float brickSize = 0.0615f;
    static constexpr float brickSpacing = 0.065f;

    static constexpr int numRows = 6;
    static constexpr int numColumns = 12;

    static EventType leftActionKeyDownEventType_;
    static EventType leftActionKeyUpEventType_;
    static EventType rightActionKeyDownEventType_;
    static EventType rightActionKeyUpEventType_;
    static EventType fireActionKeyDownEventType_;
    static EventType updateWorldPositionEventType_;

    static Uuid batUuid;
    static Uuid ballUuid;


    class BatScript : public ScriptComponentBase
    {
    public:

        // Required by reflection system
        std::type_index GetType() override { return std::type_index(typeid(BatScript)); }
        static std::string Name() { return "BatScript"; }
        static std::vector<std::string> BaseClasses() { return {}; }
        static std::vector<Dynasti::PropertyPtr> Properties()
        {
            return std::vector<Dynasti::PropertyPtr>{};
        }
        void OnCreate() override {};
        void OnDestroy() override {};

        void OnEvent(const Dynasti::Event event, std::shared_ptr<Scene> scene) override
        {
            auto eventType = event.GetType();
            if (eventType == leftActionKeyDownEventType_)
            {
                left = true;
            }
            else if (eventType == leftActionKeyUpEventType_)
            {
                left = false;
            }
            else if (eventType == rightActionKeyDownEventType_)
            {
                right = true;
            }
            else if (eventType == rightActionKeyUpEventType_)
            {
                right = false;
            }
            else if (eventType == fireActionKeyDownEventType_ && ballAttached)
            {
                Fire();
                ballAttached = false;
            }
        }

        void OnOverlapBeginEvent(const Event event, std::shared_ptr<Scene> scene) override
        {
        }

        void OnOverlapEndEvent(const Dynasti::Event event, std::shared_ptr<Dynasti::Scene> scene) override
        {
        }

        void OnUpdate(std::shared_ptr<Scene> scene, std::shared_ptr<FrameTimer> frameTimer) override
        {
            float deltaTime = 0.05f; // static_cast<float>(frameTimer->GetFrameInterval());
            if (left && !right)
            {
                pos.x = std::clamp(pos.x - (speed * deltaTime), range.x, range.y);
                auto transform2dComp = scene->FindComponent<Transform2dComponent>(uuid);
                transform2dComp->SetLocalPosition(pos);
                //Event moveBatEvent(updateWorldPositionEventType_, uuid, pos);
                //scene->BroadcastEvent(moveBatEvent);
            }
            else if (!left && right)
            {
                pos.x = std::clamp(pos.x + (speed * deltaTime), range.x, range.y);
                auto transform2dComp = scene->FindComponent<Transform2dComponent>(uuid);
                transform2dComp->SetLocalPosition(pos);
                //Event moveBatEvent(updateWorldPositionEventType_, uuid, pos);
                //scene->BroadcastEvent(moveBatEvent);
            }
        }

        void SetInitPos(Vec2 initPos) { pos = initPos; }


    protected:

        bool left = false;
        bool right = false;
        bool ballAttached = true;
        float speed = 0.3f;
        Vec2 pos;
        Vec2 range{-0.5f + 0.5f*batSize, 0.5f - 0.5f*batSize};

        void Fire()
        {
        }


    };


    class BallScript : public ScriptComponentBase
    {
    public:

        // Required by reflection system
        std::type_index GetType() override { return std::type_index(typeid(BallScript)); }
        static std::string Name() { return "BallScript"; }
        static std::vector<std::string> BaseClasses() { return {}; }
        static std::vector<Dynasti::PropertyPtr> Properties()
        {
            return std::vector<Dynasti::PropertyPtr>{};
        }
        void OnCreate() override {};
        void OnDestroy() override {};

        void OnEvent(const Dynasti::Event event, std::shared_ptr<Scene> scene) override
        {
            auto eventType = event.GetType();
            if (eventType == fireActionKeyDownEventType_)
            {
                moving = true;
                vel = Vec2{speed, speed};
            }

        }

        void OnOverlapBeginEvent(const Event event, std::shared_ptr<Scene> scene) override
        {
            if (event.GetData<Uuid>() == batUuid)
            {
                auto batTransform = Scene::mainScene->FindComponent<Transform2dComponent>(batUuid);
                Vec2 batPos = batTransform->GetPosition();
                float diff = pos.x - batPos.x;
                float angle = 1.5f * 0.5f * pi * diff / batSize;
                speed += accelRate;
                vel = Vec2{speed * sin(angle), speed * cos(angle)};
            }
            else
            {
                auto brickId = event.GetData<Uuid>();
                auto brickTransform = Scene::mainScene->FindComponent<Transform2dComponent>(brickId);
                auto brickPos = brickTransform->GetPosition();

                Vec2 diff{brickPos.x - pos.x, brickPos.y - pos.y};
                if (fabs(diff.x) > fabs(diff.y) && vel.x*diff.x > 0.0f)
                {
                    vel.x = -vel.x;
                }
                if (fabs(diff.y) > fabs(diff.x) && vel.y*diff.y > 0.0f)
                {
                    vel.y = -vel.y;
                }
            }
        }


        void OnOverlapEndEvent(const Dynasti::Event event, std::shared_ptr<Dynasti::Scene> scene) override
        {
        }

        void OnUpdate(std::shared_ptr<Scene> scene, std::shared_ptr<FrameTimer> frameTimer) override
        {
            float deltaTime = 0.05f;
            if (moving)
            {
                Vec2 newPos = Vec2{pos.x + (vel.x * deltaTime), pos.y + (vel.y * deltaTime)};
                if (newPos.x < xRange.x) {
                    vel.x = -vel.x;
                    //newPos.x = newPos.x - (xRange.x - newPos.x)
                }
                else if (newPos.x > xRange.y)
                {
                    vel.x = -vel.x;
                }

                if (newPos.y > yRange.y)
                {
                    vel.y = -vel.y;
                }
                pos = newPos;
            }
            else
            {
                auto batTransform = Scene::mainScene->FindComponent<Transform2dComponent>(batUuid);
                Vec2 batPos = batTransform->GetPosition();
                pos.x = batPos.x;
                pos.y = batPos.y + 0.025f;
            }

            auto transform2dComp = scene->FindComponent<Transform2dComponent>(uuid);
            transform2dComp->SetLocalPosition(pos);
        }

        void SetInitPos(Vec2 initPos) { pos = initPos; }

    protected:

        bool moving = false;
        float speed = 0.15f;
        float accelRate = 0.005f;
        float size = 0.025f;
        Vec2 vel{0.0f, 0.0f};
        Vec2 xRange{-0.5f + size, 0.5f - size};
        Vec2 yRange{-10000.0f, 0.5f - size};
        Vec2 pos;

    };


    class BrickScript : public ScriptComponentBase
    {
    public:

        // Required by reflection system
        std::type_index GetType() override { return std::type_index(typeid(BrickScript)); }
        static std::string Name() { return "BrickScript"; }
        static std::vector<std::string> BaseClasses() { return {}; }
        static std::vector<Dynasti::PropertyPtr> Properties()
        {
            return std::vector<Dynasti::PropertyPtr>{};
        }
        void OnCreate() override {};
        void OnDestroy() override {};

        void OnOverlapBeginEvent(const Event event, std::shared_ptr<Scene> scene) override
        {
            if (event.GetData<Uuid>() == ballUuid)
            {
                ++numHits;
                if (numHits == 1)
                {
                    auto renderableComp = scene->FindComponent<RenderableComponent>(uuid);
                    renderableComp->SetColor(brickHitColor);
                }
                else if (numHits == 2)
                {
                    auto renderableComp = scene->FindComponent<RenderableComponent>(uuid);
                    renderableComp->SetColor(Vec4{0.0f, 0.0f, 0.0f, 0.0f});
                    auto collisionComp = scene->FindComponent<Shape2dComponent>(uuid);
                    collisionComp->ToggleCollision(false);
                }
            }
        }

        void OnOverlapEndEvent(const Dynasti::Event event, std::shared_ptr<Dynasti::Scene> scene) override
        {
        }

    protected:

        int numHits = 0;

    };


    class DongApp : public Application
    {
    public:

        DongApp() : Application("Dong")
        {
            leftActionKeyDownEventType_ = eventManager_->RegisterEvent("LeftActionKeyDown");
            leftActionKeyUpEventType_ = eventManager_->RegisterEvent("LeftActionKeyUp");
            rightActionKeyDownEventType_ = eventManager_->RegisterEvent("RightActionKeyDown");
            rightActionKeyUpEventType_ = eventManager_->RegisterEvent("RightActionKeyUp");
            fireActionKeyDownEventType_ = eventManager_->RegisterEvent("FireActionKeyDown");
            updateWorldPositionEventType_ = eventManager_->RegisterEvent("UpdateWorldPosition");
        }

    protected:

        bool SetupGame() override
        {
            if (Dynasti::Scene::mainScene->GetName() != "MainScene")
                return false;

            classRegistry_->RegisterClass<BrickScript>();
            classRegistry_->RegisterClass<BatScript>();
            classRegistry_->RegisterClass<BallScript>();

            //CreateWalls();
            CreateBat();
            CreateBall();
            CreateBricks();

            return true;
        }

        void CreateWalls()
        {
        }

        void CreateBat()
        {
            Vec2 initPos{0.0f, -0.45f};
            Vec2 size{batSize, 0.025f};
            Vec4 color{0.8f, 0.8f, 0.5f, 1.0f};

            auto &meshAssets = assetManager_->GetMeshRegistry();
            auto bat = Dynasti::Scene::mainScene->CreateNewEntity(-1, "Bat");
            batUuid = bat->uuid;
            auto transform2dComp = Dynasti::Scene::mainScene->CreateComponent<Transform2dComponent>(bat->uuid);
            transform2dComp->SetLocalPosition(initPos);
            transform2dComp->SetLocalScale(size);

            auto renderableComp = Dynasti::Scene::mainScene->CreateComponent<RenderableComponent>(bat->uuid);
            renderableComp->SetMesh(meshAssets.Find("Quad"));
            renderableComp->SetColor(color);

            auto shapeComp = Dynasti::Scene::mainScene->CreateComponent<Shape2dComponent>(bat->uuid);
            auto &localBox = shapeComp->GetLocalShape();
            auto &worldBox = shapeComp->GetWorldShape();
            localBox.size = size;
            worldBox.size = size;
            shapeComp->SetWorldPosition(initPos);

            auto batComp = Scene::mainScene->CreateComponent<BatScript>(bat->uuid);
            batComp->SetInitPos(initPos);
        }

        void CreateBall()
        {
            Vec2 initPos{0.0f, 0.1f};
            Vec2 size{ballSize, ballSize};
            Vec4 color{0.5f, 0.1f, 0.8f, 1.0f};

            auto &meshAssets = assetManager_->GetMeshRegistry();
            auto ball = Dynasti::Scene::mainScene->CreateNewEntity(-1, "Ball");
            ballUuid = ball->uuid;
            auto transform2dComp = Dynasti::Scene::mainScene->CreateComponent<Transform2dComponent>(ball->uuid);
            transform2dComp->SetLocalPosition(initPos);
            transform2dComp->SetLocalScale(size);

            auto renderableComp = Dynasti::Scene::mainScene->CreateComponent<RenderableComponent>(ball->uuid);
            renderableComp->SetMesh(meshAssets.Find("Quad"));
            renderableComp->SetColor(color);

            auto shapeComp = Dynasti::Scene::mainScene->CreateComponent<Shape2dComponent>(ball->uuid);
            auto &localBox = shapeComp->GetLocalShape();
            auto &worldBox = shapeComp->GetWorldShape();
            localBox.size = size;
            worldBox.size = size;
            shapeComp->SetWorldPosition(initPos);

            auto ballComp = Scene::mainScene->CreateComponent<BallScript>(ball->uuid);
        }

        void CreateBricks()
        {
            auto &meshAssets = assetManager_->GetMeshRegistry();
            RandomFloat randPos(-0.5f, 0.5f);
            RandomFloat randColor(0.0f, 1.0f);
            int numShapes = 0;
            for (int j = 0; j < numRows; ++j)
            {
                for (int i = 0; i < numColumns; ++i)
                {
                    Vec2 pos{(static_cast<float>(i) - 0.5f*static_cast<float>(numColumns) + 0.5f) * brickSpacing,
                             (static_cast<float>(j) - 0.5f*static_cast<float>(numRows)) * brickSpacing + 0.15f};
                    CreateBrick("Brick" + std::to_string(i), pos);
                }
            }
        }

        void CreateBrick(std::string name, Dynasti::Vec2 pos)
        {
            auto &meshAssets = assetManager_->GetMeshRegistry();
            auto brick = Dynasti::Scene::mainScene->CreateNewEntity(-1, name);
            auto transform2dComp = Dynasti::Scene::mainScene->CreateComponent<Transform2dComponent>(brick->uuid);
            transform2dComp->SetLocalPosition(pos);
            transform2dComp->SetLocalScale({brickSize, brickSize});
            auto renderableComp = Dynasti::Scene::mainScene->CreateComponent<RenderableComponent>(brick->uuid);
            renderableComp->SetMesh(meshAssets.Find("Quad"));
            renderableComp->SetColor(brickInitColor);
            auto shapeComp = Dynasti::Scene::mainScene->CreateComponent<Shape2dComponent>(brick->uuid);
            auto &localBox = shapeComp->GetLocalShape();
            auto &worldBox = shapeComp->GetWorldShape();
            localBox.size = {brickSize, brickSize};
            worldBox.size = {brickSize, brickSize};
            shapeComp->SetWorldPosition(pos);

            auto quadComp = Scene::mainScene->CreateComponent<BrickScript>(brick->uuid);
        }

    };
}


int main(int argc, char* args[])
{
    Dynasti::Dong::DongApp app;
    app.Run();
    return 0;
}