#include "Dynasti/Application/Application.h"
#include "Dynasti/Asset/AssetManager.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Scene/Scene.h"
#include "Dynasti/Renderer/RenderableComponent.h"
#include "Dynasti/Transform/Transform2dComponent.h"


void CreateScene()
{
    if (Dynasti::Scene::mainScene->GetName() != "MainScene")
        return;

    auto assetManager = Dynasti::Scene::mainScene->GetAssetManager();
    auto& meshAssets = assetManager->GetMeshRegistry();

    auto triangle1 = Dynasti::Scene::mainScene->CreateNewEntity(-1, "Circle");
    auto tComp1 = Dynasti::Scene::mainScene->CreateComponent<Dynasti::Transform2dComponent>(triangle1->uuid);
    tComp1->SetLocalPosition(Dynasti::Vec2(-0.35f, 0.0f));
    tComp1->SetLocalScale(Dynasti::Vec2(0.25f, 0.25f));
    auto rComp1 = Dynasti::Scene::mainScene->CreateComponent<Dynasti::RenderableComponent>(triangle1->uuid);
    rComp1->SetMesh(meshAssets.Find("Ellipse"));
    rComp1->SetColor(Dynasti::Vec4(1.0f, 0.0f, 0.5f, 1.0f));

    auto triangle2 = Dynasti::Scene::mainScene->CreateNewEntity(-1, "Annulus");
    auto tComp2 = Dynasti::Scene::mainScene->CreateComponent<Dynasti::Transform2dComponent>(triangle2->uuid);
    tComp2->SetLocalPosition(Dynasti::Vec2(0.35f, 0.0f));
    //tComp2->SetLocalRotation(0.5f);
    tComp2->SetLocalScale(Dynasti::Vec2(0.25f, 0.25f));
    auto rComp2 = Dynasti::Scene::mainScene->CreateComponent<Dynasti::RenderableComponent>(triangle2->uuid);
    rComp2->SetMesh(meshAssets.Find("Annulus"));
    rComp2->SetColor(Dynasti::Vec4(0.0f, 1.0f, 0.8f, 1.0f));
    //auto testScriptComp2 = Dynasti::Scene::mainScene->CreateComponent<TestScriptComponent>(triangle2->uuid);
}


int main(int argc, char* args[])
{
    Dynasti::Application app("TestProject1", std::bind(&CreateScene));
    app.Run();
    return 0;
}